package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.ICity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by joews on 2/23/2017.
 */
class DestinationCardTest
{
	List<DestinationCard> mDestinationCards = new ArrayList<>();
	List<ICity[]> mICities = new ArrayList<>();
	List<Integer> mPoints = new ArrayList<>();

	@BeforeEach
	void setUp()
	{
		//per preconditions the cities arary can't be null nor either entry in it
		mICities.add(new ICity[]{new City("Alabama"), new City("Arkansas")});
		mICities.add(new ICity[]{new City("Florida"), new City("Georgia")});
		mICities.add(new ICity[]{new City("Delaware"), new City("Alabama")});

		//per preconditinos points is >= 0
		mPoints.add(0);
		mPoints.add(1);
		mPoints.add(20);

		mDestinationCards.add(new DestinationCard(mICities.get(0), mPoints.get(0)));
		mDestinationCards.add(new DestinationCard(mICities.get(1), mPoints.get(1)));
		mDestinationCards.add(new DestinationCard(mICities.get(2), mPoints.get(2)));
		mDestinationCards.add(new DestinationCard(mDestinationCards.get(1)));
	}

	@AfterEach
	void tearDown()
	{
		mICities.clear();
		mPoints.clear();
		mDestinationCards.clear();

	}

	@Test
	void getCities()
	{
		for (int i = 0; i < mDestinationCards.size() - 1; i++)
		{
			assertEquals(mICities.get(i).length, mDestinationCards.get(i).getCities().size());
			assertEquals(mICities.get(i)[0], mDestinationCards.get(i).getCities().get(0));
			assertEquals(mICities.get(i)[1], mDestinationCards.get(i).getCities().get(1));
		}

		assertEquals(mDestinationCards.get(1).getCities().size(), mDestinationCards.get(3).getCities().size());
		assertEquals(mDestinationCards.get(1).getCities().get(0), mDestinationCards.get(3).getCities().get(0));
		assertEquals(mDestinationCards.get(1).getCities().get(1), mDestinationCards.get(3).getCities().get(1));


	}

	@Test
	void getPoints()
	{
		for (int i = 0; i < mDestinationCards.size() - 1; i++)
		{
			assertTrue(mPoints.get(i).equals(mDestinationCards.get(i).getPoints()));
		}
		assertEquals(mDestinationCards.get(1).getPoints(), mDestinationCards.get(3).getPoints());
	}

	@Test
	void getId()
	{
		//Ensure all IDS are unique and not null
		Set<String> ids = new HashSet<>();
		for (int i = 0; i < mDestinationCards.size(); i++)
		{
			if (mDestinationCards.get(i).getId() != null)
			{
				ids.add(mDestinationCards.get(i).getId());
			}
		}
		//NOTE: 1 is repeated
		assertEquals(mDestinationCards.size() - 1, ids.size());
	}

}