package edu.byu.cs340.models.gameModel.map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Created by joews on 2/27/2017.
 *
 * Tests for the route object
 */
class RouteTest
{
	@BeforeEach
	void setUp()
	{

	}

	@AfterEach
	void tearDown()
	{

	}

	@Test
	void getId()
	{

	}

	@Test
	void getCities()
	{

	}

	@Test
	void isClaimed()
	{

	}

	@Test
	void getClaimedUsername()
	{

	}

	@Test
	void getType()
	{

	}

	@Test
	void getNumTrains()
	{

	}

	@Test
	void claimRoute()
	{

	}

	@Test
	void getPoints()
	{

	}

	@Test
	void equals()
	{

	}

}