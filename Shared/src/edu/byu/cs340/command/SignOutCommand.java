package edu.byu.cs340.command;


/**
 * Created by Rebekah on 2/8/2017.
 * Base command mData for  signing a player out.
 */
public class SignOutCommand extends BaseCommand
{

	protected String mLobbyID;
	protected String mGameID;

	/**
	 * @param username the name of the user
	 * @param lobbyID  Nullable
	 * @param gameID   Nullable
	 * @pre username is not null
	 */
	public SignOutCommand(String username, String lobbyID, String gameID)
	{
		super(username);
		if (lobbyID != null)
		{
			mLobbyID = lobbyID;
		}
		if (gameID != null)
		{
			mGameID = gameID;
		}
	}
}
