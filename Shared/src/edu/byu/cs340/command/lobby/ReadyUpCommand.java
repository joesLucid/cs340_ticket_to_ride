package edu.byu.cs340.command.lobby;

/**
 * Created by joews on 2/1/2017.
 * Base command mData for reading up a player
 */
public class ReadyUpCommand extends BaseLobbyCommand
{
	protected boolean mIsReady;


	public ReadyUpCommand(String username, int lastCommand, String lobbyID, boolean isReady)
	{
		super(username, lastCommand, lobbyID);
		mIsReady = isReady;
	}
}
