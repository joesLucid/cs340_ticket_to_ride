package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.BaseCommand;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;

/**
 * Created by joews on 2/7/2017.
 * Base command mData for getting games list
 */
public class GetGamesListCommand extends BaseCommand
{
	IRoomsListInfo mRoomsListInfo;

	public GetGamesListCommand(String username, IRoomsListInfo roomsListInfo)
	{
		super(username);
		mRoomsListInfo = roomsListInfo;
	}
}
