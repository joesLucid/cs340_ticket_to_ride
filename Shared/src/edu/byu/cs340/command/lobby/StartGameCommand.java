package edu.byu.cs340.command.lobby;

import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * Base command mData for starting a game from the lobby
 */
public class StartGameCommand extends BaseLobbyCommand
{
	int mNumTrainCardsInDeck = 0;
	int mNumTrainsPerPlayer = 0;
	int mNumDestinationCardsInDeck = 0;
	List<ITrainCard> mRevealedTrainCards;
	List<String> mPlayerOrder;
	List<ICity> mCities;
	List<IRoute> mRoutes;
	String mLobbyName;
//	List<IDestinationCard> mDemoDestinationCardDeck;//TODO: Demo only
//	List<ITrainCard> mDemoTrainCardDeck;//TODO: Demo only

	public StartGameCommand(String username, int lastCommand, String lobbyID)
	{
		super(username, lastCommand, lobbyID);
		mRevealedTrainCards = new ArrayList<>();
		mPlayerOrder = new ArrayList<>();
		this.mCities = new ArrayList<>();
		this.mRoutes = new ArrayList<>();
//		this.mDemoDestinationCardDeck = new ArrayList<>();
//		this.mDemoTrainCardDeck = new ArrayList<>();
	}

	public StartGameCommand(StartGameCommand that)
	{
		super(that);
		this.mNumDestinationCardsInDeck = that.mNumDestinationCardsInDeck;
		this.mNumTrainsPerPlayer = that.mNumTrainsPerPlayer;
		this.mNumTrainCardsInDeck = that.mNumTrainCardsInDeck;
		this.mPlayerOrder = that.mPlayerOrder;
		this.mRevealedTrainCards = that.mRevealedTrainCards;
		this.mCities = that.mCities;
		this.mRoutes = that.mRoutes;
//		this.mDemoDestinationCardDeck = that.mDemoDestinationCardDeck;
//		this.mDemoTrainCardDeck = that.mDemoTrainCardDeck;
		this.mRevealedTrainCards = new ArrayList<>();
		for (ITrainCard card : that.mRevealedTrainCards)
		{
			mRevealedTrainCards.add(card);
		}
		this.mLobbyName = that.mLobbyName;
	}
}
