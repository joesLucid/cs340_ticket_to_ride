package edu.byu.cs340.command.lobby;

/**
 * Created by joews on 2/1/2017.
 * Base command mData for removing a player from the lobby
 */
public class RemovePlayerFromLobbyCommand extends BaseLobbyCommand
{
	protected String newHost;
	protected String userNameToRemove;

	public RemovePlayerFromLobbyCommand(String username, int lastCommand, String lobbyID, String userNameToRemove)
	{
		super(username, lastCommand, lobbyID);
		this.userNameToRemove = userNameToRemove;
	}

	/**
	 * Get value of UserNameToKick
	 *
	 * @return Returns the value of UserNameToKick
	 */
	public String getUserNameToRemove()
	{
		return userNameToRemove;
	}

	/**
	 * Set the value of userNameToRemove variable
	 *
	 * @param userNameToRemove The value to set userNameToRemove member variable to
	 */
	public void setUserNameToRemove(String userNameToRemove)
	{
		this.userNameToRemove = userNameToRemove;
	}


}
