package edu.byu.cs340.command.game;

/**
 * Created by crown on 3/24/2017.
 */
public class LeaveGameCommand extends BaseGameCommand {	
	public LeaveGameCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public LeaveGameCommand(BaseGameCommand that) {
		super(that);
	}
}
