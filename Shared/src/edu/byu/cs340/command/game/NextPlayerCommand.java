package edu.byu.cs340.command.game;

/**
 * Created by joews on 3/4/2017.
 */
public class NextPlayerCommand extends BaseGameCommand
{
	protected String mNextPlayer;

	public NextPlayerCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}

	public NextPlayerCommand(NextPlayerCommand that)
	{
		super(that);
		mNextPlayer = that.mNextPlayer;
	}
}
