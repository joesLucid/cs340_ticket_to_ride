package edu.byu.cs340.command.game;

import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class ReturnDestinationCardsCommand extends BaseGameCommand
{
	int mMaxCardsToReturn;
	List<IDestinationCard> mCardsToReturn;
	boolean mSuccess = false;
	int mNumCardsReturned;
	int mNewDestinationDeckSize;
	protected int mNewScore;
	int mNumCardsKept;
	int mPrivateScore;

	public ReturnDestinationCardsCommand(String username, int lastCommand, String gameID, int
			maxCardsToReturn, List<IDestinationCard> cardsToReturn)
	{
		super(username, lastCommand, gameID);
		mNumCardsReturned = cardsToReturn.size();
		this.mCardsToReturn = cardsToReturn;
		mMaxCardsToReturn = maxCardsToReturn;
	}

	public ReturnDestinationCardsCommand(ReturnDestinationCardsCommand that)
	{
		super(that);
		mCardsToReturn = new ArrayList<>();
		for (IDestinationCard card : that.mCardsToReturn)
		{
			if (card != null)
			{
				mCardsToReturn.add(new DestinationCard(card));
			}
		}
		mNumCardsReturned = that.mNumCardsReturned;
		mSuccess = that.mSuccess;
		mNewDestinationDeckSize = that.mNewDestinationDeckSize;
		this.mNewScore = that.mNewScore;
		this.mMaxCardsToReturn = that.mMaxCardsToReturn;
		mNumCardsKept = that.mNumCardsKept;
		mPrivateScore = that.mPrivateScore;
	}

}
