package edu.byu.cs340.command.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tjacobhi on 24-Feb-17.
 *
 */
public class GetGameState extends BaseGameCommand
{
	protected List<String> mPlayers;

	public GetGameState(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
		mPlayers = new ArrayList<>();
	}


}
