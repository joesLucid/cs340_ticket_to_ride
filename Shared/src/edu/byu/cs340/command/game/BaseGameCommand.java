package edu.byu.cs340.command.game;

import edu.byu.cs340.command.BaseRoomCommand;

/**
 * Created by joews on 2/1/2017.
 *
 * Base command functionality for games.
 */
public abstract class BaseGameCommand extends BaseRoomCommand
{
	protected String mGameID;

	public BaseGameCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand);
		mGameID = gameID;
	}

	public BaseGameCommand(BaseGameCommand that)
	{
		super(that);
		mGameID = that.mGameID;
	}


	/**
	 * Get value of GameID
	 *
	 * @return Returns the value of GameID
	 */
	public String getGameID()
	{
		return mGameID;
	}

	/**
	 * Set the value of gameName variable
	 *
	 * @param gameID The value to set gameName member variable to
	 */
	public void setGameID(String gameID)
	{
		mGameID = gameID;
	}

}
