package edu.byu.cs340.command.game;

import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;

import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class DrawTrainCardCommand extends BaseGameCommand
{
	ITrainCard mTrainCard;
	int mDrawnZone;
	List<ITrainCard> mRevealed;
	boolean mCanDrawAnother;
	int mNewTrainDeckSize;
	int mNewDiscardSize;

	public DrawTrainCardCommand(String username, int lastCommand, String gameID, int drawnZone)
	{
		super(username, lastCommand, gameID);
		mDrawnZone = drawnZone;
	}

	public DrawTrainCardCommand(DrawTrainCardCommand that)
	{
		super(that);
		if (that.mTrainCard != null)
		{
			mTrainCard = new TrainCard(that.mTrainCard);
		}
		mDrawnZone = that.mDrawnZone;
		mRevealed = that.mRevealed;
		mCanDrawAnother = that.mCanDrawAnother;
		mNewTrainDeckSize = that.mNewTrainDeckSize;
		mNewDiscardSize = that.mNewDiscardSize;
	}
}
