package edu.byu.cs340.command.game;

import edu.byu.cs340.models.gameModel.gameOver.GameOverData;

/**
 * Created by Jenessa Welker on 3/25/2017.
 * 
 * This is the command for the server telling the client the game is over.
 * Contains all the data necessary for the game over screen.
 * 
 * @invariant mData != null
 */
public class GameOverCommand extends BaseGameCommand {
	//A class containing player scores, destination cards claimed, longest route points, etc. for the game over screen
	GameOverData mData;

	/**
	 * Constructor uses super.
	 * @param username the username of the current player
	 * @param lastCommand the last command the client had gotten since this command
	 * @param gameID the id of the game this command belongs to
	 */
	public GameOverCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	/**
	 * Copy constructor
	 * @param that the command to copy
	 */
	public GameOverCommand(GameOverCommand that)
	{
		super(that);
		mData = that.mData;
	}
}
