package edu.byu.cs340.command;

/**
 * Created by joews on 2/11/2017.
 *
 * Base command for rooms which requires a last command that the room executed.
 */
public class BaseRoomCommand extends BaseCommand
{
	protected int mLastCommand;

	public BaseRoomCommand(String username, int lastCommand)
	{
		super(username);
		mLastCommand = lastCommand;
	}

	public BaseRoomCommand(BaseRoomCommand that)
	{
		super(that);
		mLastCommand = that.mLastCommand;
	}
}
