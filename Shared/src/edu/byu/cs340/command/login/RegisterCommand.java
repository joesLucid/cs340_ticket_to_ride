package edu.byu.cs340.command.login;

import edu.byu.cs340.command.IBaseCommand;

/**
 * Created by joews on 2/1/2017.
 * Base command holding mData for register.
 */
public class RegisterCommand extends BaseLoginCommand
{

	protected String mConfirmPassword;

	public RegisterCommand(String username, String password, String authenticationToken, String confirmPassword)
	{
		super(username, password, authenticationToken);
		mConfirmPassword = confirmPassword;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
//		super.strip(userStripping);
//		if (userStripping != mUsername)
//		{
//			mAuthenticationToken = null;
//			mPassword = null;
//		}
		return this;
	}
}
