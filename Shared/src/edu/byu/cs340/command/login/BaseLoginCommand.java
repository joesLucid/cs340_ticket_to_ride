package edu.byu.cs340.command.login;

import edu.byu.cs340.command.BaseCommand;

/**
 * Created by joews on 2/1/2017.
 *
 * Base command holding mData for login command types
 */
public abstract class BaseLoginCommand extends BaseCommand
{
	protected String mPassword;
	protected String mAuthenticationToken;


	public BaseLoginCommand(String username, String password, String authenticationToken)
	{
		super(username);
		mPassword = password;
		mAuthenticationToken = authenticationToken;
	}

	/**
	 * Get value of Password
	 *
	 * @return Returns the value of Password
	 */
	public String getPassword()
	{
		return mPassword;
	}

	/**
	 * Set the value of password variable
	 *
	 * @param password The value to set password member variable to
	 */
	public void setPassword(String password)
	{
		mPassword = password;
	}

	/**
	 * Get value of AuthenticationToken
	 *
	 * @return Returns the value of AuthenticationToken
	 */
	public String getAuthenticationToken()
	{
		return mAuthenticationToken;
	}

	/**
	 * Set the value of authenticationToken variable
	 *
	 * @param authenticationToken The value to set authenticationToken member variable to
	 */
	public void setAuthenticationToken(String authenticationToken)
	{
		mAuthenticationToken = authenticationToken;
	}
}
