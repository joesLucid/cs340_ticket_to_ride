package edu.byu.cs340.models.gameModel.bank;

/**
 * Created by joews on 2/22/2017.
 *
 * interface for Cards with ID
 * @invatian getId()!=null
 */
public interface ICard
{
	/**
	 * Gets the ID of the card.
	 *
	 * @return Returns the card's ID.
	 *
	 * @pre None
	 * @post Returns the ID.
	 */
	String getId();
}
