package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by joews on 2/22/2017.
 * Class representing TrainCards. These simply hold what type of train the card is associated with.
 *
 * @invariant getTrainType()!=null && getTrainType()!=TrainType.None
 */
public class TrainCard extends Card implements ITrainCard
{
	private TrainType mTrainType;

	/**
	 * Constructs the Train card with the type TrainType specified.
	 *
	 * @param trainType TrainType associated with this card.
	 * @pre mTrainType!=null && mTrainType!=TrainType.None
	 * @post getTrainType()==trainType
	 */
	public TrainCard(TrainType trainType)
	{
		mTrainType = trainType;
	}

	/**
	 * Copy Constructor.
	 *
	 * @param that Train Card to copy.
	 */
	public TrainCard(ITrainCard that)
	{
		super((TrainCard) that);
		this.mTrainType = that.getTrainType();
	}

	/**
	 * Get value of TrainType
	 *
	 * @return Returns the value of TrainType
	 *
	 * @post result!=null
	 */
	public TrainType getTrainType()
	{
		return mTrainType;
	}
}
