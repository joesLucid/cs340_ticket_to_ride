package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by joews on 3/4/2017.
 */
public interface ITrainCard extends ICard
{
	/**
	 * Get value of TrainType
	 *
	 * @return Returns the value of TrainType
	 *
	 * @post result!=null
	 */
	public TrainType getTrainType();
}
