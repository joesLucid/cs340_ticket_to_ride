package edu.byu.cs340.models.gameModel.map;

import java.util.List;

/**
 * Created by joews on 2/23/2017.
 *
 * Object representing the routes. Holds cities.
 *
 * @invariant getId()!=null
 * @invariant getCities()!=null
 * @invariant getType()!=null
 */
public interface IRoute
{
	/**
	 * Gets the ID of the route.
	 *
	 * @return the ID of the route.
	 */
	String getId();

	/**
	 * Gets the cities associated with the route.
	 *
	 * @return The cities associated with the route
	 *
	 * @post result != null && result.size()==2
	 * @post result[0] && result [1] may be null
	 */
	List<ICity> getCities();

	/**
	 * Tells if the route is claimed.
	 *
	 * @return True if the route is claimed, false otherwise.
	 */
	boolean isClaimed();

	/**
	 * Gets the name of the user who has claimed the route, null if no one has.
	 *
	 * @return Username of the person who claimed the route (if any).
	 *
	 * @post if isClaimed(), result !=null, else result may be null
	 */
	String getClaimedUsername();

	/**
	 * Get the type of train associated with the route.
	 *
	 * @return The type fo train associated with the route.
	 *
	 * @post result!=null && result!=TrainType.Locomotive
	 * @post result == TrainType.None if there is no restriction on train type card that can claim it.
	 */
	TrainType getType();

	/**
	 * Gets the number of trains required to claim the route.
	 *
	 * @return the number of trains required to claim the route.
	 *
	 * @post 0&lt;result&lte;6.
	 */
	int getNumTrains();

	/**
	 * Claims the route for the user specified.
	 *
	 * @param username Name of user claiming the route.
	 * @return Returns true if the route is successfully claimed, false otherwise.
	 *
	 * @pre username!=null && !username.isEmpty()
	 * @pre !isClaimed()
	 * @post getClaimedUsername().equals(username)
	 * @post isClaimes()==true
	 */
	boolean claimRoute(String username);

	/**
	 * Gets the number of points assocaited with the route (!numTrains)
	 *
	 * @return the number of points
	 *
	 * @pre getNumTrains()>=0
	 */
	public int getPoints();

	String getPairedRouteID();
}
