package edu.byu.cs340.models.gameModel.map;

/**
 * Created by joews on 2/22/2017.
 *
 * Enum specifying the types of trains associated with routes.
 */
public enum TrainType
{

	Box("purple"),
	Passenger("white"),
	Tanker("blue"),
	Reefer("yellow"),
	Freight("orange"),
	Hopper("black"),
	Coal("red"),
	Caboose("green"),
	Locomotive("locomotive"),
	None("none");
	private String mColor;

	TrainType(String color)
	{
		mColor = color;
	}

	/**
	 * Get value of RotatedImage
	 *
	 * @return Returns the value of RotatedImage
	 */
	public String getRotatedImage()
	{
		return "/game/" + getColor() + "TrainR.jpg";
	}

	protected String getColor()
	{
		return mColor;
	}

}