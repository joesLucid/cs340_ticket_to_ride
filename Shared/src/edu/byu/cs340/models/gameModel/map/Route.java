package edu.byu.cs340.models.gameModel.map;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by joews on 2/26/2017.
 *
 * Object representing the route between two cities on the game map.
 */
public class Route implements IRoute
{
	private String mId;
	protected ICity[] mCities;
	private String mClaimedUsername;
	private TrainType mTrainType;
	private int mNumTrains;
	private String mPairedRouteID;
	private static final int[] mPointValues = {1, 2, 4, 7, 10, 15};



	/**
	 * @param cities    Array containing the two cities.
	 * @param trainType Type of train required for the route, TrainType.None if no requirement for route
	 * @param numTrains The number of trains required to claim the route (0,6]
	 * @param pairedRouteID The ID of the route this route is paired with. Null if the route is not paired.
	 * @pre cities!=null && cities.size()==2 && cities[0]!=null && cities[1]!=null
	 * @pre trainType!=TrainType.Locomotive && trainType!=null
	 * @pre numTrains>0 && numTrains <=6
	 */
	public Route(ICity[] cities, TrainType trainType, int numTrains, String pairedRouteID)
	{
		mId = UUID.randomUUID().toString();
		mCities = cities;
		mTrainType = trainType;
		mNumTrains = numTrains;
		mClaimedUsername = null;
		mPairedRouteID = pairedRouteID;
	}

	/**
	 * Copy constructor.
	 *
	 * @param that Object to copy.
	 * @pre that!=null
	 * @post all variables in this are the same as that.
	 */
	public Route(IRoute that)
	{
		this.mId = that.getId();
		this.mClaimedUsername = that.getClaimedUsername();
		this.mTrainType = that.getType();
		this.mNumTrains = that.getNumTrains();
		this.mPairedRouteID = that.getPairedRouteID();
		ICity[] newCities = new ICity[2];
		for (int i = 0; i < 2; i++)
		{
			newCities[i] = new City(that.getCities().get(i));
		}
		this.mCities = newCities;
	}

	@Override
	public String getId()
	{
		return mId;
	}

	@Override
	public List<ICity> getCities()
	{
		return Collections.unmodifiableList(Arrays.asList(mCities));
	}

	@Override
	public boolean isClaimed()
	{
		return !(mClaimedUsername == null || mClaimedUsername.isEmpty());
	}

	@Override
	public String getClaimedUsername()
	{
		return mClaimedUsername;
	}

	@Override
	public TrainType getType()
	{
		return mTrainType;
	}

	@Override
	public int getNumTrains()
	{
		return mNumTrains;
	}

	@Override
	public boolean claimRoute(String username)
	{
		if (!isClaimed() && (mClaimedUsername == null || mClaimedUsername.isEmpty()))
		{
			mClaimedUsername = username;
			return true;
		} else
		{
			return false;
		}
	}

	@Override
	public int getPoints()
	{
		return mPointValues[mNumTrains - 1];
	}

	/**
	 * Override equals to compare the routes by ID.
	 *
	 * @param obj Object to compare to.
	 * @return Returns true if the objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Route)
		{
			return ((Route) obj).getId().equals(this.getId());
		} else
		{
			return false;
		}
	}

	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}
	
	public String getPairedRouteID() {
		return mPairedRouteID;
	}

	public void setPairedRouteID(String pairedRouteID) {
		mPairedRouteID = pairedRouteID;
	}
}
