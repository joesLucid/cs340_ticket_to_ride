package edu.byu.cs340.models.gameModel.map;

/**
 * Created by joews on 2/22/2017.
 *
 * Interface for the city objects.
 */
public interface ICity
{
	/**
	 * Gets th id of the city.
	 *
	 * @return The string ID of te city.
	 */
	String getId();

	/**
	 * Gets the name of the city.
	 *
	 * @return The string name of the city.
	 */
	String getName();
}
