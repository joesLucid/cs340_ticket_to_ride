package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.gameOver.GameOverData;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.roomModel.IPlayerRoom;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by joews on 2/24/2017.
 *
 * Interface for the servers game models. Keeps track of hte cards, routes, cities, players, and chat associated with
 * a game. Extends functionality from IPLayerRoom.
 */
public interface IServerGameModel extends IPlayerRoom
{
	/**
	 * Number of destination cards to draw in a group.
	 */
	int NUM_DESTINATION_CARDS_TO_DRAW = 3;
	/**
	 * Number of destination cards required to keep at the start of the game.
	 */
	int MIN_NUM_DESTINATION_CARDS_START = 2;
	/**
	 * Number of destination cards required to keep during the non-first turns in the game.
	 */
	int MIN_NUM_DESTINATION_CARDS_KEEP_IN_GAME = 1;


	/**
	 * Verifies current player owns the cards, the route is not taken, the cards are sufficient to claim the route.
	 * If this is met the route is then claimed, the cards are removed from the player and placed in the discard pile.
	 *
	 * @param routeID    The id associated with the route to claim.
	 * @param trainCards The train cards to claim route with.
	 * @return True if the route was claimed, false otherwise.
	 *
	 * @pre isInitialized()==true
	 * @pre routeID!=null
	 * @pre trainCards!=null
	 * @post getRoute(routeID).isClaimer()==true
	 * @post getPlayer(getCurrentPlayerName()).getRoutes().contains(getRoute(routeID)) == true
	 * @post getPlayer(getCurrentPlayerName()).trainCards().containsAll(trainCards)) == false
	 */
	boolean claimRoute(String routeID, List<ITrainCard> trainCards);

	/**
	 * Gets the number of trains a specific player has
	 *
	 * @param username The user name
	 * @return the number of trains the player has left
	 *
	 * @pre isInitialized()==true
	 * @pre hasPlayer(username)
	 * @pre username!=null
	 * @post the number of trains the player has
	 * @post result > 0
	 */
	int getTrainsRemaining(String username);

	/**
	 * Gets the routes on the edu.byu.cs340.models.gameModel.
	 *
	 * @return A copy of the routes in the edu.byu.cs340.models.gameModel.
	 *
	 * @pre isInitialized()==true
	 */
	List<IRoute> getRoutes();

	/**
	 * Gets all of the cities on the edu.byu.cs340.models.gameModel.
	 *
	 * @return A copy of the cities on the edu.byu.cs340.models.gameModel.
	 *
	 * @pre isInitialized()==true
	 */
	List<ICity> getCities();

	/**
	 * Gets the username of hte player with the longest route.
	 *
	 * @return The username of the player with hte longest route.
	 *
	 * @pre isInitialized()==true
	 */
	Set<String> getLongestRoutePlayers();

	/**
	 * Gets the destination cards held by the current player.
	 *
	 * @return A copped list of destination cards held by the current player.
	 *
	 * @pre isInitialized()==true
	 */
	List<IDestinationCard> getCurrentPlayersDestinationCards();

	/**
	 * Gets the destination cards held by the current player that they are choosing between.
	 *
	 * @return A copped list of destination cards held by the current player that they are to chose between
	 *
	 * @pre isInitialized()==true
	 */
	List<IDestinationCard> getCurrentPlayersTemporaryDestinationCards();

	/**
	 * Gets the train cards held by the current player.
	 *
	 * @return A copped list of train cards held by the current player.
	 *
	 * @pre isInitialized()==true
	 */
	List<ITrainCard> getCurrentPlayersTrainCards();


	Map<String, Integer> getPublicScore();


	int getPublicScore(String username);

	/**
	 * @param username the user to see their hidden score
	 * @return the hidden score
	 */
	int getHiddenScore(String username);

	/**
	 * Tells whether it is the player's turn in the game.
	 *
	 * @param username Username of player asking
	 * @return True if is username's turn, false otherwise.
	 *
	 * @pre username!=null;
	 * @pre isInitialized()==true
	 * @post returns if its the players turn.
	 */
	boolean isPlayersTurn(String username);

	/**
	 * Attempts to change the turn to the next player.
	 *
	 * @return Returns true if it is successful, false otherwise
	 */
	boolean nextPlayer();

	/**
	 * gets the name of the current player.
	 *
	 * @return The name of the current player
	 *
	 * @pre isInitialized()==true
	 */
	String getCurrentPlayerName();


	/**
	 * Removes the given player from the game.
	 *
	 * @param username the username to remove
	 * @return returns false if it couldn't complete the request. (Player not in game).
	 */
	boolean removePlayer(String username);


	//<editor-fold desc="Card Manipulation">

	/**
	 * Draws {@link #NUM_DESTINATION_CARDS_TO_DRAW} Destination bank from the destination card deck.
	 * This will draw new cards unless the player already has destination cards they haven't returned,
	 * in which case those cards will be returned.
	 *
	 * @return a list of the bank drawn, or a list of smaller sign with all bank drawn (the remaining ones left)
	 *
	 * @pre isInitialized()==true
	 * @pre username!=null
	 * @pre isPlayerTurn(username) || !isInitialState()
	 * @post the bank drawn will be removed from the deck.
	 * @post returns the destination card drawn.
	 */
	List<IDestinationCard> drawDestinationCards(String username);

	/**
	 * Draws the top cad from the train card deck and returns it. It is removed from the deck. If the deck was empty it
	 * will shuffle the discard pile and use that as the deck. If the discard pile is also empty it will return null.
	 *
	 * @return The card drawn or null if no card is available.
	 *
	 * @pre isInitialized()==true
	 * @pre username!=null
	 * @pre isPlayerTurn(username) || !isInitialState()
	 * @post returns the train card drawn or null if non are available
	 * @post if the deck is empty when drawing, it will set the deck as the discard pile and shuffle it.
	 */
	ITrainCard drawTrainCardFromDeck(String username);

	/**
	 * Gets the train card from the designated revealedZone (index). Replaces the revealed card
	 * with the top card of the train card deck by drawing it (which will force shuffle of the discard
	 * pile if needed).
	 *
	 * @param revealedZone The revealed zone 0-4.
	 * @return The train card selected or null if no card.
	 *
	 * @pre isInitialized()==true
	 * @pre 0 <= revealedZone <= 4
	 * @pre username!=null
	 * @pre isPlayerTurn(username) || !isInitialState()
	 * @post card in revealedZone != original card in that zone (by ID)
	 * @post returns the card.
	 */
	ITrainCard drawTrainCardFromRevealed(int revealedZone, String username);

	/**
	 * Tells if the given user can draw another card this turn
	 *
	 * @return whether the user can draw another card
	 */
	boolean canDrawAnotherCard();

	/**
	 * Gets the cards from the revealed train card area.
	 *
	 * @return List of revealed train cards
	 */
	List<ITrainCard> getRevealedCards();

	/**
	 * Gets the order of the players by name
	 *
	 * @return A list of names of players in turn order
	 */
	List<String> getPlayerOrder();


	/**
	 * Returns the train cards that the player drew that they aren't keeping in their hand. Adds the remaining
	 * ones the player drew to their hand.
	 *
	 * @param destinationCardList List of destination cards to put on the bottom of he deck.
	 * @param username            the username that is returning cards
	 * @return True is the destination cards are put back false otherwise
	 *
	 * @pre isInitialized()==true
	 * @pre username!=null && hasPlayer(username)
	 */
	boolean returnDestinationCardsToBottom(List<IDestinationCard> destinationCardList, String username);

	/**
	 * Gets the number of trains for a user
	 *
	 * @param username Username of hte user to get num of trains for
	 * @return Int number of trains
	 *
	 * @post result>0
	 * @pre hasPlayer(username)==true
	 */
	int getNumTrains(String username);

	/**
	 * Gets the number of train cards in the deck
	 *
	 * @return The number of train cards in the deck
	 */
	int getNumTrainCardsInDeck();

	/**
	 * Gets the number of train cards in the discard pile
	 *
	 * @return The number of train cards in the discard pile
	 */
	int getNumTrainCardsInDiscard();

	/**
	 * Gets the number of destination cards in the deck
	 *
	 * @return The number of destination cards in the deck
	 */
	int getNumDestinationCardsInDeck();

//</editor-fold>


	//<editor-fold desc="Initializing">

	/**
	 * Initializes the game models from a ServerLobbyModel
	 *
	 * @param lobby
	 * @pre lobby!=null
	 * @post isInitialized()==true
	 */
	void initializeFromLobby(LobbyModel lobby, List<ITrainCard> trainCards, List<IDestinationCard> destinationCards,
	                         List<ICity> cities, List<IRoute> routes);

	/**
	 * Method to determine if the game has been initialized.
	 *
	 * @return True if the game has been initialized, false otherwise.
	 */
	boolean isInitialized();


	/**
	 * Returns the name of the player who will be next
	 *
	 * @return
	 */
	String getNextPlayerName();

//	/**
//	 * Method to return train cards only to be used with demo. DELETE after phase 2.
//	 * @return A list of the train cards.
//	 */
//	List<ITrainCard> getDemoTrainCards();
//
//	/**
//	 * Method to return destination cards only to be used with demo. DELETE after phase 2.
//	 * @return A list of the destination cards.
//	 */
//	List<IDestinationCard> getDemoDestinationCards();
	//</editor-fold>

	/**
	 * @return true when the game is over
	 */
	boolean isGameOver();

	GameOverData getGameOverData();

	/**
	 * @return true when the final round is in progress
	 */
	boolean isFinalRound();

	boolean isInitialState();

	String getFinalRoundLastPlayer();

	void markLastTurn();

	boolean isLastTurn();

	boolean hasDestinationCards();

	int getNumDestinationCards(String username);

	boolean setNextPlayer(String nextPlayer);
}
