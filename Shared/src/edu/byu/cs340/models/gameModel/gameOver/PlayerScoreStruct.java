package edu.byu.cs340.models.gameModel.gameOver;

/**
 * Created by joews on 3/24/2017.
 */
public class PlayerScoreStruct
{
	public String mUsername;
	public int mPointsFromClaimedRoutes;
	public int mPointsFromLongestRoute;
	public int mPointsFromReachedDestinations;
	public int mPointsLostFromUnreachedDestinations;
	public int mTotalPoints;
	public int mDestCompleted;
	public int mLongestPath;

}
