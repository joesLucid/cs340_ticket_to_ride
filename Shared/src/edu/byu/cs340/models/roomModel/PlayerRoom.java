package edu.byu.cs340.models.roomModel;


import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.ChatMessenger;

import java.util.List;
import java.util.UUID;

/**
 * Created by joews on 2/1/2017.
 *
 * IPlayerRoom handles the adding and removing of mPlayers from the room. This is to be inherited
 * by the lobby and game classes.
 */
public abstract class PlayerRoom implements IPlayerRoom
{
	protected PlayerManager mPlayerManager;
	protected String mRoomName;
	protected String mRoomID;
	protected CommandManager mCommandManager;
	protected ChatMessenger mChatMessenger;

	public PlayerRoom()
	{
		this.mPlayerManager = createPlayerManager();
		this.mRoomName = "";
		this.mRoomID = "";
		this.mCommandManager = new CommandManager();
		this.mChatMessenger = new ChatMessenger();
	}

	/**
	 * Constructor for creating a new room
	 *
	 * @param roomName     what to name the room
	 * @param hostUsername the username of the user creating the room, becomes the host
	 */
	public PlayerRoom(String roomName, String hostUsername)
	{
		this.mPlayerManager = createPlayerManager(hostUsername);
		this.mRoomName = roomName;
		createRoomID();
		this.mCommandManager = new CommandManager();
		mCommandManager.addPlayer(hostUsername);
		this.mChatMessenger = new ChatMessenger();
	}

	protected abstract PlayerManager createPlayerManager();

	protected abstract PlayerManager createPlayerManager(String hostName);


	/**
	 * Abstract method that is implement by sub-class player rooms to add player. They will create the actual
	 * player object (Such as a gameplayer, or lobbyplayer etc) and add to the mPlayers edu.byu.cs340.models.gameModel.
	 *
	 * @param username the name of the user being added
	 * @return Returns a bool of whether the player could be added or not.
	 */
	public boolean addPlayer(String username)
	{
		boolean result = mPlayerManager.addPlayer(username);
		if (result)
		{
			mCommandManager.addPlayer(username);
		}
		return result;
	}

	/**
	 * Sets the host player username, which is associated with the host player through the edu.byu.cs340.models
	 * .gameModel
	 *
	 * @param hostUsername username of the player to set as the host
	 */
	public void setHost(String hostUsername)
	{
		mPlayerManager.setHost(hostUsername);
	}

	/**
	 * Removes player from the room.
	 *
	 * @param username Player to remove
	 * @return returns true if the user was removed, false otherwise.
	 *
	 * @pre user must be in room (getPlayer(user) != null)
	 */
	public boolean removePlayer(String username)
	{
		return mPlayerManager.removePlayer(username);
	}

	/**
	 * Returns the player associated with the username
	 *
	 * @param username Username of player to get
	 * @return Returns IPlayer associated with username or null if not in room
	 */
	public IPlayer getPlayer(String username)
	{
		return mPlayerManager.getPlayer(username);
	}

	/**
	 * Get value of RoomName
	 *
	 * @return Returns the value of RoomName
	 */
	public String getRoomName()
	{
		return mRoomName;
	}

	/**
	 * Returns the Unique ID of the room.
	 *
	 * @return the ID associated with this room
	 */
	public String getRoomID()
	{
		return mRoomID;
	}


	/**
	 * Creates a unique ID for the room and assigns the member to it
	 */
	private void createRoomID()
	{
		mRoomID = UUID.randomUUID().toString();
	}

	/**
	 * Returns a list of IPlayer in the room.
	 *
	 * @return The list of IPlayers (empty if no players)
	 */
	@Override
	public List<IPlayer> getPlayers()
	{
		return mPlayerManager.getPlayers();
	}

	/**
	 * @param username the name of the user to check
	 * @return whether the user is in this room
	 */
	public boolean hasPlayer(String username)
	{
		return username != null && mPlayerManager.hasPlayer(username);
	}

	/**
	 * @return whether the room has no players
	 */
	public boolean isEmpty()
	{
		return mPlayerManager.isEmpty();
	}

	/**
	 * Returns the index of the last command this room stored
	 */
	public int getLastCommand()
	{
		//TODO: Modify in the future to take in a player
		return mCommandManager.getLastCommandIndex(null);
	}

	/**
	 * Gets the index of hte last command this room stored for a given player.
	 *
	 * @param username Player to ask about
	 * @return The index of hte last command.
	 */
	public int getLastCommand(String username)
	{
		return mCommandManager.getLastCommandIndex(username);
	}

	/**
	 * Adds the given command to the list of commands in this room
	 */
	public boolean addCommand(IBaseCommand command)
	{
		if (command != null)
		{
			mCommandManager.addCommand(command);
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * @param lastCommand the index of the command from which to get the command list
	 * @return A list of IBaseCommand containing all commands executed in this room since lastCommand
	 * @pre userName!=null
	 * @post result != null
	 */
	public List<IBaseCommand> getRecentCommands(int lastCommand, String userName)
	{
		if (lastCommand < 0)
		{
			lastCommand = -1;
		}
		return mCommandManager.getCommandsSince(lastCommand, userName);
	}

	/**
	 * Set the value of roomName variable
	 *
	 * @param roomName The value to set roomName member variable to
	 */
	public void setRoomName(String roomName)
	{
		mRoomName = roomName;
	}

	/**
	 * Set the value of roomID variable
	 *
	 * @param roomID The value to set roomID member variable to
	 */
	public void setRoomID(String roomID)
	{
		mRoomID = roomID;
	}

	/**
	 * @return the username of the host of this room
	 */
	public String getHost()
	{
		return mPlayerManager.getHost();
	}

	/**
	 * @return the number of players in this room
	 */
	public int getNumPlayers()
	{
		return mPlayerManager.getNumPlayers();
	}

	/**
	 * Gets the player manager of the game.
	 *
	 * @return The player manager.
	 *
	 * @post result != null
	 */
	public PlayerManager getPlayerManager()
	{
		return mPlayerManager;
	}


	public List<ChatMessage> getChatMessages(long lastIndex)
	{
		return mChatMessenger.getNewMessages(lastIndex);
	}

	public long getLastIndex()
	{
		return mChatMessenger.getLastIndex();
	}

	public void addChatMessage(List<ChatMessage> messages)
	{
		for (ChatMessage messsage : messages)
		{
			mChatMessenger.addChatMessage(messsage);
		}
	}

	public void addChatMessage(ChatMessage message)
	{
		mChatMessenger.addChatMessage(message);
	}


}
