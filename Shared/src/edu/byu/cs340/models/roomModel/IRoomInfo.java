package edu.byu.cs340.models.roomModel;

import java.util.List;

/**
 * Created by tjacobhi on 04-Feb-17.
 *
 * This is used to return specific data to the UI about different available games.
 */
public interface IRoomInfo
{
	/**
	 * This should be a hidden value that is used to keep track of which game this is.
	 *
	 * @return the mId for the game
	 */
	String getRoomID();

	/**
	 * This is used to obtain the visible name for the game.
	 *
	 * @return the title for the game
	 */
	String getGameTitle();

	/**
	 * This is used to get the mPlayers connected to the game
	 *
	 * @return a list of mPlayers
	 */
	List<IPlayer> getPlayers();

	/**
	 * Updates the object with infromation from another instance of IRoomInfo with the same ID
	 *
	 * @param info the object to update this with
	 * @pre info.getRoomID() == this.getRoomID()
	 */
	void update(IRoomInfo info);
}
