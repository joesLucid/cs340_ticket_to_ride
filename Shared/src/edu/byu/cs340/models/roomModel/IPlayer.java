package edu.byu.cs340.models.roomModel;

/**
 * Created by joews on 2/1/2017.
 *
 * IPlayer class used in IPlayerRooms which stores the user and other relevant information
 * to the room.
 */
public interface IPlayer
{

	/**
	 * Get value of User
	 *
	 * @return Returns the value of User
	 */
	String getUsername();

	boolean isHost();

	void setIsHost(boolean isHost);


}
