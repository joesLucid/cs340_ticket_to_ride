package edu.byu.cs340.models.roomModel;

import java.util.List;

/**
 * Created by joews on 2/7/2017.
 *
 * Object to hold infromation about a room, namley roomID, title, and a list of players.
 */
public class RoomInfo implements IRoomInfo
{
	private String roomID;
	private String title;
	private List<IPlayer> players;

	public RoomInfo(String roomID, String title, List<IPlayer> players)
	{
		this.roomID = roomID;
		this.title = title;
		this.players = players;
	}

	public RoomInfo(IPlayerRoom room)
	{
		this.roomID = room.getRoomID();
		this.title = room.getRoomName();
		this.players = room.getPlayers();
	}

	/**
	 * This should be a hidden value that is used to keep track of which game this is.
	 *
	 * @return the mId for the game
	 */
	public String getRoomID()
	{
		return roomID;
	}

	/**
	 * This is used to obtain the visible name for the game.
	 *
	 * @return the title for the game
	 */
	@Override
	public String getGameTitle()
	{
		return title;
	}

	/**
	 * This is used to get the mPlayers connected to the game
	 *
	 * @return a list of mPlayers
	 */
	@Override
	public List<IPlayer> getPlayers()
	{
		return players;
	}

	/**
	 * Updates the object with infromation from another instance of IRoomInfo with the same ID
	 *
	 * @param info the object to update this with
	 * @pre info.getRoomID() == this.getRoomID()
	 */
	@Override
	public void update(IRoomInfo info)
	{
		if (this.roomID.equals(info.getRoomID()))
		{
			this.players = info.getPlayers();
			this.title = info.getGameTitle();
		}
	}
}
