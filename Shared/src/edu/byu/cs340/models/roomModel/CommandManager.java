package edu.byu.cs340.models.roomModel;

import edu.byu.cs340.command.IBaseCommand;

import java.util.*;

/**
 * Created by joews on 2/22/2017.
 *
 * Holds a list of commands in order by their index.
 */
public class CommandManager
{
	protected Map<String, List<IBaseCommand>> mUserCommandMap;
	protected List<IBaseCommand> mAllCommands;

	public CommandManager()
	{
		this.mUserCommandMap = new HashMap<>();
		this.mAllCommands = new ArrayList<>();
	}

	/**
	 * Adds a username to the edu.byu.cs340.models.gameModel of commands and starts tracking their commands.
	 *
	 * @param userName
	 */
	public void addPlayer(String userName)
	{
		if (userName != null && !mUserCommandMap.containsKey(userName))
		{
			List<IBaseCommand> cmds = new ArrayList<>();
			for (IBaseCommand cmd : mAllCommands)
			{
				//TODO: Figure out how stripping is working so we can get this part going...
				IBaseCommand strippedCommand = cmd.strip(userName);
				if (strippedCommand != null)
				{
					cmds.add(strippedCommand);
				}
			}
			Collections.sort(cmds);
			mUserCommandMap.put(userName, cmds);
		}
	}

	/**
	 * Adds the command to the command manager and formats for server and clients. Adds index to command
	 *
	 * @param cmd Command to add
	 * @pre cmd!=null
	 * @post cmd added to manager
	 * @post cmd.getIndex()!=-1, instead it is set ot index number in the server list.
	 */
	public void addCommand(IBaseCommand cmd)
	{
		if (cmd == null)
		{
			return;
		}

		cmd.setIndex(getLastCommandIndex(null) + 1);
		//add to server list
		mAllCommands.add(cmd);

		//add to client version
		for (Map.Entry<String, List<IBaseCommand>> pair : mUserCommandMap.entrySet())
		{
			//TODO: Figure out stripping
			IBaseCommand strippedCommand = cmd.strip(pair.getKey());
			if (strippedCommand != null)
			{
				pair.getValue().add(strippedCommand);
			}
		}
	}

	/**
	 * Returns a list of command sassociate with a user after a given index.
	 *
	 * @param lastIndex The index of the command last recieved
	 * @param useName   the username associated with the command list
	 * @return The list of commands that have happened since mLastIndex
	 *
	 * @pre mLastIndex >0
	 * @pre userName !=null
	 * @post the list of commands will be returned as unmodifiable since last index (or an mepty list if none)
	 */
	public List<IBaseCommand> getCommandsSince(int lastIndex, String useName)
	{
		List<IBaseCommand> cmds = mUserCommandMap.get(useName);
		if (cmds == null || cmds.size() == 0)
		{
			return new ArrayList<>();
		}
		Collections.sort(cmds);
		boolean indexFound = false;
		int indexInCommand = cmds.size() - 1;
		//iterate in reverse through commands untill we find one with an index less than the furrent
		while (indexInCommand >= 0 && !indexFound)
		{
			if (cmds.get(indexInCommand).getIndex() <= lastIndex)
			{
				//stop
				indexFound = true;
			} else
			{
				indexInCommand--;
			}
		}
		//increment once to compensate for the >= condition
		indexInCommand++;
		return Collections.unmodifiableList(cmds.subList(indexInCommand, cmds.size()));
	}

	/**
	 * gets the index of hte last command according to the server
	 *
	 * @param user the user to get commands associated with
	 * @return The index of the last command according to the server.
	 *
	 * @pre if user == null get last of all commands
	 * @pre user == null || user is in game
	 */
	public int getLastCommandIndex(String user)
	{
		//TODO: Modify in the future to take in a player
		if (user == null)
		{
			if (mAllCommands.size() > 0)
			{
				return mAllCommands.get(mAllCommands.size() - 1).getIndex();
			} else
			{
				return -1;
			}

		} else if (mUserCommandMap.containsKey(user))
		{
			if (mUserCommandMap.get(user).size() > 0)
			{
				return mUserCommandMap.get(user).get(mUserCommandMap.get(user).size() - 1).getIndex();
			} else
			{
				return -1;
			}
		} else
		{
			return -1;
		}
	}
}
