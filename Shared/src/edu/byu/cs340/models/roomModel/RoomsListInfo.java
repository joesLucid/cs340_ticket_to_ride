package edu.byu.cs340.models.roomModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 2/7/2017.
 *
 * Stores a list of IRoomInfo which represent the rooms currently available.
 */
public class RoomsListInfo implements IRoomsListInfo

{
	/**
	 * Map of roomID to IRoomInfo
	 */
	Map<String, IRoomInfo> mRoomInfos;

	public RoomsListInfo()
	{
		mRoomInfos = new HashMap<>();
	}

	/**
	 * Sets the mData in the object to mData
	 *
	 * @param data Data to set in object
	 * @pre mData != null
	 * @post mData internally = mData
	 */
	@Override
	public void setData(List<IRoomInfo> data)
	{
		mRoomInfos.clear();
		for (IRoomInfo info : data)
		{
			mRoomInfos.put(info.getRoomID(), info);
		}
	}


	/**
	 * Updates the mData in the object if it matches the room ID in IRoomInfo, otherwise it adds it.
	 *
	 * @param info IRoomInfo to add/update models with
	 * @pre info != null && info.getRoomID()!=null
	 * @post if mData contains info.getRoomID() update that object with info, otherwise add to mData
	 */
	@Override
	public void updateAddGame(IRoomInfo info)
	{
		if (mRoomInfos.containsKey(info.getRoomID()))
		{
			IRoomInfo room = mRoomInfos.get(info);
			room.update(info);
		} else
		{
			mRoomInfos.put(info.getRoomID(), info);
		}
	}

	/**
	 * Returns a list of all of the IRoomInfo's
	 *
	 * @return Returns a list of all of the IRoomInfo's
	 */
	@Override
	public List<IRoomInfo> getData()
	{
		return new ArrayList<>(mRoomInfos.values());
	}

//	/**
//	 * Updates the every room in the object with every room in mData. If they exist already it simply updates,
//	 * otherwise it adds.
//	 * @pre mData != null
//	 * @param mData mData to update models with
//	 */
//	@Override
//	public void updateData(IRoomsListInfo mData)
//	{
//		for (IRoomInfo info : mData.getData())
//		{
//			String gameID = info.getRoomID();
//			if (mRoomInfos.containsKey(gameID))
//			{
//				IRoomInfo toUpdate = mRoomInfos.get(gameID);
//				toUpdate.update(info);
//			} else
//			{
//				mRoomInfos.put(gameID, info);
//			}
//		}
//	}
}
