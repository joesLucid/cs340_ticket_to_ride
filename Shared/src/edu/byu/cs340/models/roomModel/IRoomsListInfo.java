package edu.byu.cs340.models.roomModel;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * Interface for list of IRoomInfo.
 */
public interface IRoomsListInfo
{
	/**
	 * Sets the mData in the object to mData
	 *
	 * @param data Data to set in object
	 * @pre data != null
	 * @post data internally = data
	 */
	void setData(List<IRoomInfo> data);

	/**
	 * Updates teh data in the object if it matches the room ID in IRoomIndo, otherwise it adds it.
	 *
	 * @param info IRoomInfo to add/update models with
	 * @pre info != null && info.getRoomID()!=null
	 * @post if mData contains info.getRoomID() update that object with info, otherwise add to mData
	 */
	void updateAddGame(IRoomInfo info);

	/**
	 * Returns a list of all of the IRoomInfo's
	 *
	 * @return Returns a list of all of the IRoomInfo's
	 */
	List<IRoomInfo> getData();

//	/**
//	 * Updates the every room in the object with every room in data. If they exist already it simply updates,
//	 * otherwise it adds.
//	 * @pre data != null
//	 * @param mData mData to update models with
//	 */
//	public void updateData(IRoomsListInfo data);
}
