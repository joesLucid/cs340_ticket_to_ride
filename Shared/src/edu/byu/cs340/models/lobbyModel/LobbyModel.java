package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.models.ChatMessenger;
import edu.byu.cs340.models.roomModel.PlayerManager;
import edu.byu.cs340.models.roomModel.PlayerRoom;

/**
 * Created by joews on 2/1/2017.
 *
 * Base class for the lobby models. Shared code between the client and server version goes here. This contains
 * functionality such as to ready or unready a player.
 */
public abstract class LobbyModel extends PlayerRoom
{
	public LobbyModel()
	{
		super();
	}

	public LobbyModel(String roomName, String hostUsername)
	{
		super(roomName, hostUsername);
	}

	/**
	 * Overrides the getPlayer functionality by casting to a LobbyPlayer.
	 *
	 * @param username the name of the player to get
	 * @return Returns the player casted as a LobbyPlayer
	 */
	@Override
	public LobbyPlayer getPlayer(String username)
	{
		return (LobbyPlayer) super.getPlayer(username);
	}


	/**
	 * Mark the player associated with the user as ready if they exist.
	 *
	 * @param user The user to ready up
	 * @return True if player readied up, false otherwise.
	 */
	public boolean readyUser(String user)
	{
		return ((LobbyPlayerManager) mPlayerManager).readyUser(user);
	}

	/**
	 * Mark the player associated with the user as not ready if they exist.
	 *
	 * @param user The user to ready up
	 * @return True if player was unreadies, false otherwise.
	 */
	public boolean unReadyUser(String user)
	{

		return ((LobbyPlayerManager) mPlayerManager).unReadyUser(user);
	}

	/**
	 * Checks if players are ready to start game
	 *
	 * @return whether all players in the lobby are ready
	 */
	public boolean isAllReady()
	{
		return ((LobbyPlayerManager) mPlayerManager).isAllReady();
	}

	@Override
	protected PlayerManager createPlayerManager()
	{
		return new LobbyPlayerManager();
	}

	@Override
	protected PlayerManager createPlayerManager(String hostName)
	{
		return new LobbyPlayerManager(hostName);
	}

	public ChatMessenger getChatMessenger()
	{
		return mChatMessenger;
	}
}
