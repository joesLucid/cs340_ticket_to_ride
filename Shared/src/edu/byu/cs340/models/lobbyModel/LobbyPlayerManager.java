package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.models.roomModel.IPlayer;
import edu.byu.cs340.models.roomModel.PlayerManager;

import java.util.Map;

/**
 * Created by joews on 2/22/2017.
 */
public class LobbyPlayerManager extends PlayerManager
{
	public LobbyPlayerManager()
	{
		super();
	}

	public LobbyPlayerManager(String hostName)
	{
		super(hostName);
	}

	@Override
	protected IPlayer createPlayer(String playerName)
	{
		if (playerName != null)
		{
			return new LobbyPlayer(playerName);
		} else
		{
			return null;
		}
	}

	/**
	 * Overrides the getPlayer functionality by casting to a LobbyPlayer.
	 *
	 * @param username the name of the player to get
	 * @return Returns the player casted as a LobbyPlayer
	 */
	@Override
	public LobbyPlayer getPlayer(String username)
	{
		return (LobbyPlayer) super.getPlayer(username);
	}


	/**
	 * Mark the player associated with the user as ready if they exist.
	 *
	 * @param user The user to ready up
	 * @return True if player readied up, false otherwise.
	 */
	public boolean readyUser(String user)
	{
		if (mPlayers.containsKey(user))
		{
			LobbyPlayer player = getPlayer(user);
			player.setReady(true);
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Mark the player associated with the user as not ready if they exist.
	 *
	 * @param user The user to ready up
	 * @return True if player was unreadies, false otherwise.
	 */
	public boolean unReadyUser(String user)
	{
		LobbyPlayer player = getPlayer(user);
		if (player != null)
		{
			player.setReady(false);
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Checks if players are ready to start game
	 *
	 * @return whether all players in the lobby are ready
	 */
	public boolean isAllReady()
	{
		for (Map.Entry<String, IPlayer> entry : mPlayers.entrySet())
		{
			ILobbyPlayer player = (ILobbyPlayer) entry.getValue();
			if (!player.isReady())
			{
				return false;
			}
		}
		return true;
	}
}
