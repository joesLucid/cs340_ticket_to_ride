package edu.byu.cs340.models;

import java.util.*;

/**
 * Created by Rebekah on 2/15/2017.
 * this class handles the collection of messages that users use to communicate
 */
public class ChatMessenger implements IChatMessenger
{
	protected Set<ChatMessage> mMessages;
	protected long mLastIndex = -1;

	public ChatMessenger()
	{
		mMessages = new TreeSet<>();
	}

	@Override
	public void addChatMessage(ChatMessage message)
	{
		if (mMessages == null || !(mMessages instanceof TreeSet))
		{
			mMessages = new TreeSet<>();
		}
		if (message.getIndex() > -1)
		{
			mLastIndex = Math.max(message.getIndex(), mLastIndex);
		} else
		{
			mLastIndex = message.getIndex();
			message.setIndex(mLastIndex);
		}
		mMessages.add(new ChatMessage(message));
	}
	@Override
	public List<ChatMessage> getNewMessages(long lastIndex)
	{
		Deque<ChatMessage> newMessages = new ArrayDeque<>();
		Iterator<ChatMessage> iterator = mMessages.iterator();
		while (iterator.hasNext())
		{
			ChatMessage msg = iterator.next();
			if (msg.getIndex() > lastIndex)
			{
				newMessages.push(msg);
			} else
			{
				break;
			}
		}
		return new ArrayList<>(newMessages);
	}

	public long getLastIndex()
	{
		if (mMessages == null || mMessages.size() == 0)
		{
			return -1;
		} else
		{
			return mLastIndex;
		}
	}

}
