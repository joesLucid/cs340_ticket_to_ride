package edu.byu.cs340.models;

import java.util.Calendar;

/**
 * Created by Rebekah on 2/13/2017.
 * this class holds the chat messages sent by users to each other.
 *
 */
public class ChatMessage implements IChatMessage, Comparable{
	private long mIndex = -1;
	private String mUserName;
    private String mMessage;

    /**
     * Gets the class name for serializetion.
     */
    protected final String mClassName = getClass().getSimpleName();
    /**
     * gets the package name for serialization
     */
    protected final String mClassPackage = getClass().getPackage().getName();

    // 1) create a java calendar instance
    private Calendar calendar = Calendar.getInstance();

    /**
     * @pre assumes the message is a well formatted string and that the username is correct
     * @param userName the name of the user sending the message
     * @param message the text being sent by the user to communicate
     * @post the message will be timestamped and a ChatMessage object will be created
     */
    public ChatMessage(String userName, String message)
    {
        this.mUserName = userName;
        this.mMessage = message;
        // 2) get a java.util.Date from the calendar instance.
        //    this date will represent the current instant, or "now".
//        java.util.Date now = calendar.getTime();

        // 3) a java current time (now) instance
//        mTimestamp = new java.sql.Timestamp(now.getTime());
//
    }

	public ChatMessage(ChatMessage that)
	{
		this.mUserName = that.mUserName;
		this.mMessage = that.mMessage;
		this.mIndex = that.mIndex;
	}

    public String getMessage() {
        return mMessage;
    }

	public long getIndex()
	{
		return mIndex;
	}

	public void setIndex(long index)
	{
		mIndex = index;
	}

    public String getUserName() {
        return mUserName;
    }

    @Override
    public int compareTo(Object o) {
        ChatMessage other = (ChatMessage) o;
	    return new Long(other.mIndex).compareTo(mIndex);
    }
}
