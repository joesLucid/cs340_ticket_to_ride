package edu.byu.cs340.models;

/**
 * Created by joews on 2/1/2017.
 *
 * Object representing a user only containing a username.  Used to hide password and other information about a user.
 */
public class BaseUser
{
	protected String mUserName;

	public BaseUser(String userName)
	{
		mUserName = userName;
	}

	/**
	 * Get value of UserName
	 *
	 * @return Returns the value of UserName
	 */
	public String getUserName()
	{
		return mUserName;
	}

	/**
	 * Set the value of userName variable
	 *
	 * @param userName The value to set userName member variable to
	 */
	public void setUserName(String userName)
	{
		mUserName = userName;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BaseUser baseUser = (BaseUser) o;
		return !(mUserName == null || baseUser.mUserName == null) && mUserName.equals(baseUser.mUserName);
	}

	@Override
	public int hashCode()
	{
		return mUserName.hashCode();
	}
}
