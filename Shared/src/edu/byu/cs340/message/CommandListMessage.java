package edu.byu.cs340.message;

import edu.byu.cs340.command.IBaseCommand;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * A message containing a list of commands.
 */
public class CommandListMessage extends BaseMessage
{

	private List<String> mSerializedCommands;

	public <T extends IBaseCommand> CommandListMessage(List<String> serializedCommands)
	{
		this.mSerializedCommands = serializedCommands;
	}

	/**
	 * Get value of SerializedCommands
	 *
	 * @return Returns the value of SerializedCommands
	 */
	public List<String> getSerializedCommands()
	{
		return mSerializedCommands;
	}

	/**
	 * Set the value of serializedCommands variable
	 *
	 * @param serializedCommands The value to set serializedCommands member variable to
	 */
	public void setSerializedCommands(List<String> serializedCommands)
	{
		mSerializedCommands = serializedCommands;
	}
}
