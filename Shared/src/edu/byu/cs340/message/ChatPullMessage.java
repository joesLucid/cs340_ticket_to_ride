package edu.byu.cs340.message;

import edu.byu.cs340.models.ChatMessage;

import java.util.List;

/**
 * Created by joews on 3/6/2017.
 */
public class ChatPullMessage extends BaseMessage
{

	protected List<ChatMessage> mChatMessages;
	protected String mRoomId;
	protected long mLastMessage;
	protected String mUsername;


	public ChatPullMessage(String roomId, long lastMessage, String username)
	{
		mRoomId = roomId;
		mLastMessage = lastMessage;
		mUsername = username;
	}

	/**
	 * Get value of ChatMessages
	 *
	 * @return Returns the value of ChatMessages
	 */
	public List<ChatMessage> getChatMessages()
	{
		return mChatMessages;
	}

	/**
	 * Set the value of chatMessages variable
	 *
	 * @param chatMessages The value to set chatMessages member variable to
	 */
	public void setChatMessages(List<ChatMessage> chatMessages)
	{
		mChatMessages = chatMessages;
	}

	/**
	 * Get value of RoomId
	 *
	 * @return Returns the value of RoomId
	 */
	public String getRoomId()
	{
		return mRoomId;
	}

	/**
	 * Set the value of roomId variable
	 *
	 * @param roomId The value to set roomId member variable to
	 */
	public void setRoomId(String roomId)
	{
		mRoomId = roomId;
	}

	/**
	 * Get value of LastMessage
	 *
	 * @return Returns the value of LastMessage
	 */
	public long getLastMessage()
	{
		return mLastMessage;
	}

	/**
	 * Set the value of lastMessage variable
	 *
	 * @param lastMessage The value to set lastMessage member variable to
	 */
	public void setLastMessage(long lastMessage)
	{
		mLastMessage = lastMessage;
	}

	/**
	 * Get value of Username
	 *
	 * @return Returns the value of Username
	 */
	public String getUsername()
	{
		return mUsername;
	}

	/**
	 * Set the value of username variable
	 *
	 * @param username The value to set username member variable to
	 */
	public void setUsername(String username)
	{
		mUsername = username;
	}
}
