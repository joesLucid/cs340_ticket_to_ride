package edu.byu.cs340;

/**
 * Created by joews on 2/6/2017.
 */
public class Utils
{
	public static final String GAME_TITLE = "Ticket to Ride";

	public static final int MIN_USERNAME_LENGTH = 6;
	public static final int MAX_USERNAME_LENGTH = 15;
	public static final int MIN_PASSWORD_LENGTH = 6;
	public static final int MAX_PASSWORD_LENGTH = 32;
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String AUTHORIZATION_OF_REQUEST_FAILED = "Unable to Authenticate User. User may be signed into" +
			" another client.";
	public static final String REQUEST_COULD_NOT_BE_DESERIALIZED_ON_SERVER = "Request could not be deserialized on " +
			"server";


	/**
	 * Checks to see if the username is null, the correct length, and alpha numeric.
	 *
	 * @param username Username to test
	 * @return Return true if the username is valid, false otherwise
	 * @pre none
	 * @post returns result
	 */
	public static boolean isUsernameValid(String username)
	{
		return username != null && username.length() >= Utils.MIN_USERNAME_LENGTH && username.length() <= Utils
				.MAX_USERNAME_LENGTH && username.matches("[A-Za-z0-9]+");
	}

	/**
	 * Checks to see if the password is null, the correct length, and alpha numeric.
	 *
	 * @pre none
	 * @post returns result
	 * @param password Password to test
	 * @return Return true if the password is valid, false otherwise
	 */
	public static boolean isPasswordValid(String password)
	{
		return password != null && password.length() >= Utils.MIN_PASSWORD_LENGTH && password.length() <= Utils
				.MAX_PASSWORD_LENGTH && password.matches("[A-Z\\-a-z. ?_0-9@&$!()*%,~`\\[\\]:;+=\"']+");
	}


}
