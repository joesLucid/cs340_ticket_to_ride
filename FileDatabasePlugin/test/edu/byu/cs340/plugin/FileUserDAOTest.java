package edu.byu.cs340.plugin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import edu.byu.cs340.UserDTO;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by Sean on 4/13/2017.
 */
class FileUserDAOTest {
	
	
	@Test
	public void ConstructorTest(){
		FileUserDAO dao = new FileUserDAO();

		String path = FileUserDAO.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = "";
		String FILEAPPENDIX = "filedatabase/";
		String USERFILEPATH = "users.json";
		try
		{
			decodedPath = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e)
		{
			System.err.println("If this happens, cry");
		}
		String fullPath = decodedPath + FILEAPPENDIX + USERFILEPATH;

		File file = new File(fullPath);
		assertTrue(file.exists());
		try (Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fullPath))))
		{
			Gson gson = new Gson();
			assertEquals(new JsonArray(), gson.fromJson(reader, JsonArray.class));
		} catch (FileNotFoundException e){
			System.err.println("lol");
		} catch (IOException e){
			System.err.println("lolol");
		}
	}
	
	/*@Test
	public void AddSomethingTest(){
		File file = new File("dummy.json");
			/*
		try
		{
			if (!file.createNewFile()){
				fail("derp");
			}
		} catch (IOException e){
			System.err.println("lol");
		}
			/
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("dummy.json")))){
			Gson gson = new Gson();
			JsonWriter jsonWriter = gson.newJsonWriter(writer);
			jsonWriter.beginArray();
			
			jsonWriter.beginObject();
			jsonWriter.name("name").value(42).endObject();
			jsonWriter.beginObject();
			jsonWriter.name("name").value(42).endObject();
			
			jsonWriter.endArray();
			
			
		} catch (FileNotFoundException e){
			System.err.println("lol");
		} catch (IOException e){
			System.err.println("lolol");
		}
	}
	
	@Test
	public void AddNoOverwrite(){
		try {
			Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream("dummy.json")));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();
			JsonWriter writer = new JsonWriter(new FileWriter(new File("dummy.json")));
			array.add(false);
			
			gson.toJson(array, writer);
			System.out.println(gson.toJson(array));
			writer.close();
			
		} catch (FileNotFoundException e){
			System.err.println("lol");
			e.printStackTrace();
			fail("Exception");
		} catch (IOException e){
			System.err.println("lolol");
			e.printStackTrace();
			fail("Exception");
		}
	}
	*/
	
	@Test
	public void SetUserTest(){
		FileUserDAO dao = new FileUserDAO();
		
		dao.setUser(new UserDTO("Jacob", 2, "sneeze"));
		
		dao.setUser(new UserDTO("Sean", 25, "test"));
		
		dao.setUser(new UserDTO("Jacob", 6, "I wasn't ready"));
	}
	
	@Test
	public void GetUsersTest(){
		FileUserDAO dao = new FileUserDAO();
		
		List<UserDTO> list = new ArrayList<>();
		UserDTO dto1 = new UserDTO("Jacob", 2, "sneeze");
		UserDTO dto2 = new UserDTO("Sean", 25, "test");
		list.add(dto1);
		list.add(dto2);
		
		dao.setUser(dto1);
		
		dao.setUser(dto2);
	}
	
	@Test
	public void MapTest(){
		FileUserDAO dao = new FileUserDAO();
		
		Map<String, String> map = new HashMap<>();
		map.put("Jacob", "Ready");
		map.put("Ashwood", "Ready");
		map.put("Greymoss", "Not");
		
		dao.setUsernameToRoomIDMap(map);
		Map<String, String> resultantMap = dao.getUsernameToRoomIDMap();
		assertEquals(map, resultantMap);
		for (String s : map.keySet())
		{
			String res1 = map.get(s);
			String res2 = resultantMap.get(s);
			assertTrue(res1.equals(res2));
		}
	}
}