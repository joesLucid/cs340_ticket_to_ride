package edu.byu.cs340.plugin;

import edu.byu.cs340.IGameDAO;

import java.util.List;

/**
 * Created by Sean on 4/12/2017.
 * <p>
 * DAO for accessing Game data
 */
public class FileGameDAO implements IGameDAO
{
	private final String GAMETOFILENAME = "gamestofiles";
	private final String FILENAME_PREFIX = "game_";
	private FileRoomDAO mFileRoomDAO;

	public FileGameDAO()
	{
		mFileRoomDAO = new FileRoomDAO(GAMETOFILENAME, FILENAME_PREFIX);
	}

	@Override
	public void setGame(String GameID, String serializedGame)
	{
		mFileRoomDAO.setRoom(GameID, serializedGame);
	}

	@Override
	public void addCommandToGame(String GameID, String serializedCmd)
	{
		mFileRoomDAO.addCommandToRoom(GameID, serializedCmd);
	}

	@Override
	public List<String> getGames()
	{
		return mFileRoomDAO.getRooms();
	}

	@Override
	public String getGame(String GameID)
	{
		return mFileRoomDAO.getRoom(GameID);
	}

	@Override
	public List<String> getGameCommands(String GameID)
	{
		return mFileRoomDAO.getRoomCommands(GameID);
	}

	@Override
	public void removeGame(String GameID)
	{
		mFileRoomDAO.removeRoom(GameID);

	}

	@Override
	public void clearDAO()
	{
		mFileRoomDAO.clearDAO();

	}
}
