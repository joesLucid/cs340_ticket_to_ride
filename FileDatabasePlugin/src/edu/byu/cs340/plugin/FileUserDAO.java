package edu.byu.cs340.plugin;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import edu.byu.cs340.IUserDAO;
import edu.byu.cs340.UserDTO;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sean on 4/12/2017.
 *
 * DAO for the file system to access users
 */
public class FileUserDAO implements IUserDAO
{
	private final String FILEAPPENDIX = "filedatabase/";
	private final String USERFILEPATH = "users.json";
	private final String MAPFILEPATH = "usergamemap.json";
	private String decodedPath;

	public FileUserDAO()
	{
		//String path = FileUserDAO.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		decodedPath = System.getProperty("user.dir") + "/";

		File map = new File(decodedPath + FILEAPPENDIX + MAPFILEPATH);
		if (!map.exists())
		{
			map.getParentFile().mkdirs();
			try
			{
				map.createNewFile();
				try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
						decodedPath + FILEAPPENDIX + MAPFILEPATH))))
				{
					Gson gson = new Gson();
					JsonElement emptyArray = new JsonArray();
					JsonWriter jsonWriter = gson.newJsonWriter(writer);
					gson.toJson(emptyArray, jsonWriter);
					writer.close();
				} catch (FileNotFoundException e)
				{
					System.err.println("User map file not found");
				} catch (IOException e)
				{
					System.err.println("IOException when opening user file");
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}

		}
		File file = new File(decodedPath + FILEAPPENDIX + USERFILEPATH);
		if (!file.exists())
		{
			file.getParentFile().mkdirs();
			try
			{
				file.createNewFile();
				try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
						decodedPath + FILEAPPENDIX + USERFILEPATH))))
				{
					Gson gson = new Gson();
					JsonElement emptyArray = new JsonArray();
					JsonWriter jsonWriter = gson.newJsonWriter(writer);
					gson.toJson(emptyArray, jsonWriter);
					writer.close();
				} catch (FileNotFoundException e)
				{
					System.err.println("User file not found");
				} catch (IOException e)
				{
					System.err.println("IOException when opening user file");
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setUser(UserDTO userData)
	{
		try
		{
			Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
					decodedPath + FILEAPPENDIX + USERFILEPATH)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();
			if (array == null)
			{
				array = new JsonArray();
			}
			for (JsonElement obj : array)
			{
				if (obj.getAsJsonObject().get("username").getAsString().equals(userData.getUsername()))
				{
					// change details of found user
					obj.getAsJsonObject().addProperty("username", userData.getUsername());
					obj.getAsJsonObject().addProperty("passhash", userData.getPasshash());
					obj.getAsJsonObject().addProperty("token", userData.getAuthenticationToken());

					// write the array back to the file
					JsonWriter writer = new JsonWriter(new FileWriter(new File(decodedPath + FILEAPPENDIX +
							USERFILEPATH)));

					gson.toJson(array, writer);
					writer.close();
					return;
				}
			}


			// create new object for the user
			JsonObject obj = new JsonObject();
			obj.addProperty("username", userData.getUsername());
			obj.addProperty("passhash", userData.getPasshash());
			obj.addProperty("token", userData.getAuthenticationToken());
			array.add(obj);

			// write the array back to the file
			JsonWriter writer = new JsonWriter(new FileWriter(new File(decodedPath + FILEAPPENDIX + USERFILEPATH)));
			gson.toJson(array, writer);
			writer.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found when setting user");
		} catch (IOException e)
		{
			System.err.println("IOException when setting user");
		}
	}

	@Override
	public List<UserDTO> getUsers()
	{
		List<UserDTO> users = new ArrayList<>();
		try
		{
			Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
					decodedPath + FILEAPPENDIX + USERFILEPATH)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();
			if (array != null)
			{
				for (JsonElement obj : array)
				{
					JsonObject jobj = obj.getAsJsonObject();
					users.add(new UserDTO(jobj.getAsJsonPrimitive("username").getAsString(),
							jobj.getAsJsonPrimitive("passhash").getAsInt(),
							jobj.getAsJsonPrimitive("token").getAsString()));
				}
			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found when setting user");
		} catch (IOException e)
		{
			System.err.println("IOException when setting user");
		}
		return users;
	}

	@Override
	public Map<String, String> getUsernameToRoomIDMap()
	{
		Gson gson = new GsonBuilder().create();
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
					decodedPath + FILEAPPENDIX + MAPFILEPATH)));
			Type stringStringMap = new TypeToken<Map<String, String>>()
			{
			}.getType();
			Map<String, String> map = gson.fromJson(reader, stringStringMap);
			reader.close();
			return map;
		} catch (FileNotFoundException e)
		{
			System.err.println("Could not find " + decodedPath + FILEAPPENDIX + MAPFILEPATH);
		} catch (IOException e)
		{
			System.err.println("IOExcpetion when writing to user map");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void setUsernameToRoomIDMap(Map<String, String> map)
	{
		Gson gson = new GsonBuilder().create();
		JsonWriter writer = null;
		try
		{
			writer = new JsonWriter(new FileWriter(new File(decodedPath + FILEAPPENDIX + MAPFILEPATH)));
			Type stringStringMap = new TypeToken<Map<String, String>>()
			{
			}.getType();
			String res = gson.toJson(map);
			gson.toJson(map, Map.class, writer);
			writer.close();
		} catch (FileNotFoundException e)
		{
			System.err.println("Could not find " + decodedPath + FILEAPPENDIX + MAPFILEPATH);
		} catch (IOException e)
		{
			System.err.println("IOExcpetion when writing to user map");
		}
	}

	@Override
	public void clearDAO()
	{
		File file = new File(decodedPath + FILEAPPENDIX + MAPFILEPATH);
		if (file.exists())
		{
			file.delete();
			try
			{
				file.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		file = new File(decodedPath + FILEAPPENDIX + USERFILEPATH);
		if (file.exists())
		{
			file.delete();
			try
			{
				file.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
