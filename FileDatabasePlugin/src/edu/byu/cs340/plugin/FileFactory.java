package edu.byu.cs340.plugin;

import edu.byu.cs340.IAbstractFactory;
import edu.byu.cs340.IGameDAO;
import edu.byu.cs340.ILobbyDAO;
import edu.byu.cs340.IUserDAO;

/**
 * Created by Sean on 4/17/2017.
 */
public class FileFactory implements IAbstractFactory {
	@Override
	public IGameDAO createGameDAO()
	{
		return new FileGameDAO();
	}
	
	@Override
	public IUserDAO createUserDAO()
	{
		return new FileUserDAO();
	}
	
	@Override
	public ILobbyDAO createLobbyDAO()
	{
		return new FileLobbyDAO();
	}
}
