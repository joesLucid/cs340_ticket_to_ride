package edu.byu.cs340.plugin;

import edu.byu.cs340.ILobbyDAO;

import java.util.List;

/**
 * Created by Sean on 4/12/2017.
 * <p>
 * DAO for accessing Game data
 */
public class FileLobbyDAO implements ILobbyDAO
{
	private final String LOBBYTOFILENAME = "lobbiestofiles";
	private final String FILENAME_PREFIX = "lobby_";
	private FileRoomDAO mFileRoomDAO;

	public FileLobbyDAO()
	{
		mFileRoomDAO = new FileRoomDAO(LOBBYTOFILENAME, FILENAME_PREFIX);
	}

	@Override
	public void setLobby(String lobbyID, String serializedLobby)
	{
		mFileRoomDAO.setRoom(lobbyID, serializedLobby);
	}

	@Override
	public void addCommandToLobby(String lobbyID, String serializedCmd)
	{
		mFileRoomDAO.addCommandToRoom(lobbyID, serializedCmd);
	}

	@Override
	public List<String> getLobbies()
	{
		return mFileRoomDAO.getRooms();
	}

	@Override
	public String getLobby(String lobbyID)
	{
		return mFileRoomDAO.getRoom(lobbyID);
	}

	@Override
	public List<String> getLobbyCommands(String lobbyID)
	{
		return mFileRoomDAO.getRoomCommands(lobbyID);
	}

	@Override
	public void removeLobby(String lobbyID)
	{
		mFileRoomDAO.removeRoom(lobbyID);

	}

	@Override
	public void clearDAO()
	{
		mFileRoomDAO.clearDAO();

	}
}
