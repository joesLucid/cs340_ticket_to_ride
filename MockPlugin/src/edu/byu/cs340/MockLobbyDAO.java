package edu.byu.cs340;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 4/12/2017.
 */
public class MockLobbyDAO implements ILobbyDAO
{

	private Map<String, String> data;
	private Map<String, List<String>> cmdMap;

	public MockLobbyDAO()
	{
		data = new HashMap<>();
		cmdMap = new HashMap<>();
	}


	@Override
	public void setLobby(String lobbyID, String serializedLobby)
	{
		data.put(lobbyID, serializedLobby);
		if (cmdMap.containsKey(lobbyID))
		{
			cmdMap.remove(lobbyID);
		}
	}

	@Override
	public void addCommandToLobby(String lobbyID, String serializedCmd)
	{
		if (cmdMap.containsKey(lobbyID))
		{
			cmdMap.get(lobbyID).add(serializedCmd);
		} else
		{
			List<String> temp = new ArrayList<>();
			temp.add(serializedCmd);
			cmdMap.put(lobbyID, temp);
		}
	}

	@Override
	public List<String> getLobbies()
	{
		return new ArrayList<>(data.values());
	}

	@Override
	public String getLobby(String lobbyID)
	{

		return data.get(lobbyID);
	}

	@Override
	public List<String> getLobbyCommands(String lobbyID)
	{
		return cmdMap.get(lobbyID);
	}

	@Override
	public void removeLobby(String LobbyID)
	{
		data.remove(LobbyID);
	}

	@Override
	public void clearDAO()
	{
		cmdMap.clear();
		data.clear();
	}
}
