package edu.byu.cs340;

/**
 * Created by joews on 4/12/2017.
 */
public class MockAbstractFactory implements IAbstractFactory
{
	public MockAbstractFactory()
	{
	}

	@Override
	public IGameDAO createGameDAO()
	{
		return new MockGameDAO();
	}

	@Override
	public IUserDAO createUserDAO()
	{
		return new MockUserDAO();
	}

	@Override
	public ILobbyDAO createLobbyDAO()
	{
		return new MockLobbyDAO();
	}
}
