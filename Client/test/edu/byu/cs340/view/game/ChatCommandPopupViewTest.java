package edu.byu.cs340.view.game;

import edu.byu.cs340.communication.Poller;
import edu.byu.cs340.presenter.game.ChatCommandPopupPresenter;
import edu.byu.cs340.presenter.game.GamePresenterProxy;

/**
 * Created by Rebekah on 3/8/2017.
 */
class ChatCommandPopupViewTest
{

	private Poller mPoller;
	private GamePresenterProxy mPresenterProxy;
	private ChatCommandPopupPresenter mPopupPresenter;

//	@org.junit.jupiter.api.BeforeEach
//	void setUp()
//	{
//		ClientFacade.getInstance(ClientFacade.class).setUp();
//		mPresenterProxy = new GamePresenterProxy();
//		mPopupPresenter = new ChatCommandPopupPresenter(mPresenterProxy);
//		mPopupPresenter.activate();
//	}
//
//	@org.junit.jupiter.api.AfterEach
//	void tearDown()
//	{
//		mPoller.stopPolling();
//		mPopupPresenter.deactivate();
//		mPresenterProxy.deactivate();
//	}
//
//	@org.junit.jupiter.api.Test
//	void pushChatMessage()
//	{
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("bob", "\t\n" +
//				"A man flying in a hot air balloon suddenly realizes he’s lost." +
//				" He reduces height and spots a man down below. " +
//				"He lowers the balloon further and shouts to get directions," +
//				" \"Excuse me, can you tell me where I am?\"\n" +
//				"\n");
//		mPopupPresenter.updateChatMessages();
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage( "fran" ,
//				"The man below says: \"Yes. You're in a hot air balloon," +
//						" hovering 30 feet above this field.\"\n" );
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage( "bob4chan",
//				"\"You must work in Information Technology,\" says the balloonist.\n");
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("4ran",
//				"\"I do\" replies the man. \"How did you know?\"\n" );
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("b o b",
//				"\"Well,\" says the balloonist, \"everything you have told me is technically correct," +
//						" but It's of no use to anyone.\"\n");
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("4&", "");
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("FRAN",
//				"The man below replies, \"You must work in management.\"\n");
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage("",
//				"\"I do,\" replies the balloonist, *\"But how'd you know?\"**\n");
//		((ChatCommandPopupView) mPopupPresenter.getView()).pushChatMessage( "F an",
//				"\"Well\", says the man, \"you don’t know where you are or where you’re going," +
//						" but you expect me to be able to help." +
//						" You’re in the same position you were before we met, but now it’s my fault.\"");
//		mPopupPresenter.updateChatMessages();
//
//	}
//
//	@org.junit.jupiter.api.Test
//	void initialize()
//	{
//
//	}
//
//	@org.junit.jupiter.api.Test
//	void onActivate()
//	{
//		mPopupPresenter.getView().onActivate();
//		String view = "ChatCommandPopupView";
//		String name = ((ChatCommandPopupView) mPopupPresenter.getView()).getViewName();
//		assert view.equals(name);
//	}
//
//	@org.junit.jupiter.api.Test
//	void getJPanel()
//	{
//		JPanel thispanel = mPopupPresenter.getView().getJPanel();
//		thispanel.setBackground(Color.blue);
//		assert thispanel.getBackground() == Color.blue;
//		thispanel.setEnabled(false);
//		assert  thispanel.isEnabled() == false;
//		thispanel.setEnabled(true);
//		assert thispanel.isEnabled() == true;
//	}
//
//	@org.junit.jupiter.api.Test
//	void getMinSize()
//	{
//		mPopupPresenter.getView().onActivate();
//		Dimension b = new Dimension(600, 450);
//		Dimension a = ((ChatCommandPopupView) mPopupPresenter.getView()).getMinSize();
//		assert a.equals(b);
//	}

}