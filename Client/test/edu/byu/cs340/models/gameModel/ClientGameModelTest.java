package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.*;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Rebekah on 3/25/2017.
 */
class ClientGameModelTest {

    private ClientGameModel mClientGameModel;
    private List<ClientTrainCard> mTrainCards;
    private List<ClientDestinationCard> mDestinationCards;
    private List<ICity> mCities;
    private List<IRoute> mRoutes;

    private List<String> mPlayerNames;
    private List<TrainType> mTrainCardTypes;
    private String mRoomName;
    private List<String> mCityNames;

    private ClientLobbyModel mLobbyModel;

    //Sets up the whole server gam models
    @BeforeEach
    void setUp()
    {
        if (mClientGameModel == null)
        {
            rebuildGameModel();
        }
    }

    /**
     * rebuild the game models for tests
     */
    private void rebuildGameModel()
    {

        //create a lobby to initializez it.
        mPlayerNames = new ArrayList<>();
        mPlayerNames.add("joeStack");
        mPlayerNames.add("LadyGray");
        mPlayerNames.add("Cassandra");
        mRoomName = "myRoom";

        mLobbyModel = new ClientLobbyModel("id",mRoomName, mPlayerNames.get(0));
        mLobbyModel.addPlayer(mPlayerNames.get(1));
        mLobbyModel.readyUser(mPlayerNames.get(1));
        mLobbyModel.addPlayer(mPlayerNames.get(2));
        mLobbyModel.readyUser(mPlayerNames.get(2));


        //create train cards
        mTrainCardTypes = new ArrayList<>();
        mTrainCards = new ArrayList<>();
        //ensure the first two cards are reefers
        mTrainCardTypes.add(TrainType.Reefer);
        mTrainCardTypes.add(TrainType.Reefer);
        mTrainCardTypes.add(TrainType.Locomotive);
        mTrainCards.add(new ClientTrainCard(TrainType.Reefer));
        mTrainCards.add(new ClientTrainCard(TrainType.Reefer));
        mTrainCards.add(new ClientTrainCard(TrainType.Locomotive));

        for (int i = 0; i < 2; i++)
        {
            mTrainCardTypes.add(TrainType.Box);
            mTrainCardTypes.add(TrainType.Passenger);
            mTrainCardTypes.add(TrainType.Locomotive);
            mTrainCardTypes.add(TrainType.Freight);
            mTrainCardTypes.add(TrainType.Reefer);
        }
        for (TrainType mTrainCardType : mTrainCardTypes)
        {
            mTrainCards.add(new ClientTrainCard(mTrainCardType));
        }


        //create cities - pre conditions dictace the name can't be null
        mCities = new ArrayList<>();
        mCityNames = new ArrayList<>();

        mCityNames.add("Provo");
        mCityNames.add("Salt Lake");
        mCityNames.add("Orem");
        mCityNames.add("Springville");
        for (String mCityName : mCityNames)
        {
            mCities.add(new City(mCityName));
        }
        //create routes
        mRoutes = new ArrayList<>();
        //set up first route for easy testing
        ICity[] cit = new ICity[]{mCities.get(0), mCities.get(1)};
        TrainType trainType = TrainType.Reefer;
        int num = 2;
        mRoutes.add(new Route(cit, trainType, num, null));

        //use random seed to pick numbers for cities etc
        Random rand = new Random(1);
        for (int i = 0; i < 5; i++)
        {
            //get two random cities
            int index1 = rand.nextInt(mCities.size());
            int index2 = rand.nextInt(mCities.size());
            cit = new ICity[]{mCities.get(index1), mCities.get(index2)};
            //the last 2 items are locomotive then none, we don't want locomotice, so drop off one number
            index1 = rand.nextInt(TrainType.values().length - 1);
            //if we get length-2 then add one
            if (index1 == TrainType.values().length - 2)
            {
                index1++;
            }
            trainType = TrainType.values()[index1];
            //each route has between 1 aand 6 trains;
            num = rand.nextInt(6) + 1;
            mRoutes.add(new Route(cit, trainType, num, null));
        }
        //create destination cards
        //reseed
        rand = new Random(1);

        mDestinationCards = new ArrayList<>();
        for (int i = 0; i < 10; i++)
        {
            int points = rand.nextInt(15) + 5;
            int index1 = rand.nextInt(mCities.size());
            int index2 = rand.nextInt(mCities.size());
            cit = new ICity[]{mCities.get(index1), mCities.get(index2)};
            mDestinationCards.add(new ClientDestinationCard("id",cit, points));
        }
        mClientGameModel = new ClientGameModel("Cassandra",mDestinationCards.size(), mTrainCards.size(),
		        40, mTrainCards.subList(0, 4), mPlayerNames, mLobbyModel.getRoomID(), mLobbyModel.getRoomName());
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void setGameMap() {

    }

    @Test
    void setPlayers() {

    }

    @Test
    void setBank() {

    }

    @Test
    void getGameMap() {

    }

    @Test
    void getPlayers() {

    }

    @Test
    void getBank() {

    }

    @Test
    void getClientPlayer() {

    }

    @Test
    void getPlayer() {

    }

    @Test
    void getClientPlayerTrainCards() {

    }

    @Test
    void getClientPlayerDestinationCards() {

    }

    @Test
    void getClientTrainCardsByID() {

    }

    @Test
    void getComplementOfTempDestCardsByID() {

    }

    @Test
    void getRoutes() {

    }

    @Test
    void getRoute() {

    }

    @Test
    void getRevealedCards() {

    }

    @Test
    void setRevealedCards() {

    }

    @Test
    void getTemporaryDestinationCards() {

    }

    @Test
    void addTrainCard() {

    }

    @Test
    void addTemporaryDestinationCards() {

    }

    @Test
    void addDestinationCards() {

    }

    @Test
    void getCurrentPlayer() {

    }

    @Test
    void addChatMessage() {

    }

    @Test
    void pullChatMessages() {

    }

    @Test
    void getLastChatIndex() {

    }

    @Test
    void getChatMessenger() {

    }

    @Test
    void getRoomID() {

    }

    @Test
    void populateCoordinates() {

    }

    @Test
    void claimRoute() {

    }

    @Test
    void updatePlayerScore() {

    }

    @Test
    void getLastCommand() {

    }

    @Test
    void addCommand() {

    }

    @Test
    void removeTrainCards() {

    }

    @Test
    void updateRemainingTrains() {

    }

    @Test
    void updateTrainDeckSize() {

    }

    @Test
    void updateTrainDiscardPileSize() {

    }

    @Test
    void updateDestCardDeckSize() {

    }

    @Test
    void updateCurrentPlayerTakingTurn() {

    }

    @Test
    void setIsLastRound() {

    }

    @Test
    void getIsLastRound() {

    }

    @Test
    void updateGameOverData() {

    }

}