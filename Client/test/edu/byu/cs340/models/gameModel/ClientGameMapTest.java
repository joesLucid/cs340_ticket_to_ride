package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tjacobhi on 08-Mar-17.
 *
 * This tests that the ClientGameMap behaves properly
 */
public class ClientGameMapTest
{
	private ClientGameMap testMap;

	public ClientGameMapTest()
	{
		testMap = new ClientGameMap();
	}

	@BeforeEach
	public void setUp()
	{
		testMap = new ClientGameMap();
	}

	@AfterEach
	public void tearDown()
	{
		testMap = null;
	}

	@Test
	public void getCities()
	{
		ClientCity testCity = new ClientCity("testCity");
		testCity.setXCoordinate(6);
		testCity.setYCoordinate(28);
		ClientCity grayCity = new ClientCity("grayCity");
		grayCity.setXCoordinate(7);
		grayCity.setYCoordinate(25);
		testMap.mCities.add(testCity);
		testMap.mCities.add(grayCity);

		List<ClientCity> cities = testMap.getCities();
		Assertions.assertTrue(cities.get(0).getName().equals("testCity"), "testCity Name");
		Assertions.assertTrue(cities.get(1).getName().equals("grayCity"), "grayCity Name");
		Assertions.assertTrue(cities.get(0).getXCoordinate() == 6 && cities.get(0).getYCoordinate() == 28, "testCity Coordinates");
		Assertions.assertTrue(cities.get(1).getXCoordinate() == 7 && cities.get(1).getYCoordinate() == 25, "grayCity Coordinates");
	}

	@Test
	public void setCities()
	{
		List<ClientCity> cities = new ArrayList<>();
		ClientCity testCity = new ClientCity("testCity");
		testCity.setXCoordinate(6);
		testCity.setYCoordinate(28);
		ClientCity grayCity = new ClientCity("grayCity");
		grayCity.setXCoordinate(7);
		grayCity.setYCoordinate(25);
		cities.add(testCity);
		cities.add(grayCity);
		testMap.setCities(cities);
		Assertions.assertTrue(testMap.mCities.size() == 2, "cities count");
	}

	@Test
	public void getRoutes()
	{
		List<ClientRoute> testRoutes = new ArrayList<>();
		ClientCity[] cities = {new ClientCity("Seattle"), new ClientCity("Denver"), new ClientCity("Tempe")};
		ClientRoute testRoute = new ClientRoute(cities, TrainType.Passenger, 5, null);
		testRoutes.add(testRoute);
		testMap.mRoutes = testRoutes;
		final List<ClientRoute> routes = testMap.getRoutes();
		Assertions.assertTrue(routes.size() == 1, "routes count");
	}

	@Test
	public void getRoute()
	{
		List<ClientRoute> testRoutes = new ArrayList<>();
		ClientCity[] cities = {new ClientCity("Seattle"), new ClientCity("Denver"), new ClientCity("Tempe")};
		ClientRoute testRoute = new ClientRoute(cities, TrainType.Passenger, 5, null);
		String Id = testRoute.getId();
		testRoutes.add(testRoute);
		testMap.mRoutes = testRoutes;
		Assertions.assertTrue(testMap.getRoute(Id).equals(testRoute), "route id");
	}

	@Test
	public void setRoutes()
	{
		List<ClientRoute> testRoutes = new ArrayList<>();
		ClientCity[] cities = {new ClientCity("Seattle"), new ClientCity("Denver"), new ClientCity("Tempe")};
		ClientRoute testRoute = new ClientRoute(cities, TrainType.Passenger, 5, null);
		testRoutes.add(testRoute);
		testMap.setRoutes(testRoutes);
		Assertions.assertTrue(testMap.mRoutes.size() == 1, "Route count after set");
	}

	@Test
	public void getLongestRoute()
	{

	}

	@Test
	public void setLongestRoute()
	{

	}

	@Test
	public void claimRoute()
	{

	}

}