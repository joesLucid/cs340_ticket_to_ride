package edu.byu.cs340.presenter;

/**
 * Created by tjacobhi on 02-Feb-17.
 *
 * The interface between view and models for joining a game
 */
public interface IJoinGamePresenter extends IMainPresenter
{
	/**
	 * Sends a message to join a game by passing the ID of the game
	 * @pre gameID should not be null
	 * @pre must be connected to server
	 * @post calls joinLobby on Server
	 * @param gameID the id of the game
	 */
	void joinGame(String gameID);

	/**
	 * Sends a message to join a game by passing the title of the desrired game
	 * @pre title should not be null
	 * @pre must be connected to server
	 * @post calls createLobby  with title
	 * @param title the title for the game to be created
	 */
	void createGame(String title);

	/**
	 * Refreshes the table
	 * @pre Must be logged into server
	 * @post Sends refresh message to client
	 */
	void refresh();
}
