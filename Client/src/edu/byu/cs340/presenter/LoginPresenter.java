package edu.byu.cs340.presenter;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.ILoginModel;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.view.ILoginView;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.LoginView;
import edu.byu.cs340.view.MainView;

import java.util.Observable;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This is the implementation of ILoginPresenter used by the client
 */
public class LoginPresenter extends MainPresenter implements ILoginPresenter
{
	private ILoginView mLoginView;

	public LoginPresenter()
	{
		mLoginView = new LoginView();

		try
		{
			mLoginView.initialize(this);
		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("MainView rejected presenter");
		}
	}

	@Override
	public void activate()
	{
		super.activate();
		mLoginView.unlockSignIn();
	}

	@Override
	public IView getView()
	{
		return mLoginView;
	}

	@Override
	public void signIn(String username, String password)
	{
		((LoginView) getView()).lockSignIn();
		ClientFacade.getInstance(ILoginModel.class).login(username, password);
	}

	@Override
	public void register(String username, String password, String confirmPassword)
	{
		((LoginView) getView()).lockSignIn();
		ClientFacade.getInstance(ILoginModel.class).register(username, password, confirmPassword);
	}

	@Override
	public void changeHost(String host)
	{
		ClientFacade.getInstance(ILoginModel.class).setHost(host);
	}

	@Override
	public void changePort(int port)
	{
		ClientFacade.getInstance(ILoginModel.class).setPort(Integer.toString(port));
	}

	@Override
	public String getHost()
	{
		return ClientFacade.getInstance(ILoginModel.class).getHost();
	}

	@Override
	public int getPort()
	{
		return Integer.parseInt(ClientFacade.getInstance(ILoginModel.class).getPort());
	}

	@Override
	public void update(Observable o, Object arg)
	{
		super.update(o, arg);
		if (arg instanceof ErrorObsMsg && isActive())
		{
			mLoginView.unlockSignIn();
			mLoginView.invalidCredentials(((ErrorObsMsg) arg).getMessage());
		}
//		else if (arg instanceof LoginObsMsg)
//		{
////			ClientFacade.getInstance(edu.byu.cs340.clientFacade.ILoginModel.class).changeState(ClientState
// .joinGame);
////			mLoginView.unlockSignIn();
////			System.out.println("We are now logged in!");
//
//		}
	}
}
