package edu.byu.cs340.presenter;

import edu.byu.cs340.view.IView;

import javax.swing.*;

/**
 * Created by joews on 3/1/2017.
 * Interface for non-popup presenters
 */
public interface IMainPresenter extends IPresenter
{
	/**
	 * gets the view for the presenter. Should be implemented by subclass.
	 * @return the view owned by the presenter
	 */
	IView getView();
	
	/**
	 * Signs the user out and returns to the login state.
	 */
	void signOut();

	/**
	 * Sets the menu bar to the specified menu bar.
	 * @param menuBar Menu bar to set.
	 */
	void setMenuBar(JMenuBar menuBar);


}
