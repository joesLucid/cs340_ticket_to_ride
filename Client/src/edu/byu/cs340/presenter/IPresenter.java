package edu.byu.cs340.presenter;

import java.util.Observer;

/**
 * Created by joews on 3/1/2017.
 *
 * This is the base interface all presenters inherit.
 */
public interface IPresenter extends Observer
{

	/**
	 * Activates the view
	 * @pre presenter must be created and view must be created
	 * @pre all other views must be deactivated before activating
	 * @post switches the associated view to the window
	 */
	void activate();

	/**
	 * Should be called if the view will not be displayed
	 * @pre view and presenter must be initialized
	 * @post cleans up view
	 */
	void deactivate();

	/**
	 * @return whether this presenter is active
	 */
	boolean isActive();

	/**
	 * Gets the username of the client.
	 * @return The username on the client.
	 */
	String getUsername();
}
