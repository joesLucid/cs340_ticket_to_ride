package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.IChatMessage;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.game.ChatCommandPopupView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rebekah on 2/24/2017.
 */
public class ChatCommandPopupPresenter extends PopupPresenter implements IChatCommandPopupPresenter
{

	ChatCommandPopupView mView;
	GamePresenterProxy mGamePresenterProxy;
	int lastChatIndex = -1;


	public ChatCommandPopupPresenter(GamePresenterProxy mainGamePresenter)
	{
		mGamePresenterProxy = mainGamePresenter;
		mView = new ChatCommandPopupView();
		try
		{
			mView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("Popup rejected view");
		}
	}


	@Override
	public void sendChatMessage(String message)
	{
		ClientFacade.getInstance(IGameModel.class).sendGameChatMessage(message);
	}

	@Override
	public ArrayList<IChatMessage> getCurrentChatHistory()
	{
		return null;
	}

	@Override
	public ArrayList<String> getCurrentCommandHistory()
	{
		return null;
	}

	@Override
	@Deprecated
	public void updateChatMessages()
	{
		List<ChatMessage> chatMessages = ClientFacade.getInstance(IGameModel.class).getChatMessages(lastChatIndex);
		for (ChatMessage message : chatMessages)
		{
			updateAddChatMessage(message);
		}
	}

	@Override
	public void updateAddChatMessage(ChatMessage message)
	{
		mView.pushChatMessage(message.getUserName(), message.getMessage().trim());
	}

	@Override
	public void updateAddCmd(String cmdToString)
	{
		mView.pushCmd(cmdToString);
	}

	@Override
	public IView getView()
	{
		return mView;
	}

	@Override
	public void activate()
	{
		super.activate();
		lastChatIndex = -1;
	}

	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	public void clearView()
	{
		mView.clearView();
	}

}
