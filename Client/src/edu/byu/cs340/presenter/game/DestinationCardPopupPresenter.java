package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.game.DestinationCardPopupView;

import java.util.List;

/**
 * Created by Rebekah on 2/24/2017.
 * this is the class that handles the view for the destination card popup
 */
public class DestinationCardPopupPresenter extends PopupPresenter implements IDestinationCardPopupPresenter
{

	private DestinationCardPopupView mView;
	private GamePresenterProxy mGamePresenterProxy;
	static private int DEFAULT = 10;
	int mMinToKeep = 2;
	//	private static boolean isFirstTrue = true;
	private List<ClientDestinationCard> mTemporaryCards = null;

	public DestinationCardPopupPresenter(GamePresenterProxy mainGamePresenter)
	{
		mGamePresenterProxy = mainGamePresenter;
		mView = new DestinationCardPopupView();
		try
		{
			mView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("Popup rejected view");
		}
	}


	@Override
	public IView getView()
	{
		return mView;
	}

	@Override
	public void setTemporaryCards(List<ClientDestinationCard> cards)
	{
		mTemporaryCards = cards;
		mView.updateNumberToKeep(mMinToKeep);
		mView.setCards(cards);
	}

	@Override
	public void setMinToKeep(int minToKeep)
	{
		mMinToKeep = minToKeep;
		mView.enableDestinationSelectionList(true);
		mView.enableTakeSelectedCards(true);
		//set maximum cars to return in the view

		//tell UI to enable selecting
	}

	@Override
	public String validateAndDrawCards(List<String> selectedCards)
	{
		ClientFacade.getInstance(IGameModel.class).chooseDestinationCards(selectedCards, mMinToKeep);
		mTemporaryCards = null;
		return "you drew a card(s)";
	}


	public String getUserName()
	{
		String name = null;
		if (IGameModel.class != null)
		{
			name = ClientFacade.getInstance(IGameModel.class).getUserName();

		}
		if (name == null)
		{
			name = "error loading name";
		}
		return name;
	}

	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	public void clearView()
	{
		mView.clearView();
	}

	//<editor-fold desc="Old methods">

	/**
	 * Get value of TemporaryCards
	 *
	 * @return Returns the value of TemporaryCards
	 */
	@Deprecated
	public List<ClientDestinationCard> getTemporaryCards()
	{
		return mTemporaryCards;
	}


	public int getMinCardsToKeep()
	{
		return mMinToKeep;
	}
	//</editor-fold>
}
