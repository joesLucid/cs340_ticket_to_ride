package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.game.TurnStartPopupView;

/**
 * Created by joews on 3/1/2017.
 * 
 */
public class TurnStartPopupPresenter extends PopupPresenter implements ITurnStartPopupPresenter
{
	TurnStartPopupView mView;
	GamePresenterProxy mGamePresenterProxy;

	public TurnStartPopupPresenter(GamePresenterProxy mainGamePresenter)
	{
		mGamePresenterProxy = mainGamePresenter;
		mView = new TurnStartPopupView();
		try
		{
			mView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("Popup rejected view");
		}
	}

	@Override
	public void activate()
	{
		super.activate();
		//test if we can draw train cards
		boolean canDrawTrainCards = ClientFacade.getInstance(IGameModel.class).canDrawTrainCards();
		boolean canDrawDestCards = ClientFacade.getInstance(IGameModel.class).canDrawDestCards();
		boolean canClaimRoutes = true;
		mView.setPossibleActions(canDrawDestCards, canDrawTrainCards, canClaimRoutes);
	}

	@Override
	public void optionSelected(int option)
	{
		mGamePresenterProxy.startTurnOptionSelected(option);
	}

	@Override
	public IView getView()
	{
		return mView;
	}

	@Override
	public void updateIsLastRound()
	{
		mView.updateIsLastRound();
	}

	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	public void clearView()
	{
		mView.clearView();
	}
}
