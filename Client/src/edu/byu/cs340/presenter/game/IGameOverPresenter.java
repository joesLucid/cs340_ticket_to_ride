package edu.byu.cs340.presenter.game;

import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;

/**
 * Created by joews on 3/24/2017.
 */
public interface IGameOverPresenter extends IPopupPresenter
{
	void setGameOverData(IGameOverData data);

	void sendLeaveGameSignal();

	void clearView();
}
