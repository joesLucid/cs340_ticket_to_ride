package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.models.ChatMessage;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * Client Implementation of the lobby models.
 */
public class ClientLobbyModel extends LobbyModel
{
	public ClientLobbyModel()
	{
		super();
	}

	public ClientLobbyModel(String roomName, String roomID, String user)
	{
		super(roomName, user);
		this.mRoomID = roomID;
	}

	@Override
	public List<ChatMessage> getChatMessages(long lastIndex)
	{
		return super.getChatMessages(lastIndex);
	}

	@Override
	public long getLastIndex()
	{
		return super.getLastIndex();
	}

	@Override
	public void addChatMessage(ChatMessage message)
	{
		super.addChatMessage(message);
	}

	public void addChatMessage(List<ChatMessage> messages)
	{
		super.addChatMessage(messages);
	}

}
