package edu.byu.cs340.models;

/**
 * Created by joews on 2/1/2017.
 *
 * The user on the client side. This stores the username, password and authentication token of the user.
 */
public class ClientUser extends BaseUser
{

	private String mPassword;
	private String mAuthenticationToken;

	public ClientUser(String userName, String password)
	{
		super(userName);
		this.mPassword = password;
	}

	/**
	 * Get value of AuthenticationToken
	 *
	 * @return Returns the value of AuthenticationToken
	 */
	public String getAuthenticationToken()
	{
		return mAuthenticationToken;
	}

	/**
	 * Set the value of authenticationToken variable
	 *
	 * @param authenticationToken The value to set authenticationToken member variable to
	 */
	public void setAuthenticationToken(String authenticationToken)
	{
		mAuthenticationToken = authenticationToken;
	}

	/**
	 * Get value of Password
	 *
	 * @return Returns the value of Password
	 */
	public String getPassword()
	{
		return mPassword;
	}

	/**
	 * Set the value of password variable
	 *
	 * @param password The value to set password member variable to
	 */
	public void setPassword(String password)
	{
		mPassword = password;
	}
}

