package edu.byu.cs340.models;

import java.util.Observable;

/**
 * Created by Rebekah on 3/15/2017.
 */
public class ModelObservable extends Observable {

    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg);
    }
}
