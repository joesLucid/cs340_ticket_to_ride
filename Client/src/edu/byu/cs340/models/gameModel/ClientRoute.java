package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.gameModel.map.Route;
import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by Sean on 2/25/2017.
 *
 * Client models representation of the routes on the board
 */
public class ClientRoute extends Route {
	
	public ClientRoute(ICity[] cities, TrainType trainType, int numTrains, String pairedRouteID)
	{
		super(cities, trainType, numTrains, pairedRouteID);
	}

	public ClientRoute(IRoute that, ICity[] cities)
	{
		super(that);
		this.mCities = cities;
	}
	
	public ClientRoute(IRoute that)
	{
		super(that);
		ICity[] newCities = new ICity[2];
		for (int i = 0; i < 2; i++)
		{
			newCities[i] = new ClientCity(that.getCities().get(i));
		}
		this.mCities = newCities;

	}
	
	/**
	 * Override equals to compare the cities by ID.
	 *
	 * @param obj Object to compare to.
	 * @return Returns true if the objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ClientRoute)
		{
			return ((ClientRoute) obj).getId().equals(this.getId());
		} else
		{
			return false;
		}
	}

	/**
	 * Overrides hashcode based on id
	 *
	 * @return mId.hashCode()
	 */
	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}
}
