package edu.byu.cs340.models.gameModel;

import java.util.Set;

/**
 * Created by Sean on 2/25/2017.
 *
 * Client representation of the longest route marker
 */
public class ClientLongestRoute {
	protected Set<IGamePlayer> mPlayer;
	protected int mLength;

	public ClientLongestRoute(Set<IGamePlayer> player, int length)
	{
		mPlayer = player;
		mLength = length;
	}

	public Set<IGamePlayer> getPlayer()
	{
		return mPlayer;
	}

	public void setPlayer(Set<IGamePlayer> player)
	{
		mPlayer = player;
	}
	
	public int getLength()
	{
		return mLength;
	}
	
	public void setLength(int length)
	{
		mLength = length;
	}
}
