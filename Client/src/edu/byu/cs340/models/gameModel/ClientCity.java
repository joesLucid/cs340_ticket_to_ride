package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.ICity;

import java.awt.geom.Point2D;

/**
 * Created by Sean on 2/25/2017.
 * <p>
 * Client representation of cities on the game board
 */
public class ClientCity extends City {
	protected int mXCoordinate;
	protected int mYCoordinate;
	
	public ClientCity(String name)
	{
		super(name);
	}
	
	public ClientCity(ICity that)
	{
		super(that);
	}
	
	public int getXCoordinate()
	{
		return mXCoordinate;
	}
	
	public void setXCoordinate(int XCoordinate)
	{
		mXCoordinate = XCoordinate;
	}
	
	public int getYCoordinate()
	{
		return mYCoordinate;
	}
	
	public void setYCoordinate(int YCoordinate)
	{
		mYCoordinate = YCoordinate;
	}
	
	public Point2D getPoint()
	{
		return new Point2D.Double(mXCoordinate, mYCoordinate);
	}
	
	/**
	 * Override equals to compare the cities by ID.
	 *
	 * @param obj Object to compare to.
	 * @return Returns true if the objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ClientCity)
		{
			return ((ClientCity) obj).getId().equals(this.getId());
		} else
		{
			return false;
		}
	}
	
	/**
	 * Overrides hashcode based on id
	 *
	 * @return mId.hashCode()
	 */
	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}
}