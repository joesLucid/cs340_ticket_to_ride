package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.roomModel.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 3/1/2017.
 *
 * Client models representation for players during a game
 */
public class LocalGamePlayer extends Player implements IGamePlayer {
	protected List<ClientDestinationCard> mDestinationCards = new ArrayList<>();
	protected List<ClientTrainCard> mTrainCards = new ArrayList<>();
	protected int mPrivateScore = 0;

	protected int mTrainsRemaining = 0;

	protected int mScore = 0;
	
	/**
	 * Constructor for player.
	 *
	 * @param username Username of player
	 * @pre username!=null && username.isEmpty()==false
	 * @post getUsername().equals(username)
	 */
	public LocalGamePlayer(String username)
	{
		super(username);
	}
	
	public List<ClientDestinationCard> getDestinationCards()
	{
		return mDestinationCards;
	}

	@Override
	public int getDestinationCardsNum() {
		return mDestinationCards.size();
	}

	public void setDestinationCards(List<ClientDestinationCard> destinationCards)
	{
		mDestinationCards = destinationCards;
	}

	@Override
	public int getTrainCardsNum() {
		return mTrainCards.size();
	}

	public void addTrainCard(ClientTrainCard trainCards)
	{
		if (!mTrainCards.contains(trainCards))
		{
			mTrainCards.add(trainCards);
		}
	}

	public void removeTrainCard(ClientTrainCard card)
	{
		if (mTrainCards.contains(card))
		{
			mTrainCards.remove(card);
		}
	}
	public void addDestinationCards(List<ClientDestinationCard> destinationCards)
	{
		for (ClientDestinationCard card : destinationCards)
		{
			if (!mDestinationCards.contains(card))
			{
				mDestinationCards.add(card);
			}
		}
	}

	public List<ClientTrainCard> getTrainCards()
	{
		return mTrainCards;
	}

	public void setTrainCards(List<ClientTrainCard> trainCards)
	{
		mTrainCards = trainCards;
	}
	
	public int getTrainsRemaining()
	{
		return mTrainsRemaining;
	}
	
	public void setTrainsRemaining(int trainsRemaining)
	{
		mTrainsRemaining = trainsRemaining;
	}
	
	public int getScore()
	{
		return mScore;
	}
	
	public void setScore(int score)
	{
		mScore = score;
	}

	public int getPrivateScore()
	{
		return mPrivateScore;
	}

	public void setPrivateScore(int privateScore)
	{
		mPrivateScore = privateScore;
	}
	/*public void updateScore(){
		
	}*/


}
