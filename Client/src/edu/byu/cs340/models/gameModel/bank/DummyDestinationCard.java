package edu.byu.cs340.models.gameModel.bank;

import java.util.UUID;

/**
 * Created by Sean on 3/1/2017.
 *
 */
public class DummyDestinationCard extends ClientDestinationCard
{
	public DummyDestinationCard()
	{
		super(UUID.randomUUID().toString(), null, 0);
	}
}
