package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.IGamePlayer;

/**
 * Created by Sean on 3/1/2017. the is the class for the games resourcs
 */
public class ClientGameBank
{
	private int mDestinationCardDeckSize;

	private int mTrainCardDeckSize;

	private int mDiscardedPileSize;

	private int mMinToKeep;

	private ClientTrainCard[] mRevealedCards;

	private ClientDestinationCard[] mTemporaryCards;

	private IGamePlayer mClientPlayer;


	public ClientDestinationCard[] getTemporaryCards()
	{
		return mTemporaryCards;
	}

	public void setTemporaryCards(ClientDestinationCard[] temporaryCards, int minToKeep)
	{
		mTemporaryCards = temporaryCards;
		mMinToKeep = minToKeep;
	}

	public int getMinToKeep()
	{
		return mMinToKeep;
	}

	public IGamePlayer getClientPlayer()
	{
		return mClientPlayer;
	}

	public void setClientPlayer(IGamePlayer clientPlayer)
	{
		mClientPlayer = clientPlayer;
	}

	public ClientGameBank()
	{
		mDestinationCardDeckSize = 0;
		mTrainCardDeckSize = 0;
		mRevealedCards = new ClientTrainCard[5];
		mDiscardedPileSize = 0;
		mTemporaryCards = new ClientDestinationCard[3];
	}

	public int getDestinationCardDeckSize()
	{
		return mDestinationCardDeckSize;
	}

	public void setDestinationCardDeckSize(int destinationCardDeckSize)
	{
		mDestinationCardDeckSize = destinationCardDeckSize;
	}

	public int getTrainCardDeckSize()
	{
		return mTrainCardDeckSize;
	}

	public void setTrainCardDeckSize(int trainCardDeckSize)
	{
		mTrainCardDeckSize = trainCardDeckSize;
	}


	public int getDiscardedPileSize()
	{
		return mDiscardedPileSize;
	}


	public void setDiscardedPileSize(int discardedPileSize)
	{
		mDiscardedPileSize = discardedPileSize;
	}

	public ClientTrainCard[] getRevealedCards()
	{
		return mRevealedCards;
	}

	public void setRevealedCards(ClientTrainCard[] revealedCards)
	{
		mRevealedCards = revealedCards;
	}

	public void removeRevealedCard(ClientTrainCard card)
	{
		if (card != null && mRevealedCards != null)
		{
			for (int i = 0; i < mRevealedCards.length; i++)
			{
				ClientTrainCard revealedCard = mRevealedCards[i];
				if (revealedCard != null && revealedCard.getId().equals(card.getId()))
				{
					mRevealedCards[i] = null;
				}
			}
		}
	}

	public void decrementTrainCardDeck(int decrement)
	{
		mTrainCardDeckSize -= decrement;
	}

	public void decrementDestinationCardDeck(int decrement)
	{
		mDestinationCardDeckSize -= decrement;
	}

	public void incrementTrainCardDeck(int increment)
	{
		mTrainCardDeckSize += increment;
	}

	public void incrementDestinationCardDeck(int increment)
	{
		mDestinationCardDeckSize += increment;
	}

}
