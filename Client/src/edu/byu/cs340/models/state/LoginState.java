package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class LoginState extends State
{
	public LoginState()
	{
		super();
	}

	//login
	//register
	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UILogin:
				onUILogin(event);
				break;
			case CMDLogin:
				onCMDLogin(event);
				break;
			case UIRegister:
				onUIRegister(event);
				break;
		}
	}

	private void onUIRegister(Event event)
	{
		String username = (String) event.get(EventKeys.USERNAME);
		String password = (String) event.get(EventKeys.PASSWORD);
		String confirm_password = (String) event.get(EventKeys.CONFIRM_PASSWORD);
		//send off command to server
		CommandManager.getInstance().register(username, password, confirm_password);
	}

	private void onCMDLogin(Event event)
	{
		//If we get here the state is updated
		ClientModelManager.getInstance().setState(new JoinGameState());
		CommandManager.getInstance().sendAmInGameRequest();
	}

	private void onUILogin(Event event)
	{
		String username = (String) event.get(EventKeys.USERNAME);
		String password = (String) event.get(EventKeys.PASSWORD);
		//send off command
		CommandManager.getInstance().login(username, password);
	}
}
