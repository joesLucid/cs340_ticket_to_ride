package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class

DrawSecondTrainCardState extends DrawTrainCardState
{
	public DrawSecondTrainCardState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
	}

	@Override
	protected void onCMDDrawTrainCard(Event event)
	{
		String playerName = (String) event.get(EventKeys.PLAYER_NAME);
		if (playerName.equals(ClientModelManager.getInstance().getUsername()))
		{
			ClientModelManager.getInstance().setState(new NotMyTurnState());
		}
	}


}
