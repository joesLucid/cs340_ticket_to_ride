package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class DrawDestinationCardState extends GameState
{
	public DrawDestinationCardState()
	{
		super();
	}

	@Override
	public void onActivate()
	{
		super.onActivate();
		CommandManager.getInstance().drawDestinationCards();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case CMDDrawDestinationCards:
				onCMDDrawDestinationCards(event);
				break;
		}
	}

	protected void onCMDDrawDestinationCards(Event event)
	{
		ClientModelManager.getInstance().setState(new ReturnDestinationCardState());
	}

	@Override
	protected boolean onError(String errorMessage, int errorCode)
	{
		boolean sendMsg = super.onError(errorMessage, errorCode);
		switch (errorCode)
		{
			case ErrorMessage.NO_CARDS_IN_DEST_DECK:
				onNoCardsInDestDeck();
				break;
		}
		return sendMsg;
	}

	private void onNoCardsInDestDeck()
	{
		ClientModelManager.getInstance().setState(new PickActionState());
	}


}
