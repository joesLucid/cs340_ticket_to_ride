package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.models.state.State;

/**
 * Created by joews on 3/15/2017.
 */
public class GameOverState extends State
{
	public GameOverState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
		}
	}

}
