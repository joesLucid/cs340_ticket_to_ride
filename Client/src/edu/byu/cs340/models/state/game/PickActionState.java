package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class PickActionState extends GameState
{


	public PickActionState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIClaimRoutePicked:
				//go to draw destination card
				ClientModelManager.getInstance().setState(new ClaimRouteState());
				break;
			case UIDrawDestinationCardsPicked:
				//go to draw destination card
				ClientModelManager.getInstance().setState(new DrawDestinationCardState());
				break;
			case UIDrawTrainCardsPicked:
				//go to draw destination card
				ClientModelManager.getInstance().setState(new DrawFirstTrainCardState());
				break;
			case CMDDrawTrainCard:
				onCMDDrawTrainCard(event);
				break;
			case CMDDrawDestinationCards:
				onCMDDrawDestinationCards(event);
				break;
		}
	}

	protected void onCMDDrawDestinationCards(Event event)
	{
		ClientModelManager.getInstance().setState(new ReturnDestinationCardState());
	}

	protected void onCMDDrawTrainCard(Event event)
	{
		DrawableTrainStateProcessor.onCMDDrawTrainCard(event);
	}
}
