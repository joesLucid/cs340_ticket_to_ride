package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;

import java.util.List;

/**
 * Created by joews on 3/15/2017.
 */
public class ClaimRouteState extends GameState
{
	public ClaimRouteState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UICancel:
				onUICancel(event);
				break;
			case UIClaimRoute:
				onUIClaimRoute(event);
				break;
			case CMDClaimRoute:
				onCMDClaimRoute(event);
				break;
		}
	}

	private void onCMDClaimRoute(Event event)
	{
		ClientModelManager.getInstance().setState(new NotMyTurnState());
	}
	
	private void onUICancel(Event event)
	{
		ClientModelManager.getInstance().setState(new PickActionState());
	}

	private void onUIClaimRoute(Event event)
	{
		String routeID = (String) event.get(EventKeys.ROUTE_ID);
		List<String> trainCardIDs = (List<String>) event.get(EventKeys.TRAIN_CARD_ID_LIST);
		
		IClientGameModel gameModel = ClientModelManager.getInstance().getGame();
		List<ClientTrainCard> cardsToSend = gameModel.getClientTrainCardsByID(trainCardIDs);

		CommandManager.getInstance().claimRoute(routeID, cardsToSend);
	}
}
