package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class JoinGameState extends State
{
	public JoinGameState()
	{
		super();
	}

	@Override
	public void onActivate()
	{
		ClientModelManager.getInstance().processStateEvent(new Event(EventType.UIPullGames));
		super.onActivate();
	}

	//return to login (handle by super)
	//join a game
	//refresh the join game list
	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIPullGames:
				onUIPollGames(event);
				break;
			case CMDGameListUpdated:
				onCMDGameListUpdated(event);
				break;
			case UIJoinGame:
				onUIJoinGame(event);
				break;
			case CMDJoinGame:
				onCMDJoinGame(event);
				break;
			case UICreateLobby:
				onUICreateLobby(event);
				break;
			case CMDCreateLobby:
				onCMDCreateLobby(event);
				break;
		}
	}

	private void onUICreateLobby(Event event)
	{
		String lobbyName = (String) event.get(EventKeys.LOBBY_NAME);
		String username = ClientModelManager.getInstance().getUsername();
		//send off command to server
		CommandManager.getInstance().createLobby(username, lobbyName);
	}

	private void onCMDCreateLobby(Event event)
	{
		//transition to the lobby state
		ClientModelManager.getInstance().setState(new LobbyState());
	}

	private void onCMDJoinGame(Event event)
	{
		//transition to the lobby state
		ClientModelManager.getInstance().setState(new LobbyState());
	}

	private void onUIJoinGame(Event event)
	{
		String lobbyID = (String) event.get(EventKeys.LOBBY_ID);
		String username = ClientModelManager.getInstance().getUsername();
		//send off command to server
		CommandManager.getInstance().joinLobby(username, lobbyID);
	}

	private void onCMDGameListUpdated(Event event)
	{
		//currently do nothing
	}

	private void onUIPollGames(Event event)
	{
		String usename = ClientModelManager.getInstance().getUsername();
		//send off command to server
		CommandManager.getInstance().pollGameList(usename);
	}
}
