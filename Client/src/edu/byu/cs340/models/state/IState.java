package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;

/**
 * Created by joews on 3/15/2017.
 */
public interface IState
{

	void processEvent(Event event);

	void onActivate();
}
