package edu.byu.cs340.models;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.ClientGameModel;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;
import edu.byu.cs340.models.roomModel.RoomsListInfo;
import edu.byu.cs340.models.state.IState;

import java.util.Collection;
import java.util.Observer;

/**
 * Created by joews on 2/1/2017.
 *
 * Manager that stores all of the client side mData models, and executes commands on said models.
 */
public class ClientModelManager  implements IClientModelManager {

	private ClientUser mClientUser;
	private ClientLobbyModel mClientLobbyModel;
	private IClientGameModel mClientGameModel;
	private IRoomsListInfo mGamesListInfo;
	private ModelObservable mModelObservable;
	private static ClientModelManager mSingleton;
	private IState mState;

	
	public ClientModelManager()
	{
		mClientUser = new ClientUser(null,null);
		mGamesListInfo = new RoomsListInfo();
		mClientLobbyModel = new ClientLobbyModel();
		mModelObservable = new ModelObservable();
	}

	public static ClientModelManager getInstance()
	{
		if (mSingleton == null)
		{
			mSingleton = new ClientModelManager();
		}
		return mSingleton;
	}

	public String getAuthenticationToken()
	{
		return mClientUser.getAuthenticationToken();
	}
	
	public String getUsername()
	{
		return mClientUser.getUserName();
	}

	public ClientUser getUser()
	{
		return mClientUser;
	}
	
	public void setUser(ClientUser user)
	{
		mClientUser = user;
	}

	public IClientGameModel getGame()
	{
		return mClientGameModel;
	}

	public void setGame(ClientGameModel game)
	{
		mClientGameModel = game;
	}

	public ClientLobbyModel getLobby()
	{
		return mClientLobbyModel;
	}

	public void setLobby(ClientLobbyModel lobby)
	{
		mClientLobbyModel = lobby;
	}

	/**
	 * Get value of GamesListInfo
	 *
	 * @return Returns the value of GamesListInfo
	 */
	public IRoomsListInfo getGamesListInfo()
	{
		return mGamesListInfo;
	}

	/**
	 * Set the value of gamesListInfo variable
	 *
	 * @param gamesListInfo The value to set gamesListInfo member variable to
	 */
	public void setGamesListInfo(IRoomsListInfo gamesListInfo)
	{
		mGamesListInfo = gamesListInfo;
	}

	/**
	 * clears the local lobby models.
	 */
	public void clearLobby()
	{
		mClientLobbyModel = new ClientLobbyModel();
	}

	/**
	 * Clears the local game models.
	 */
	public void clearGame()
	{
		mClientGameModel = null;
	}

	/**
	 * Clears the local game list.
	 */
	public void clearGameList()
	{
		mGamesListInfo = new RoomsListInfo();
	}

	public synchronized void addObserver(Observer o) {
		mModelObservable.addObserver(o);
	}

	public synchronized void deleteObserver(Observer o) {
		mModelObservable.deleteObserver(o);
	}

	public void notifyObservers(IObsMsg message)
	{
		mModelObservable.notifyObservers(message);
	}

	@Override
	public void notifyObservers(Collection<IObsMsg> observerMessages)
	{
		for (IObsMsg msg : observerMessages)
		{
			this.notifyObservers(msg);
		}
	}

	/**
	 * Get value of IState
	 *
	 * @return Returns the value of State
	 */
	public IState getState()
	{
		return mState;
	}

	/**
	 * Set the value of State variable
	 *
	 * @param IState The value to set IState member variable to
	 */
	public synchronized void setState(IState IState)
	{
		mState = IState;
		mState.onActivate();
	}

	@Override
	public synchronized void processStateEvent(Event event)
	{
		mState.processEvent(event);
	}
}
