package edu.byu.cs340.models;


import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.ClientGameModel;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;
import edu.byu.cs340.models.state.IState;

import java.util.Collection;
import java.util.Observer;

/**
 * Created by Rebekah on 3/15/2017.
 */
public interface IClientModelManager {

    String getAuthenticationToken();

     String getUsername();

    ClientUser getUser();

    void setUser(ClientUser user);

    IClientGameModel getGame();

    void setGame(ClientGameModel game);

    ClientLobbyModel getLobby();

    void setLobby(ClientLobbyModel lobby);

    /**
     * Get value of GamesListInfo
     *
     * @return Returns the value of GamesListInfo
     */
    IRoomsListInfo getGamesListInfo();

    /**
     * Set the value of gamesListInfo variable
     *
     * @param gamesListInfo The value to set gamesListInfo member variable to
     */
    void setGamesListInfo(IRoomsListInfo gamesListInfo);

    /**
     * clears the local lobby model.
     */
    void clearLobby();

    /**
     * Clears the local game model.
     */
    void clearGame();

    /**
     * Clears the local game list.
     */
    void clearGameList();

	void addObserver(Observer o);

	void deleteObserver(Observer o);

	void notifyObservers(IObsMsg message);

	void notifyObservers(Collection<IObsMsg> observerMessages);

	void processStateEvent(Event event);

	IState getState();

	void setState(IState IState);
}
