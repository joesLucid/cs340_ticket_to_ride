package edu.byu.cs340.view;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.ILobbyModel;
import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;
import edu.byu.cs340.presenter.ILobbyPresenter;
import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.view.game_lobby.PlayerListTableModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * Created by joews on 2/8/2017.
 */
public class LobbyView extends MainView implements ILobbyView
{
	private JPanel mMainPanel;
	private JButton mSendButton;
	private JTextArea mChatInput;
	private JTextArea mChatHistoryArea;
	private JButton mSIGNOUTButton;
	private JTable mPlayersTable;
	private JButton mActionButton;
	private JButton mKickPlayerButton;
	private JLabel mPlayersLabel;
	private JScrollPane mChatHistoryScroll;
	private JScrollPane mChatInputScroll;
	private JPanel mChatInputPanel;
	private JPanel mChatPanel;
	private JPanel mLeftPanel;
	private JPanel mRightPanel;
	private JPanel mButtonPanel;
	private JPanel mTablePanel;
	private JLabel mUserName;
	private JLabel mMessage;
	private JButton mLeaveGameButton;
	private JPanel mRootPanel;
	private JLabel mTitle;
	private JScrollPane mPlayerScroll;
	private JPanel mTopPanel;
	private JPanel mTopRightPanel;
	private PlayerListTableModel mTableModel;

	private final static String READY_TEXT = "Ready";
	private final static String UNREADY_TEXT = "Un-Ready";
	private final static String START_TEXT = "Start";

	private final int ICON_COLUMN_WIDTH = 30;
	private final int ROW_HEIGHT = 30;

	private boolean isReady = false;
	private boolean isHost = false;
	private static final String TEXT_SUBMIT = "text-submit";
	private static final String INSERT_BREAK = "insert-break";


	private BufferedImage mBackgroundImage = null;
	private String mBackgroundPath = "/ticketToRideBox.jpg";

	//<editor-fold desc="GUI Builder functionality">
	/*
	 * The following methods are used by the GUI builder
	 */


	/**
	 * Required function called when using GUI Builder approach if you specify an object has a "Custom Create". Not
	 * required otherwise. When this is used IntelliJ will create everything it can and then any object in the form
	 * marked "Custom Create" it will not create. You will have to create that object here.
	 */
	private void createUIComponents()
	{
		mRootPanel = new JPanel(new BorderLayout())
		{
			@Override
			public void paintComponent(Graphics g)
			{
				Graphics2D g2d = (Graphics2D) g;
				super.paintComponent(g);
				g.drawImage(mBackgroundImage, 0, 0, getWidth(), getHeight(), this);
				g.setColor(ViewUtils.SEMI_TRANSPARENT);
			}
		};
		mTopPanel = new JPanel(new BorderLayout())
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				g.setColor(ViewUtils.SEMI_TRANSPARENT);
				ViewUtils.drawRoundedRectBehindTextComponent((Graphics2D) g, mTitle, 4, 5);
			}
		};
		mTopRightPanel = new JPanel(new BorderLayout())
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				g.setColor(ViewUtils.SEMI_TRANSPARENT);
				ViewUtils.drawRoundedRectBehindTextComponent((Graphics2D) g, mUserName, 4, 5);
			}
		};
		//create the table
		setupTable();

	}

	/**
	 * Helper function to create the table, set it to a PlayerListTableModel and set properties for it.
	 */
	private void setupTable()
	{
		mTableModel = new PlayerListTableModel();
		mPlayersTable = new JTable(mTableModel);

		mPlayersTable.getColumnModel().getColumn(0).setMaxWidth(ICON_COLUMN_WIDTH);
		mPlayersTable.getColumnModel().getColumn(2).setMaxWidth(ICON_COLUMN_WIDTH * 3);


		mPlayersTable.setRowHeight(ROW_HEIGHT);
		mPlayersTable.setShowVerticalLines(false);

	}
	//</editor-fold>


	//<editor-fold desc="IView Methods">
	/*
	 * The following group of comments are required by the IView interface
	 */

	/**
	 * Initialize the view by giving it the presenter associated with it
	 *
	 * @param presenter The associated presenter to the view
	 * @throws InvalidPresenterException Throws exception if the provided presenter is the wrong type.
	 */
	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
		if (!(getConnectedPresenter() instanceof ILobbyPresenter))
		{
			throw new InvalidPresenterException();
		}
	}

	/**
	 * Called when the view is made active by the window manager. This is called after the view is created, but
	 * before it is displayed. This initializes texts fields and other fields.
	 */
	@Override
	public void onActivate()
	{
		super.onActivate();
		mMessage.setMaximumSize(new Dimension(300, 50));
		mMessage.setText("");
		mUserName.setText(getConnectedPresenter().getUsername());
		//TODO: Should not be skipping the presenter
		isReady = ClientFacade.getInstance(ILobbyModel.class).getAmIReady();
		isHost = ClientFacade.getInstance(ILobbyModel.class).getAmIHost();
		updateActionButtonText();
		mTitle.setText(((ILobbyPresenter) getConnectedPresenter()).getTitle());
		mChatHistoryArea.setText("");
	}

	/**
	 * Returns the root (outermost) JPanel. This is important for when working with the GUI Builder. the window
	 * manager "thinks" that the view itself is the LobbyView. Any hand coded classes can simply have the MainView
	 * class
	 * itself be the root, but because we used the GUI builder we have to tell the WindowManager what the actual root
	 * view is, in this case the mRootPanel.
	 *
	 * @return
	 */
	@Override
	public JPanel getJPanel()
	{
		return mRootPanel;
	}

	//</editor-fold>


	//<editor-fold desc="ILobbyView methods">

	/**
	 * Adds or Updates the player in te table based on user name. If they are already in it, it will simply update the
	 * fields.
	 *
	 * @param player the player who has joined
	 */
	@Override
	public void addUpdatePlayer(ILobbyPlayer player)
	{
		mTableModel.updateAddPlayer(player);

	}

	/**
	 * This will be used to show the user that a player left
	 *
	 * @param player the player who left
	 */
	@Override
	public void removePlayer(ILobbyPlayer player, ILobbyPlayer newHost)
	{

		mTableModel.removePlayer(player);
		mTableModel.updateAddPlayer(newHost);
	}

	/**
	 * Sets the players in the lobby to the list players.
	 *
	 * @param players Players to set in table in lobby for view.
	 */
	@Override
	public void setLobbyPlayers(List<ILobbyPlayer> players)
	{
		mTableModel.setData(players);
	}

	/**
	 * Stores whether the client's player is ready or is the host. Updates GUI based on these values.
	 *
	 * @param isReady Whether the client's player is ready.
	 * @param isHost  Whether the client's player is the host.
	 */
	@Override
	public void setClientPlayerStatus(boolean isReady, boolean isHost)
	{
		this.isReady = isReady;
		this.isHost = isHost;
		updateActionButtonText();
	}

	/**
	 * Pushes chat message to MChatHistoryArea text box
	 *
	 * @param username  the user who sent the message
	 * @param message the message
	 */
	@Override
	public void pushChatMessage(String username, String message)
	{
		mChatHistoryArea.append(username + ": " + message + "\n");
		JScrollBar sb = mChatHistoryScroll.getVerticalScrollBar();
		sb.setValue(sb.getMaximum());
	}

	/**
	 * Displays error message
	 *
	 * @param message Message to display
	 */
	@Override
	public void invalidAction(String message)
	{
		if (message != null)
		{
			if (message.length() > 80)
			{
				message = message.substring(0, 76) + "...";
			}
			mMessage.setForeground(Color.RED);
			mMessage.setText(message);
		}

	}

	@Override
	public String getViewName()
	{
		return "Lobby";
	}
	//</editor-fold>

	/**
	 * This constructor is called when creating the LobbyView object. This is used primarily for assigning listners.
	 *
	 * NOTE: If using the GUI builder method, all of the objects will be created first. The only exception is if you
	 * specify "Custom Create" and fail to actually initialize a field in the "createUIComponents" method.
	 */
	public LobbyView()
	{
		this.mSIGNOUTButton.addActionListener((listener) -> getConnectedPresenter().signOut());
		mActionButton.addActionListener((listener) -> onActionButton());
		mKickPlayerButton.addActionListener((listener) -> onKickPlayer());
		mSendButton.addActionListener((listener) -> onSendMessage());
		mLeaveGameButton.addActionListener((listener) -> onLeave());
		mSendButton.addActionListener((listener) -> onSendMessage());


		InputMap input = mChatInput.getInputMap();
		KeyStroke enter = KeyStroke.getKeyStroke("ENTER");
		KeyStroke shiftEnter = KeyStroke.getKeyStroke("shift ENTER");
		input.put(shiftEnter, INSERT_BREAK);  // input.get(enter)) = "insert-break"
		input.put(enter, TEXT_SUBMIT);

		ActionMap actions = mChatInput.getActionMap();
		actions.put(TEXT_SUBMIT, new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onSendMessage();
			}
		});
		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(mBackgroundPath));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		mChatHistoryScroll.getViewport().setBackground(new Color(240, 240, 240, 240));
		mChatInputScroll.getViewport().setBackground(new Color(240, 240, 240, 250));
		mPlayerScroll.getViewport().setBackground(new Color(240, 240, 240, 220));
		mButtonPanel.setBackground(new Color(220, 220, 220, 250));
	}

	//<editor-fold desc="Private Members">

	/**
	 * Sends message to presenter to kick selected player.
	 */
	private void onKickPlayer()
	{
		String userToKick = null;
		int rowSelected = mPlayersTable.getSelectedRow();
		if (rowSelected >= 0)
		{
			userToKick = mTableModel.getUserName(rowSelected);
		}
		if (userToKick != null)
		{
			((ILobbyPresenter) getConnectedPresenter()).sendRemovePlayerSignal(userToKick);
		}
	}


	/**
	 * Sends the message in the mChatInput to the presenter.
	 */
	private void onSendMessage()
	{
		String message = mChatInput.getText();
		if (message != null && !message.isEmpty())
		{
			((ILobbyPresenter) getConnectedPresenter()).sendChatMessage(message);
		}
		mChatInput.setText("");
	}

	/**
	 * Sends signal to presenter depending on status.
	 * If client's player is host it will try to start the game
	 * else if the client's player is ready it will unready the player
	 * else if will ready the player.
	 */
	private void onActionButton()
	{
		if (isHost)
		{
			((ILobbyPresenter) getConnectedPresenter()).sendStartSignal();
		} else if (isReady)
		{
			((ILobbyPresenter) getConnectedPresenter()).sendUnreadySignal();
		} else
		{
			((ILobbyPresenter) getConnectedPresenter()).sendReadySignal();
		}
	}

	/**
	 * Sends signal to presenter to leave lobby.
	 */
	private void onLeave()
	{
		((ILobbyPresenter) getConnectedPresenter()).sendLeaveSignal();
	}


	/**
	 * Updates the Ready/Unready/Start button's text based on if the player is host or based on ready status.
	 * Also calls to toggle the visibility of the kick player button.
	 */
	private void updateActionButtonText()
	{
		if (mActionButton != null)
		{
			if (isHost)
			{
				mActionButton.setText(START_TEXT);
				toggleKickPlayerButton(true);
			} else if (isReady)
			{
				mActionButton.setText(UNREADY_TEXT);
				toggleKickPlayerButton(false);
			} else
			{
				mActionButton.setText(READY_TEXT);
				toggleKickPlayerButton(false);
			}
		}
	}

	/**
	 * Toggles visibility of kick player button.
	 *
	 * @param toggle True will show, false will hide the kick player button.
	 */
	private void toggleKickPlayerButton(boolean toggle)
	{
		mKickPlayerButton.setVisible(toggle);
	}

	//</editor-fold>


}
