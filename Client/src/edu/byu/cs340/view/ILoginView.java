package edu.byu.cs340.view;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This is the interface for the view responsible for logging in.
 */
public interface ILoginView extends IView
{
	/**
	 * This is used to stop further incoming requests to sign in while the client is communicating to the server
	 */
	void lockSignIn();
	
	/**
	 * This is used to allow further incoming requests to sign in, usually after a failed sign in.
	 */
	void unlockSignIn();
	
	/**
	 * This is called when a log in failed due to invalid credentials
	 * @param message Explains why the user couldn't log in.
	 */
	void invalidCredentials(String message);
}