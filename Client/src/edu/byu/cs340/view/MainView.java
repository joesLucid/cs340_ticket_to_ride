package edu.byu.cs340.view;

import edu.byu.cs340.presenter.IMainPresenter;
import edu.byu.cs340.presenter.IPresenter;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This is a base class for all views in the main window. This extends the swing UI component JPanel in order to be
 * drawn to the JFrame
 */
public abstract class MainView extends View implements IView
{
	private IMainPresenter mConnectedPresenter;

	/**
	 * This should be called to associate the presenter with the view. It will only associate the presenter the first
	 * time this is called.
	 *
	 * @param presenter The associated presenter to the view
	 * @throws InvalidPresenterException when the presenter is not the presenter that matches the view
	 */


	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		if (presenter instanceof IMainPresenter)
		{
			if (mConnectedPresenter == null)
				mConnectedPresenter = (IMainPresenter) presenter;
		} else
		{
			throw new InvalidPresenterException();
		}
	}

	/**
	 * Sub-classes can call this to get the connected presenter
	 *
	 * @return the currently connected presenter.
	 */
	protected IMainPresenter getConnectedPresenter()
	{
		return mConnectedPresenter;
	}

	@Override
	public void onActivate()
	{
		// Default implementation is to do nothing
	}


	public void storeSavedSizeLocal()
	{
		mSavedSize = WindowManager.getInstance().getMainWindowSize();
		mSavedLocal = WindowManager.getInstance().getMainWindowLocal();
		mHasSavedSize = true;
	}

}
