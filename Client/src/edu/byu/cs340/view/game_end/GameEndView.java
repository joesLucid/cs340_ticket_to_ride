package edu.byu.cs340.view.game_end;

import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.presenter.game.GameOverPresentesr;
import edu.byu.cs340.presenter.game.IGameOverPresenter;
import edu.byu.cs340.view.PopupView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sean on 3/24/2017.
 *
 * View for showing scores at the end of the game
 */
public class GameEndView extends PopupView implements IGameEndView
{
	private JPanel mRootPanel;
	private JButton mReturnButton;
	private JPanel mScorePanel;
	private JScrollPane mScrollPane;
	private JLabel mWinner;
	private JPanel mPlayerListBox;
	
	/**
	 * Creates a new GameEndView with no panels
	 * @post sets listener on leave game button
	 */
	public GameEndView()
	{
		mPlayerListBox = new JPanel();
		mPlayerListBox.setLayout(new BoxLayout(mPlayerListBox, BoxLayout.X_AXIS));
		mScrollPane.setViewportView(mPlayerListBox);
		mReturnButton.addActionListener((listener) -> onLeaveGame());
	}
	
	/**
	 * Activates the view the same as a PopupView
	 * @post all panels from previous activations removed
	 */
	@Override
	public void onActivate()
	{
		super.onActivate();
		mPlayerListBox.removeAll();
	}
	
	/**
	 * Populates the view with the given data
	 * @param data the GameOverData object containing the players in the game and their scores.
	 * @pre data has at least one player
	 * @post view contains panels for each player
	 * @post view announces the name of the winning player
	 */
	@Override
	public void setGameOverData(IGameOverData data)
	{
		clearView();
		int bestScoreIndex = 0;
		int bestScore = data.getData().get(0).mTotalPoints;

		for (int i = 0; i < data.getData().size(); i++)
		{
			if (data.getData().get(i).mTotalPoints > bestScore)
			{
				bestScoreIndex = i;
				bestScore = data.getData().get(i).mTotalPoints;
			} else if (data.getData().get(i).mTotalPoints == bestScore)
			{
				//if tie then see if the player has more dest cards completed
				if (data.getData().get(i).mDestCompleted > data.getData().get(bestScoreIndex).mDestCompleted)
				{
					bestScoreIndex = i;
				}//else if equal test longest route
				else if (data.getData().get(i).mDestCompleted > data.getData().get(bestScoreIndex).mDestCompleted &&
						data.getData().get(i).mLongestPath > data.getData().get(i).mLongestPath)
				{
					bestScoreIndex = i;
				}//otherwise just stick with the current best
			}
		}
		// change mWinner
		mWinner.setText("Winner: " + data.getData().get(bestScoreIndex).mUsername);

		// create PlayerScorePanels
		for (int i = 0; i < data.getData().size(); i++)
		{
			PlayerScorePanel playerInfo = new PlayerScorePanel(data.getData().get(i));
			mPlayerListBox.add(playerInfo);
			mPlayerListBox.repaint();
			mPlayerListBox.revalidate();
		}
	}
	
	/**
	 * Clears the view of all text and panels
	 */
	@Override
	public void clearView()
	{
		mWinner.setText("");
		mPlayerListBox.removeAll();
	}
	
	/**
	 * Sends a leave game signal to the client
	 */
	private void onLeaveGame()
	{
		((IGameOverPresenter) getConnectedPresenter()).sendLeaveGameSignal();
	}
	
	/**
	 * initializes the presenter associated with the view
	 * @param presenter The associated presenter to the view
	 * @throws InvalidPresenterException if the given presenter is not a GameOverPresenter
	 * @post given presenter is attached to this view
	 */
	@Override
	synchronized public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
		if (!(getConnectedPresenter() instanceof GameOverPresentesr))
		{
			throw new InvalidPresenterException();
		}
	}
	
	/**
	 * @return the root panel of the view
	 */
	@Override
	public JPanel getJPanel()
	{
		return mRootPanel;
	}
	
	/**
	 * @return the minimum dimension of the window
	 */
	@Override
	public Dimension getMinSize()
	{
		return new Dimension(800, 300);
	}
}
