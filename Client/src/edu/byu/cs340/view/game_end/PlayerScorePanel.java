package edu.byu.cs340.view.game_end;

import edu.byu.cs340.models.gameModel.gameOver.PlayerScoreStruct;

import javax.swing.*;

/**
 * Created by Sean on 3/24/2017.
 *
 */
public class PlayerScorePanel extends Box{
	
	private JPanel mRootPanel;
	private JLabel mPlayerName;
	private JLabel mRouteScore;
	private JLabel mLongestScore;
	private JLabel mDestinationScore;
	private JLabel mFailedScore;
	private JLabel mTotalScore;
	private JLabel mCompletedDestinations;
	private JLabel mLongestRoute;

	public PlayerScorePanel(PlayerScoreStruct data)
	{
		super(BoxLayout.Y_AXIS);

		this.add(mRootPanel);
		mPlayerName.setText(data.mUsername);
		mRouteScore.setText(Integer.toString(data.mPointsFromClaimedRoutes));
		mLongestScore.setText(Integer.toString(data.mPointsFromLongestRoute));
		mDestinationScore.setText(Integer.toString(data.mPointsFromReachedDestinations));
		mFailedScore.setText(Integer.toString(data.mPointsLostFromUnreachedDestinations));
		mTotalScore.setText(Integer.toString(data.mTotalPoints));
		mLongestRoute.setText(Integer.toString(data.mLongestPath));
		mCompletedDestinations.setText(Integer.toString(data.mDestCompleted));
	}
}
