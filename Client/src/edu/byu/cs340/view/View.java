package edu.byu.cs340.view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by joews on 3/1/2017.
 *
 * This is the view that all views inherit from. This extends the swing UI component JPanel in order to be drawn to
 * the JFrame
 */
public abstract class View extends JPanel implements IView
{
	protected Dimension mSavedSize;
	protected Point mSavedLocal;
	protected boolean mHasSavedSize = false;
	@Override
	public String getViewName()
	{
		return this.getClass().getSimpleName();
	}

	public boolean hasSavedLocalSize()
	{
		return mHasSavedSize;
	}

	/**
	 * Sub classes should make a check to see if the connectedPresenter is the right type of presenter. If it is not,
	 * it should throw this exception.
	 */
	public static class InvalidPresenterException extends Exception {}

	@Override
	public JPanel getJPanel()
	{
		return this;
	}


	public Dimension getSavedSize()
	{
		return mSavedSize;
	}

	public Point getSavedLocal()
	{
		return mSavedLocal;
	}


}
