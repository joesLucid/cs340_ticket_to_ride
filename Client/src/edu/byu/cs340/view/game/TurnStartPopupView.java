package edu.byu.cs340.view.game;

import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.presenter.game.GamePresenterProxy;
import edu.byu.cs340.presenter.game.ITurnStartPopupPresenter;
import edu.byu.cs340.presenter.game.TurnStartPopupPresenter;
import edu.byu.cs340.view.PopupView;
import edu.byu.cs340.view.ViewUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by crown on 3/1/2017.
 * Represents the window that gives a user options of what to do in their turn.
 */
public class TurnStartPopupView extends PopupView implements ITurnStartPopupView
{
	private JPanel mRootPanel;
	private JButton mDrawDestinationCardsButton;
	private JButton mDrawTrainCardsButton;
	private JButton mClaimRouteButton;
	private JPanel mBoxPanel;
	private JPanel mPromptPanel;
	private JPanel mButtonPanelParent;
	private JPanel mButtonPanel;
	private JLabel mTitleLabel;
	private JLabel mLastRoundLabel;
	private BufferedImage mBackgroundImage;
	private static final String mBackgroundImagePath = "/game/turnStartBackground.jpg";
	private static final int TITLE_GUTTER = 4;
	private static final int ROUNDS = 3;
	private boolean mIsLastRound = false;

	public TurnStartPopupView()
	{
		super();
		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(mBackgroundImagePath));
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		mRootPanel.add(mBoxPanel, BorderLayout.CENTER);
		mButtonPanel.setBackground(ViewUtils.SEMI_TRANSPARENT);
		mButtonPanel.setOpaque(true);
		mDrawDestinationCardsButton.addActionListener((listener) -> onPress(GamePresenterProxy
				.DRAW_DESTINATION_CARD_OPTION));
		mDrawTrainCardsButton.addActionListener((listener) -> onPress(GamePresenterProxy.DRAW_TRAIN_CARD_OPTION));
		mClaimRouteButton.addActionListener((listener) -> onPress(GamePresenterProxy.CLAIM_ROUTE_OPTION));
	}

	private void createUIComponents()
	{
		mRootPanel = new JPanel()
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;

				if (mBackgroundImage != null)
				{
					g2d.drawImage(mBackgroundImage, 0, 0, mRootPanel.getWidth(), mRootPanel.getHeight(), null);
				}
				g2d.setColor(ViewUtils.SEMI_TRANSPARENT);
				int titleX = mTitleLabel.getX() - TITLE_GUTTER;
				int titleY = mTitleLabel.getY() - TITLE_GUTTER;
				int titleWidth = mTitleLabel.getWidth() + 2 * TITLE_GUTTER;
				int titleHeight = mTitleLabel.getHeight() + 2 * TITLE_GUTTER;
				if (mIsLastRound)
				{
					titleHeight += mLastRoundLabel.getHeight();
				}
				g2d.fillRoundRect(titleX, titleY, titleWidth, titleHeight, ROUNDS, ROUNDS);

			}
		};
		mRootPanel.setLayout(new BorderLayout());

	}


	private void onPress(int i)
	{
		((ITurnStartPopupPresenter) getConnectedPresenter()).optionSelected(i);
	}


	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
		if (!(getConnectedPresenter() instanceof TurnStartPopupPresenter))
		{
			throw new InvalidPresenterException();
		}
	}

	@Override
	public void onActivate()
	{
		super.onActivate();
	}


	@Override
	public JPanel getJPanel()
	{
		return mRootPanel;
	}

	@Override
	public Dimension getMinSize()
	{
		return new Dimension(375, 212);
	}

	@Override
	public void setPossibleActions(boolean canDrawDestCards, boolean canDrawTrainCards, boolean canClaimRoutes)
	{
		mClaimRouteButton.setEnabled(canClaimRoutes);
		mDrawDestinationCardsButton.setEnabled(canDrawDestCards);
		mDrawTrainCardsButton.setEnabled(canDrawTrainCards);
	}

	@Override
	public void updateIsLastRound()
	{
		mLastRoundLabel.setVisible(true);
		mIsLastRound = true;
	}

	@Override
	public void clearView()
	{
		mIsLastRound = false;
		setPossibleActions(true, true, true);
	}
}

