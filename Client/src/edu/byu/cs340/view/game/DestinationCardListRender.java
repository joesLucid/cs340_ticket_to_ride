//package edu.byu.cs340.view.game;
//
//import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
//
//import javax.swing.*;
//import java.awt.*;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by joews on 3/7/2017.
// */
//public class DestinationCardListRender extends DefaultListCellRenderer
//{
//	private final ImageIcon mBaseImage;
//	private Map<String, JPanel> JPanelMap;
//	int xScale;
//	int yScale;
//
//	public DestinationCardListRender(int xScale, int yScale)
//	{
//		this.xScale = xScale;
//		this.yScale = yScale;
//		mBaseImage = new ImageIcon(getClass().getResource("/game/mapUSsmall.jpg"));
//	}
//
//	@Override
//	public Component getListCellRendererComponent(
//			JList<? extends JPanel> list, Object value, int index,
//			boolean isSelected, boolean cellHasFocus)
//	{
////			JPanel card = new JPanel();
////			BorderLayout layout = new BorderLayout();
////			card.setLayout(layout);
////			layout.addLayoutComponent(new JLabel());
//
//		JPanel panel = (JPanel) super.getListCellRendererComponent(
//				list, value, index, isSelected, cellHasFocus);
//		JPanel panelToAdd = JPanelMap.get(value); // transform it
//		panel.add(panelToAdd);
//		return panel;
//	}
//
//	public void createImageMap(ClientDestinationCard[] list)
//	{
//		JPanelMap=new HashMap<>();
//		for (ClientDestinationCard s : list)
//		{
//			JPanelMap.put(s.getId(), createPanel(s));
//		}
//	}
//
//	private JPanel createPanel(ClientDestinationCard card)
//	{
//		JPanel res = new JPanel();
//		BoxLayout layout = new BoxLayout(res, BoxLayout.Y_AXIS);
//		res.setLayout(layout);
//		res.add(new JLabel(card.sendGetCities().get(0).getName() + " to " + card.sendGetCities().get(1).getName()));
//		res.add(new JLabel(mBaseImage));
//		res.add(new JLabel("Points: " + card.getPoints()));
//		res.setBorder(BorderFactory.createLineBorder(Color.black));
//		return res;
//	}
//
//}
