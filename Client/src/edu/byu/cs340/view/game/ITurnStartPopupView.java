package edu.byu.cs340.view.game;

/**
 * Created by crown on 2/24/2017.
 * This is the interface for a class that implements displaying the popup to start a player's turn
 */
public interface ITurnStartPopupView {
	void setPossibleActions(boolean canDrawDestCards, boolean canDrawTrainCards, boolean canClaimRoutes);

	void updateIsLastRound();

	void clearView();
}
