package edu.byu.cs340.view.game;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.presenter.game.DestinationCardPopupPresenter;
import edu.byu.cs340.view.PopupView;
import edu.byu.cs340.view.ViewUtils;
import edu.byu.cs340.view.game.destinationCardPanel.DestinationCardList;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rebekah on 2/24/2017.
 */
public class DestinationCardPopupView extends PopupView implements IDestinationCardPopupView
{
	public static final String CHOOSE_S_DESTINATION_CARDS_BELOW = "Select %s+ Destination Cards";
	//	private boolean hasDrawnCards;
	private JPanel mTopRootPanel;
	private JPanel mMainPanel;
	private JButton mTakeSelectedCardsButton;
	private JLabel mCardsLabel;
	private JLabel mUsername;
	//	private JButton mCancelButton;
	private JLabel mTitle;
	private JLabel mMessage;
	private JScrollPane mCardScroll;
	private JPanel mCardList;

	private static final int WIDTH = 500;
	private static final int HEIGHT = 425;
	private BufferedImage mBackgroundImage;
	private static final String mBackgroundImagePath = "/game/ticketToRideGoldenTicket.png";
	private static final int TITLE_GUTTER = 4;
	private static final int ROUNDS = 3;
//	private ClientDestinationCard[] currentCards;

	public DestinationCardPopupView()
	{
		super();
		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(mBackgroundImagePath));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		mMessage.setText("");
		mMessage.setOpaque(false);
		
		mTakeSelectedCardsButton.addActionListener((listener) -> onTakeSelectedCardsPress());
		mTakeSelectedCardsButton.setEnabled(false);

		mCardsLabel.setForeground(Color.WHITE);
		mCardsLabel.setOpaque(true);
		mCardsLabel.setBackground(new Color(0, 0, 0, 255));

		mCardScroll.setViewportView(mCardList);
		mCardScroll.getViewport().setOpaque(false);
	}

	synchronized private void onTakeSelectedCardsPress()
	{
		//draw cards and validate
		ClientDestinationCard[] selected = ((DestinationCardList) mCardList).getSelected();
		int minCards = ((DestinationCardPopupPresenter) getConnectedPresenter()).getMinCardsToKeep();
		List<String> cardsToKeep = new ArrayList<>();
		//validate number of cards selected
		if (selected.length < minCards)
		{
			mCardsLabel.setForeground(Color.RED);
			/*mMessage.setEnabled(true);
			mMessage.setText("Must select at least " + minCards);
			mMessage.setOpaque(true);
			mMessage.setBackground(new Color(0, 0, 0, 150));
			*/
		} else
		{
			for (int i = 0; i < selected.length; i++)
			{
				cardsToKeep.add(selected[i].getId());
			}
			((DestinationCardPopupPresenter) getConnectedPresenter()).validateAndDrawCards(cardsToKeep);

		}
	}

	@Override
	synchronized public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
		if (!(getConnectedPresenter() instanceof DestinationCardPopupPresenter))
		{
			throw new InvalidPresenterException();
		}
	}

	@Override
	public void onActivate()
	{
		super.onActivate();
		mUsername.setText(getConnectedPresenter().getUsername());
		mCardsLabel.setForeground(Color.WHITE);
		mMessage.setText("");
	}

	@Override
	public JPanel getJPanel()
	{

		return mTopRootPanel;
	}


	@Override
	synchronized public void setCards(List<ClientDestinationCard> givenCards)
	{
		((DestinationCardList) mCardList).setClientDestinationCards(givenCards);
		mTakeSelectedCardsButton.setEnabled(true);
	}

	@Override
	public void updateNumberToKeep(int minToKeep)
	{
		mCardsLabel.setText(String.format(CHOOSE_S_DESTINATION_CARDS_BELOW, minToKeep));
	}

	@Override
	public Dimension getMinSize()
	{
		return new Dimension(WIDTH, HEIGHT);
	}


	public void enableTakeSelectedCards(boolean enable)
	{
		mTakeSelectedCardsButton.setEnabled(enable);
	}

	public void enableDestinationSelectionList(boolean enable)
	{
		((DestinationCardList) mCardList).setLock(enable);
	}

	@Override
	public void clearView()
	{
		mMessage.setText("");
		mMessage.setOpaque(false);
		mCardsLabel.setForeground(Color.WHITE);
		mCardsLabel.setOpaque(true);
		mCardsLabel.setBackground(new Color(0, 0, 0, 255));
		((DestinationCardList) mCardList).clearView();
		mTakeSelectedCardsButton.setEnabled(false);
	}

	private void createUIComponents()
	{
		mTopRootPanel = new JPanel(new BorderLayout())
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;

				if (mBackgroundImage != null)
				{
					g2d.drawImage(mBackgroundImage, 0, 0, mTopRootPanel.getWidth(), mTopRootPanel.getHeight(), null);
				}

			}
		};
		mTopRootPanel.setOpaque(false);

		mMainPanel = new JPanel(new BorderLayout())
		{
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;
				g2d.setColor(ViewUtils.SEMI_TRANSPARENT);
				int titleX = 0;
				int titleY = 0;
				int titleWidth = this.getWidth() + 2 * TITLE_GUTTER;
				int titleHeight = this.getHeight() + 2 * TITLE_GUTTER;
				g2d.fillRoundRect(titleX, titleY, titleWidth, titleHeight, ROUNDS, ROUNDS);
			}
		};
		mMainPanel.setBorder(BorderFactory.createEmptyBorder(TITLE_GUTTER, TITLE_GUTTER, TITLE_GUTTER, TITLE_GUTTER));
		mMainPanel.setOpaque(false);

		mCardList = new DestinationCardList(BoxLayout.Y_AXIS, 0);// TODO: place custom component creation code here
	}
}
