package edu.byu.cs340.view.game;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rebekah on 3/1/2017.
 */
public class DestinationPopupCardListRenderer extends DefaultListCellRenderer
{

    private Font font = new Font("helvetica", Font.BOLD, 10);
    private Map<String, ImageIcon> imageMap;

    @Override
    public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {

        JLabel label = (JLabel) super.getListCellRendererComponent(
                list, value, index, isSelected, cellHasFocus);
        Image image = imageMap.get(value).getImage(); // transform it
        Image newimg = image.getScaledInstance(115, 179,  java.awt.Image.SCALE_SMOOTH); //todo: scale it to better size
        Icon imageIcon = new ImageIcon(newimg);  // transform it back
        label.setIcon(imageIcon);
        label.setHorizontalTextPosition(JLabel.CENTER);//you can change this for different displays
        label.setFont(font);
        return label;
    }


    public void createImageMap(String[] list) {
        imageMap = new HashMap<>();
        for (String s : list) {
            imageMap.put(s, new ImageIcon(
                    getClass().getResource(s)));
        }
    }

   /* private Image getScaledImage(Image srcImg, int w, int h){
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();

        return resizedImg;
    }*/
}
