package edu.byu.cs340.view.game;

import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.LocalGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.game.gameBoardView.GameBoardView;

import java.util.List;
import java.util.Set;

/**
 * Created by crown on 2/24/2017.
 * This is the interface for a class that implements displaying the game board and the rest of the main window
 */
public interface IGameBoardView extends IView
{


	/**
	 * Adds the player info cards to the side bar on the main window
	 *
	 * @param players the list of players in this game
	 */
	void addPlayers(List<LocalGamePlayer> players);

	void setGameBoardAction(GameBoardView.GAME_BOARD_ACTION action);

	/**
	 * Adds one player to the game
	 *
	 * @param player the player to add
	 */
	void addPlayer(IGamePlayer player);

	void updatePlayerDestCardCount(String player, int count);

	void updatePlayerTrainCardCount(String player, int count);

	void updatePlayerTrainCount(String player, int count);

	void updatePlayerScore(String player, int score);

	void updatePlayerPrivateScore(String usuername, int privateScore);
	/**
	 * Displays the given mRoute claimed by the given player
	 *
	 * @param routeID  the ID of the mRoute to claim
	 * @param username the username of the user to claim it
	 * @pre routeID != null
	 * @pre userID != null
	 */
	void setRouteAsClaimed(String routeID, String username);

	void playerLeft(String username);

	void updateTrainCardsInDeck(int deckSize);

	void updateDestCardsInDeck(int deckSize);

	void updateTrainCardsInDiscard(int deckSize);

	void updateRevealedTrainCards(List<ClientTrainCard> revealedTrainCards);

	void updateClientPlayerDestinationCards(List<ClientDestinationCard> handDestinationCards);

	void updateClientPlayerTrainCards(List<ClientTrainCard> handTrainCards);

	List<ClientTrainCard> getPlayerTrainCardsDisplayed();

	void updateLongestRoute(Set<String> usernames, int length);

	void setPlayerTurn(String username);

	void updateIsLastRound();

	void clearView();


	enum GAME_BOARD_ACTION
	{
		DRAW_FIRST_TRAIN_CARD,
		DRAW_SECOND_TRAIN_CARD,
		CLAIM_ROUTE,
		NOT_MY_TURN,
		SELECT_ACTION
	}
}
