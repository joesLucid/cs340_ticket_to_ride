package edu.byu.cs340.view.game;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.view.IView;

import java.util.List;

/**
 * Created by Rebekah on 2/24/2017.
 */
public interface IDestinationCardPopupView extends IView {
    /**
     * the function takes in a list of cards that it will display to the user and returns the drawn card(s)
     * @param givenCards the cards that will be selectable to the player
     * @return the card(s) that the player selected.
     */
    void setCards(List<ClientDestinationCard> givenCards);

	void updateNumberToKeep(int minToKeep);


	void enableTakeSelectedCards(boolean enable);

	void enableDestinationSelectionList(boolean enable);

	void clearView();
}
