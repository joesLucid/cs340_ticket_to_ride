package edu.byu.cs340.view.game.destinationCardPanel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 3/7/2017.
 */
public class DestinationCardList extends JPanel
{
	public static final String BACKGROUND_IMAGE_PATH = "/game/goldenTicket.png";
	private BoxLayout mBoxLayout;
	private GridLayout mGridLayout;
	private Map<String, Integer> mCardIDtoIndex;
	private List<ClientDestinationCard> mClientDestinationCardData;
	private List<DestinationCardPanel> mClientDestinationCardPanels;
	private List<Boolean> mIsPanelSelected;
	private Image mBaseImage;
	private boolean mAllowSelection;
	private static final int HEIGHT = 80;
	private static final int WIDTH = 150;
	private int boxAxis;

	public DestinationCardList(int boxLayout, int rightGutter)
	{
		super();
		this.boxAxis = boxLayout;
		mBoxLayout = new BoxLayout(this, boxLayout);
		this.setLayout(mBoxLayout);
		this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, rightGutter));
		this.setOpaque(false);
		mAllowSelection = true;
		try
		{
			mBaseImage = ImageIO.read(getClass().getResourceAsStream(BACKGROUND_IMAGE_PATH));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}


	public void setClientDestinationCards(List<ClientDestinationCard> cardsToAdd)
	{
		this.removeAll();
		mClientDestinationCardData = new ArrayList<>();
		mClientDestinationCardPanels = new ArrayList<>();
		mIsPanelSelected = new ArrayList<>();
		mCardIDtoIndex = new HashMap<>();
		if (cardsToAdd != null)
		{
			for (ClientDestinationCard card : cardsToAdd)
			{
				if (card != null)
				{
					Integer index = mCardIDtoIndex.get(card.getId());
					if (index != null)
					{
						//add code to update card.... which shouldn't do anything in this case....
					} else
					{
						index = mClientDestinationCardPanels.size();
						mCardIDtoIndex.put(card.getId(), index);
						mClientDestinationCardData.add(card);
						mIsPanelSelected.add(false);
						DestinationCardPanel panel = new DestinationCardPanel(card, mBaseImage, this, boxAxis);
						this.add(panel);
						mClientDestinationCardPanels.add(panel);
					}
				}
			}
		}
	}


	public void onClickCallBack(String cardID)
	{
		//do something
		System.out.println("USER CLICKED: " + cardID);
		Integer index = mCardIDtoIndex.get(cardID);
		if (index != null && mAllowSelection)
		{
			DestinationCardPanel selected = mClientDestinationCardPanels.get(index);
			selected.toggleSelection();
			selected.repaint();
		}
	}

	public ClientDestinationCard[] getSelected()
	{
		ArrayList<ClientDestinationCard> selected = new ArrayList<>();
		for (int i = 0; i < mIsPanelSelected.size(); i++)
		{
			if (mClientDestinationCardPanels.get(i).isSelected())
			{
				selected.add(mClientDestinationCardData.get(i));
			}
		}
		ClientDestinationCard[] result = new ClientDestinationCard[selected.size()];
		result = selected.toArray(result);
		return result;
	}

	public void setLock(boolean enable)
	{
		mAllowSelection = enable;
	}

	public void clearView()
	{
		mCardIDtoIndex = new HashMap<>();
		mClientDestinationCardData = new ArrayList<>();
		mClientDestinationCardPanels = new ArrayList<>();
		mIsPanelSelected = new ArrayList<>();
		this.removeAll();
	}
}

