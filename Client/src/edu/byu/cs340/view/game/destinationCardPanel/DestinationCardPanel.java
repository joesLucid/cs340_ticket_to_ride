package edu.byu.cs340.view.game.destinationCardPanel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by joews on 3/10/2017.
 */
class DestinationCardPanel extends Box
{
	private int mPoints;
	private String mTitle;
	private JTextArea mTitleArea;
	private JPanel mTitleHolder;
	private String mCardID;
	private DestinationCardList mParent;
	private static final int BORDER_THICKNESS = 5;
	private static final int HEIGHT = 60;
	private static final int WIDTH = 150;
	private static final int DIA = 25;
	private Image mBaseImage;
	private boolean mIsSelected = false;

	public DestinationCardPanel(ClientDestinationCard card, Image baseImage, DestinationCardList parent, int axis)
	{
		super(axis);
		this.mPoints = card.getPoints();
		this.mTitle = card.getCities().get(0).getName() + " to " + card.getCities().get(1).getName();
		this.mCardID = card.getId();
		this.mParent = parent;
		this.mBaseImage = baseImage;
		this.setOpaque(false);
		createLayout();
		createComponents();
		playerComponents();
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.drawImage(mBaseImage, 0, 0, WIDTH, HEIGHT, null);


		int x = WIDTH / 2 - DIA / 2;
		int y = (int) (HEIGHT * .7 - DIA / 2);
		g2d.setStroke(new BasicStroke(5));
		g2d.setColor(new Color(207, 181, 59));
		g2d.drawOval(x, y, DIA, DIA);
		g2d.setStroke(new BasicStroke(3));
		g2d.setColor(new Color(212, 175, 55));
		g2d.drawOval(x, y, DIA, DIA);


		String msg = String.valueOf(mPoints);
		g2d.setColor(Color.BLACK);
		Font font = new Font(Font.SANS_SERIF, Font.BOLD, 14);
		g2d.setFont(font);
		FontMetrics metrics = g2d.getFontMetrics(font);
		x = WIDTH / 2 - metrics.stringWidth(msg) / 2;
		y = (int) (HEIGHT * .7) - metrics.getHeight() / 2 + metrics.getAscent();
		g2d.drawString(msg, x, y);

		if (mIsSelected)
		{
			g2d.setColor(new Color(230, 230, 230, 150));
			g2d.fillRoundRect(0, 0, WIDTH, HEIGHT, 10, 10);
		}
	}


	private void playerComponents()
	{

		this.add(mTitleHolder, BorderLayout.NORTH);
	}


	private void createLayout()
	{
		this.setBorder(BorderFactory.createEmptyBorder(BORDER_THICKNESS, BORDER_THICKNESS * 3, BORDER_THICKNESS,
				BORDER_THICKNESS * 3));
		this.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setOpaque(false);
	}

	private void createComponents()
	{
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 11);

		//create the title
		mTitleHolder = new JPanel(new BorderLayout());
		mTitleArea = new JTextArea();
		mTitleHolder.add(mTitleArea, BorderLayout.CENTER);
		mTitleHolder.setOpaque(false);
		mTitleArea.setLineWrap(true);
		mTitleArea.setOpaque(false);
		mTitleArea.setEditable(false);
		mTitleArea.setDragEnabled(false);
		mTitleArea.setWrapStyleWord(true);
		mTitleArea.setRequestFocusEnabled(false);
		mTitleArea.setFocusable(false);
		mTitleArea.setFont(font);
		mTitleArea.setText(mTitle);
		mTitleArea.setAlignmentX(Component.CENTER_ALIGNMENT);
		mTitleArea.setAlignmentY(Component.CENTER_ALIGNMENT);


		//add moust adapter
		MouseAdapter adapter = new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				super.mouseClicked(e);
				mParent.onClickCallBack(mCardID);
			}
		};
		this.addMouseListener(adapter);
		mTitleArea.addMouseListener(adapter);
		mTitleHolder.addMouseListener(adapter);
	}

	/**
	 * Get value of Selected
	 *
	 * @return Returns the value of Selected
	 */
	public boolean isSelected()
	{
		return mIsSelected;
	}

	/**
	 * Set the value of selected variable
	 *
	 * @param selected The value to set selected member variable to
	 */
	public void setSelected(boolean selected)
	{
		mIsSelected = selected;
	}

	public void toggleSelection()
	{
		mIsSelected = !mIsSelected;
	}
}