package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by crown on 3/4/2017.
 * Custom panel for drawing the map on. Stores its own copy of the cities, routes, and location of the background.
 */
public class MapPanel extends JPanel
{
	//a win animation
	private static final String mAnimatedImagePath ="/animation/Fireworks-Animated-Gif-Transparent-3.gif";

	private static final Color BORDER_COLOR = new Color(240, 200, 120);
	private static int PANEL_WIDTH = 750;
	private static int PANEL_HEIGHT = 650;
	private static int IMAGE_WIDTH = 750;
	private static int IMAGE_HEIGHT = 600;
	private static int IMAGE_X = 0;
	private static int IMAGE_Y = 0;
	private static final double ORIGINAL_IMAGE_WIDTH = 1150;
	private static final double ORIGINAL_IMAGE_HEIGHT = 726;

	private static final int CITY_DIAMETER = 10;
	private static final int CITY_BORDER = 2;
	private static final int FONT_SIZE = 12;
	private static final float ROUTE_WIDTH = 5;
	private static final float ROUTE_OUTLINE = ROUTE_WIDTH + 3;
	private static final float CLAIMED_ROUTE_WIDTH = ROUTE_WIDTH + 4;
	private static final float BACKGROUN_LINE_BUFFER = 4;
	private static final int DOUBLE_OFFSET = 6;
	private static final float MITER_LIMIT = 2;
	private static final float dashSolid = 2;
	private static final float dashEmpty = 1;
	private static final float dashMagnitude = (float) Math.sqrt(Math.pow(dashSolid, 2) + Math.pow(dashEmpty, 2));
	private static final float[] DASH = {0, (dashEmpty / (2 * dashMagnitude)), (dashSolid / dashMagnitude), (dashEmpty
			/ (2 * dashMagnitude))};
	private static final float DASH_PHASE = 0;
	private static final int CAP_ENDS = BasicStroke.CAP_BUTT;

	private static final Color DEFAULT_CLAIMED_BACKGROUND = new Color(50, 50, 50, 200);
	private static final Color DEFAULT_HIGHLIGHTED_BACKGROUND = new Color(240, 240, 240, 230);

	//colors of routes in order, indexed by type of train (as in TrainType), with cyan as a placeholder for locomotive
	private static final Color[] routeColors = {
			new Color(214, 179, 209),
			new Color(233, 237, 240),
			new Color(0, 114, 184),
			new Color(246, 212, 89),
			new Color(181, 113, 40),
			new Color(49, 39, 30),
			new Color(185, 42, 0),
			new Color(161, 196, 52),
			Color.cyan,
			Color.LIGHT_GRAY
	};
	private static final Color[] borderRouteColors = {
			new Color(184, 149, 179),
			new Color(203, 207, 210),
			new Color(0, 84, 154),
			new Color(216, 182, 59),
			new Color(151, 83, 10),
			new Color(19, 9, 0),
			new Color(155, 12, 0),
			new Color(131, 166, 22),
			Color.BLUE,
			Color.GRAY
	};

	//Maps to cities by their IDs.
	private Map<String, ClientCity> mCities;
	//Map from a route ID to a DrawableRoute, which contains a ClientRoute
	private Map<String, DrawableRoute> mRoutes;
	//map of highlighted routes
	private DrawableRoute mSelectedRoute;
	//Path to the background image resource
	private BufferedImage mBackgroundImage;
	private AffineTransform mScale;
	private RouteSelectedListener mOnSelectRoute;
	private IsRouteClaimableListener mIsRouteClaimableListener;
	private boolean mIsSelectable = false;


	public MapPanel(RouteSelectedListener onSelectRoute, IsRouteClaimableListener isRouteClaimableListener)
	{
		this.mOnSelectRoute = onSelectRoute;
		this.mIsRouteClaimableListener = isRouteClaimableListener;
		setBorder(BorderFactory.createEmptyBorder());
		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				super.mouseClicked(e);
				onClick(e);
			}
		});
	}

	public void clearView()
	{
		//Maps to cities by their IDs.
		mCities = new HashMap<>();
		//Map from a route ID to a DrawableRoute, which contains a ClientRoute
		mRoutes = new HashMap<>();
		//map of highlighted routes
		mSelectedRoute = null;
		//Path to the background image resource
		mIsSelectable = false;
	}

	public interface RouteSelectedListener
	{
		void onSelection(ClientRoute route);
	}

	public interface IsRouteClaimableListener
	{
		boolean isClaimable(String routeID);
	}

	public void setBackgroundImagePath(String backgroundImagePath)
	{
		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(backgroundImagePath));
		} catch (IOException e)
		{
			//e.printStackTrace();
		}
		mScale = new AffineTransform(PANEL_WIDTH / ORIGINAL_IMAGE_WIDTH, 0, 0, PANEL_HEIGHT / ORIGINAL_IMAGE_HEIGHT,
				0, 0);
	}

	public void setCities(Map<String, ClientCity> cities)
	{
		this.mCities = cities;
	}

	public void setRoutes(Map<String, ClientRoute> routes)
	{
		mRoutes = new HashMap<>();
		for (String id : routes.keySet())
		{
			ClientRoute route = routes.get(id);
			int offset = 0;
			if (routes.get(id).getPairedRouteID() != null)
			{
				if (mRoutes.containsKey(route.getPairedRouteID()))
				{
					offset = DOUBLE_OFFSET;
				} else
				{
					offset = -DOUBLE_OFFSET;
				}
			}
			DrawableRoute drawableRoute = new DrawableRoute(routes.get(id), offset);
			mRoutes.put(drawableRoute.getID(), drawableRoute);
		}
	}

	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(PANEL_WIDTH, PANEL_HEIGHT);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		//update width and height
		PANEL_WIDTH = this.getSize().width;
		PANEL_HEIGHT = this.getSize().height;
		double scalex = PANEL_WIDTH / ORIGINAL_IMAGE_WIDTH;
		double scaley = PANEL_HEIGHT / ORIGINAL_IMAGE_HEIGHT;
		double scale = Math.min(scalex, scaley);
		IMAGE_WIDTH = (int) (scale * ORIGINAL_IMAGE_WIDTH);
		IMAGE_HEIGHT = (int) (scale * ORIGINAL_IMAGE_HEIGHT);

		IMAGE_X = (PANEL_WIDTH - IMAGE_WIDTH) / 2;
		IMAGE_Y = (PANEL_HEIGHT - IMAGE_HEIGHT) / 2;

		mScale = new AffineTransform(scale, 0, 0,
				scale, 0, 0);

		if (mBackgroundImage != null)
		{
			g2d.drawImage(mBackgroundImage, IMAGE_X, IMAGE_Y, IMAGE_WIDTH, IMAGE_HEIGHT, null);
		}

		g2d.setStroke(new BasicStroke(ROUTE_WIDTH));
		g2d.setFont(new Font(Font.SANS_SERIF, Font.BOLD, FONT_SIZE));
		for (String id : mRoutes.keySet())
		{
			mRoutes.get(id).draw(g2d);
		}

		g2d.setColor(Color.black);
		g2d.setFont(new Font(Font.SANS_SERIF, Font.BOLD, FONT_SIZE));
		for (String id : mCities.keySet())
		{
			ClientCity city = mCities.get(id);

			g2d.setColor(Color.WHITE);
			int cityBorder = CITY_DIAMETER + CITY_BORDER;
			int relX = IMAGE_X + (int) (city.getXCoordinate() * mScale.getScaleX() - cityBorder / 2.0);
			int relY = IMAGE_Y + (int) (city.getYCoordinate() * mScale.getScaleY() - cityBorder / 2.0);
			g2d.fillOval(relX, relY, cityBorder, cityBorder);

			g2d.setColor(Color.black);
			relX = IMAGE_X + (int) (city.getXCoordinate() * mScale.getScaleX() - CITY_DIAMETER / 2.0);
			relY = IMAGE_Y + (int) (city.getYCoordinate() * mScale.getScaleY() - CITY_DIAMETER / 2.0);
			g2d.fillOval(relX, relY, CITY_DIAMETER, CITY_DIAMETER);

			String cityName = city.getName();
			FontMetrics fm = g2d.getFontMetrics();
			Rectangle2D rect = fm.getStringBounds(cityName, g2d);
			g2d.setColor(new Color(255, 255, 255, 175));
			g2d.fillRect(relX,
					relY - fm.getAscent(),
					(int) rect.getWidth(),
					(int) rect.getHeight());
			g2d.setColor(Color.black);
			g2d.drawString(cityName, relX, relY);
		}

	}

	public void setClaimRoute(String routeID, Color playerColor)
	{
		mRoutes.get(routeID).setClaimed(playerColor);
	}


	public void select(String routeID)
	{
		if (mRoutes.containsKey(routeID))
		{
			deselect();
			mSelectedRoute = mRoutes.get(routeID);
			mSelectedRoute.setHighlighted(true);
			repaint();
		}
	}


	public String getSelectedRouteID()
	{
		return mSelectedRoute.getID();
	}


	public void deselect()
	{
		if (mSelectedRoute != null)
		{
			mSelectedRoute.setHighlighted(false);
			repaint();
		}
		mSelectedRoute = null;
	}


	//<editor-fold desc="Route Selection Code">


	public void setSelectable(boolean selectable)
	{
		mIsSelectable = selectable;
		deselect();
	}

	private void onClick(MouseEvent e)
	{
		int clickX = e.getX();
		int clickY = e.getY();

		DrawableRoute route = getClosestRoute(clickX, clickY);
		if (route == null || route.isClaimed())
		{
			return;
		} else if (!testIfClaimable(route))
		{
			return;
		} else if (route.isHighlighted())
		{
			deselect();
			if (mOnSelectRoute != null)
			{
				mOnSelectRoute.onSelection(null);
			}
		} else if (mIsSelectable)
		{
			select(route.getID());
			if (mOnSelectRoute != null)
			{
				mOnSelectRoute.onSelection(route.getClientRoute());
			}
		}
		repaint();
	}

	private boolean testIfClaimable(DrawableRoute route)
	{
		return mIsRouteClaimableListener.isClaimable(route.getID());
	}


	private DrawableRoute getClosestRoute(float x, float y)
	{
		DrawableRoute closestRoute = null;
		for (DrawableRoute route : mRoutes.values())
		{
			closestRoute = getBetterRouteInPair(x, y, closestRoute, route);
		}
		return closestRoute;
	}

	private DrawableRoute getBetterRouteInPair(float x, float y, DrawableRoute r1, DrawableRoute r2)
	{
		if (r1 == null)
		{
			return r2;
		} else if (r2 == null)
		{
			return r1;
		} else
		{
			//for r1, see if hte point lies in between the lines perpendicular to the line at each end of the route

			double dist1 = calcDistanceFromRoute(x, y, r1);
			double dist2 = calcDistanceFromRoute(x, y, r2);
			if (Math.abs(dist1) < Math.abs(dist2))
			{
				return r1;
			} else if (Math.abs(dist2) < Math.abs(dist1))
			{
				return r2;
			} else
			{
				dist1 = calcDistanceFromRoute(x, y, r1);
				dist2 = calcDistanceFromRoute(x, y, r2);
				//if the lines share the same points
				if ((r1.x1 == r2.x1 &&
						r1.x2 == r2.x2 &&
						r1.y1 == r2.y1 &&
						r1.y2 == r2.y2) ||
						(r1.x1 == r2.x2 &&
								r1.x2 == r2.x1 &&
								r1.y1 == r2.y2 &&
								r1.y2 == r2.y1))
				{
					if (Math.signum(dist1) == Math.signum(r1.mOffset))
					{
						return r1;
					} else
					{
						return r2;
					}
				}//otherwise just return r1
				else
				{
					return r1;
				}
			}
		}
	}

	private double calcDistanceFromRoute(double x, double y, DrawableRoute r)
	{
		//test if the point lies in the middle of the route
		double A = (r.y2 - r.y1);
		double B = (r.x1 - r.x2);
		double C = r.x2 * r.y1 - r.y2 * r.x1;
		double m = -A / B;
		double b = -C / B;

		//get the perpendicular line;
		double m_perp = -1 / m;
		double b_perp = y - m_perp * x;
		double A_perp = 1;
		double B_perp = -1 / m_perp;
		double C_perp = b_perp / m_perp;

		//check if the sign from both end points to this new line are different.
		//distance from end 1 of the route
		double dist1_perp = calcDistancePointToLine(A_perp, B_perp, C_perp, r.x1, r.y1);
		//distance from end 2 of the route
		double dist2_perp = calcDistancePointToLine(A_perp, B_perp, C_perp, r.x2, r.y2);
		if (dist1_perp * dist2_perp < 0)
		{
			//different signs means the point is straddled by the perpendiculars so return the perpendicular distance
			return calcDistancePointToLine(A, B, C, x, y);
		} else
		{
			double dist1 = Math.sqrt(Math.pow(x - r.x1, 2) + Math.pow(y - r.y1, 2));
			double dist2 = Math.sqrt(Math.pow(x - r.x2, 2) + Math.pow(y - r.y2, 2));
			if (dist1 <= dist2)
			{
				float sign = 1;
				if (x < r.x1 || (r.x1 == x && y < r.y1))
				{
					sign = -1;
				}
				return dist1 * sign;
			} else
			{
				float sign = 1;
				if (x < r.x1 || (r.x1 == x && y < r.y1))
				{
					sign = -1;
				}
				return dist2 * sign;
			}
		}
	}

	private double calcDistancePointToLine(double A, double B, double C, double x, double y)
	{
		double numerator = A * x + B * y + C;
		double denominator = Math.sqrt(Math.pow(A, 2) + Math.pow(B, 2));
		return numerator / denominator;
	}
	//</editor-fold>


	private class DrawableRoute
	{
		private ClientRoute mRoute;
		private int baseX1;
		private int baseX2;
		private int baseY1;
		private int baseY2;
		private int x1;
		private int y1;
		private int x2;
		private int y2;
		private int mOffset;
		private double mXOffset;
		private double mYOffset;
		private Color claimedColor;
		private boolean mIsHighlighted = false;
		private boolean mIsClaimed = false;

		private DrawableRoute(ClientRoute route, int offset)
		{
			mRoute = route;
			claimedColor = null;

			ClientCity city1 = (ClientCity) mRoute.getCities().get(0);
			ClientCity city2 = (ClientCity) mRoute.getCities().get(1);

			baseX1 = city1.getXCoordinate();
			baseX2 = city2.getXCoordinate();
			baseY1 = city1.getYCoordinate();
			baseY2 = city2.getYCoordinate();
			mOffset = offset;
		}

		public ClientRoute getClientRoute()
		{
			return mRoute;
		}

		private String getID()
		{
			return mRoute.getId();
		}

		private void draw(Graphics2D g2d)
		{
			//compute new x and y locations;
			x1 = (int) (baseX1 * mScale.getScaleX() + IMAGE_X);
			x2 = (int) (baseX2 * mScale.getScaleX() + IMAGE_X);
			y1 = (int) (baseY1 * mScale.getScaleY() + IMAGE_Y);
			y2 = (int) (baseY2 * mScale.getScaleY() + IMAGE_Y);

			if (mOffset != 0)
			{
				//get ratio of x to y
				int leftX = x2;
				int leftY = y2;
				int rightX = x1;
				int rightY = y1;
				if (x1 < x2 || (x1 == x2 && y1 < y2))
				{
					leftX = x1;
					leftY = y1;
					rightX = x2;
					rightY = y2;
				}


				AffineTransform rotation = new AffineTransform();

				double delX = rightX - leftX;
				double delY = rightY - leftY;
				double hyp = Math.sqrt(Math.pow(delX, 2) + Math.pow(delY, 2));
				//normalize the offset
				mXOffset = 0;
				mYOffset = 0;
				double routeOutlineAdjustmentX = 0;
				double routeOutlineAdjustmentY = 0;
				if (hyp != 0)
				{
					double[] pt = {delX * mOffset / hyp, delY * mOffset / hyp};
					AffineTransform.getRotateInstance(Math.toRadians(90), 0, 0)
							.transform(pt, 0, pt, 0, 1); // specifying to use this double[] to hold coords
					mXOffset = pt[0];
					mYOffset = pt[1];
				}

				x1 = (int) (mXOffset + leftX);
				x2 = (int) (mXOffset + rightX);
				y1 = (int) (mYOffset + leftY);
				y2 = (int) (mYOffset + rightY);
			}
//
//			if (Math.abs(x1 - x2) < Math.abs(y1 - y2))
//			{
//				x1 += mOffset;
//				x2 += mOffset;
//			} else
//			{
//				y1 += mOffset;
//				y2 += mOffset;
//			}
			g2d.setRenderingHint(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			if (mIsClaimed)
			{
				Color background;
				if (mIsHighlighted)
				{
					background = DEFAULT_HIGHLIGHTED_BACKGROUND;
				} else
				{
					background = DEFAULT_CLAIMED_BACKGROUND;
				}
				//draw line outline
				g2d.setColor(background);
				g2d.setStroke(new BasicStroke(CLAIMED_ROUTE_WIDTH + BACKGROUN_LINE_BUFFER, CAP_ENDS,
						BasicStroke
								.JOIN_MITER));
				g2d.drawLine(x1, y1, x2, y2);
				//draw line
				g2d.setStroke(new BasicStroke(CLAIMED_ROUTE_WIDTH, CAP_ENDS, BasicStroke.JOIN_MITER));
				g2d.setColor(claimedColor);
				g2d.drawLine(x1, y1, x2, y2);
				String num = String.valueOf(mRoute.getNumTrains());
				FontMetrics fm = g2d.getFontMetrics();
				Rectangle2D rect = fm.getStringBounds(num, g2d);
				g2d.setColor(Color.BLACK);
				g2d.drawString(String.valueOf(mRoute.getNumTrains()), (x1 + x2) / 2 - (int) (rect.getWidth() / 2),
						(y1 + y2) / 2 + (int) (rect.getHeight() / 2));
			} else
			{
				if (mIsHighlighted)
				{
					g2d.setColor(DEFAULT_HIGHLIGHTED_BACKGROUND);
					g2d.setStroke(new BasicStroke(CLAIMED_ROUTE_WIDTH + BACKGROUN_LINE_BUFFER, CAP_ENDS,
							BasicStroke
									.JOIN_MITER));
					g2d.drawLine(x1, y1, x2, y2);
				}

				//adjusted length for scale between points
				float length = (float) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
				int numTrains = mRoute.getNumTrains();
				float dashedLength = (numTrains) * (DASH[0] + DASH[1] + DASH[2] + DASH[3]);
//				float magnitude = (float)Math.sqrt(Math.pow(dashSolid,2) + Math.pow(dashEmpty,2));
				float factor = length / (dashedLength);
				float[] dash = new float[]{0, DASH[1] * factor, DASH[2] * factor, DASH[3] * factor};

				//daw outline line
				int dif = (int) (ROUTE_OUTLINE - ROUTE_WIDTH);
				float space = Math.max(0, dash[1] - dif);
				float[] outlineDash = new float[]{0, space, dash[2] + 2 * dif, space};
//				float outlineFactor = 2*ROUTE_OUTLINE/ROUTE_WIDTH;
				g2d.setStroke(new BasicStroke(ROUTE_OUTLINE, CAP_ENDS,
						BasicStroke.JOIN_MITER, MITER_LIMIT, outlineDash, factor * DASH_PHASE));
				g2d.setColor(borderRouteColors[mRoute.getType().ordinal()]);
//				int xa = Math.min(x1,x2);
//				int xb = Math.max(x1,x2)+1;
//				int ya = Math.min(y1,y2)-1;
//				int yb = Math.max(y1,y2)+1;
				g2d.drawLine(x1, y1, x2, y2);

				//draw fill
				g2d.setStroke(new BasicStroke(ROUTE_WIDTH, CAP_ENDS,
						BasicStroke.JOIN_MITER, MITER_LIMIT, dash, factor * DASH_PHASE));
				g2d.setColor(routeColors[mRoute.getType().ordinal()]);
				g2d.drawLine(x1, y1, x2, y2);
			}
		}

		private void setClaimed(Color playerColor)
		{
			claimedColor = playerColor;
			if (playerColor != null)
			{
				mIsClaimed = true;
			} else
			{
				mIsClaimed = false;
			}
		}

		/**
		 * Get value of Highlighted
		 *
		 * @return Returns the value of Highlighted
		 */
		public boolean isHighlighted()
		{
			return mIsHighlighted;
		}

		/**
		 * Set the value of highlighted variable
		 *
		 * @param highlighted The value to set highlighted member variable to
		 */
		public void setHighlighted(boolean highlighted)
		{
			mIsHighlighted = highlighted;
		}

		/**
		 * Get value of Claimed
		 *
		 * @return Returns the value of Claimed
		 */
		public boolean isClaimed()
		{
			return mIsClaimed;
		}

	}

}
