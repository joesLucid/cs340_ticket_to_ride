package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.LocalGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.presenter.game.GameBoardPresenter;
import edu.byu.cs340.presenter.game.IGameBoardPresenter;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.ViewUtils;
import edu.byu.cs340.view.game.IGameBoardView;
import edu.byu.cs340.view.game.ImageOnlyListRender;
import edu.byu.cs340.view.game.destinationCardPanel.DestinationCardList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by crown on 3/1/2017.
 * Represents the main screen, including the map, hand, bank, and list of players.
 */
public class GameBoardView extends MainView implements IGameBoardView
{
	private JPanel mRootPanel;
	private JButton mChatButton;
	private JPanel mPlayerListPanel;
	private JScrollPane mPlayerScrollPane;
	private JPanel mMapPanelHolder;
	private JPanel mBankNumbersPanel;
	private JPanel mBankCardsPanel;
	private JList mBankTrainList;
	private JLabel mNumberDestCardsLabel;
	private JLabel mNumberTrainCardsLabel;
	private JLabel mNumDiscardLabel;
	private JPanel mHandPanel;
	private JPanel mTrainCardHandPanel;
	private TrainHandList mTrainHandList;
	//	private JList<String> mHandTrainCards;
	private JLabel mUsernameLabel;
	private JPanel mHandBorderLayoutPanel;
	private JLabel mPlayerMessageLabel;
	private JScrollPane mHandScrollPane;
	private JButton mCancelButton;
	private JLabel mTopLabel;
	private JButton mLeaveGame;
	private JLabel mLastRoundLabel;
	private JLabel mGameBankLabel;
	private JPanel mBasePanel;
	private MapPanel mMapPanel;
	private JPanel mPlayerListBox;
	private DestinationCardList mDestinationCardList;
	private boolean mIsLastRound = false;

	private Map<String, Integer> mUsernameToIndex;
	private Map<String, PlayerCardPanel> mPlayerCardMap;
	private String currentTurnPlayer;

	private String mSelectedRouteID;

	//	private static final String SCORE = "Score: ";
//	private static final String NUM_TRAIN_CARDS = "# Train Cards: ";
//	private static final String NUM_DEST_CARDS = "# Dest. Cards: ";
//	private static final String NUM_TRAINS = "# Trains: ";
	//	private static final int BORDER_THICKNESS = 2;
	//colors of players in order, by player index
	private static final Color[] PLAYER_COLORS = {
			Color.red, Color.GREEN, Color.yellow, Color.magenta, Color.orange
	};
	public GAME_BOARD_ACTION mState;
	private static final String[] PLAYER_TRAIN_IMAGE_PATH = {
			"/game/redTrainPiece.png",
			"/game/greenTrainPiece.png",
			"/game/yellowTrainPiece.png",
			"/game/magentaTrainPiece.png",
			"/game/orangeTrainPiece.png"
	};

	public GameBoardView()
	{
		mChatButton.addActionListener((listener) -> ((GameBoardPresenter) getConnectedPresenter()).openChat());
		mCancelButton.addActionListener((listener) -> sendCancelOperation());
		mLeaveGame.addActionListener((listener) -> ((GameBoardPresenter) getConnectedPresenter()).leaveGame());

		mPlayerListBox = new JPanel();
		mPlayerListBox.setLayout(new BoxLayout(mPlayerListBox, BoxLayout.X_AXIS));
		mPlayerScrollPane.setViewportView(mPlayerListBox);
		mUsernameToIndex = new HashMap<>();
		mPlayerCardMap = new HashMap<>();

		mBankTrainList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		mBankTrainList.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				super.mousePressed(e);
				if (mState == GAME_BOARD_ACTION.DRAW_FIRST_TRAIN_CARD)
					sendDrawTrainCardFromZone(true);
				else if (mState == GAME_BOARD_ACTION.DRAW_SECOND_TRAIN_CARD)
					sendDrawTrainCardFromZone(false);
			}
		});

		mPlayerMessageLabel.setForeground(Color.WHITE);
		mLastRoundLabel.setForeground(Color.WHITE);

		mPlayerScrollPane.setBorder(BorderFactory.createEmptyBorder());
		mPlayerScrollPane.setOpaque(false);
		mPlayerScrollPane.getViewport().setOpaque(false);


		mMapPanel = new MapPanel(this::onRouteSelected, this::isRouteClaimable);
		//mState = GAME_BOARD_ACTION.DRAW_TRAIN_CARDS;
		mState = GAME_BOARD_ACTION.NOT_MY_TURN;//default state is to do nothing


		mMapPanel.setOpaque(false);
		mMapPanel.setVisible(true);
		mMapPanelHolder.add(mMapPanel);
		mMapPanelHolder.setVisible(true);

		mTrainHandList = new TrainHandList(this::onTrainCardsSelectedFromHand);
		mTrainCardHandPanel.add(mTrainHandList, BorderLayout.CENTER);
		mTrainHandList.setOpaque(false);
		mTrainCardHandPanel.setOpaque(false);

		mDestinationCardList = new DestinationCardList(BoxLayout.Y_AXIS, 0);


		mHandScrollPane.setViewportView(mDestinationCardList);
		mHandScrollPane.setBorder(BorderFactory.createEmptyBorder());
		mHandScrollPane.getViewport().setOpaque(false);
	}

	private boolean isRouteClaimable(String routeID)
	{
		return ((IGameBoardPresenter) getConnectedPresenter()).isRouteClaimable(routeID);
	}

	public void playerLeft(String username){
		if (mUsernameToIndex.containsKey(username))
		{
			Integer index = mUsernameToIndex.get(username);
			PlayerCardPanel panel = (PlayerCardPanel) mPlayerListBox.getComponent(index);
			panel.setLeftGame(true);
		}
	}

	//<editor-fold desc="Creating the GUI">
	private void createUIComponents()
	{

		mHandPanel = new JPanel(new BorderLayout())
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;
				int titleBoxHeight = mPlayerMessageLabel.getHeight() + 3;
				Color borderColor = new Color(100);
				if (mIsLastRound)
				{
					borderColor = new Color(150, 0, 0);
				}
				ViewUtils.drawRoundedTitleBox(g2d, 0, 0, mHandPanel.getWidth(), mHandPanel.getHeight(),
						titleBoxHeight, 4, 20,
						borderColor,
						borderColor,
						Color.white);

			}
		};
		mHandPanel.setOpaque(false);
		mHandPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		mBankNumbersPanel = new GameBankPanel(101, 122, this::sendDrawTrainCardFromDeck);
	}

	@Override
	public void onActivate()
	{
		super.onActivate();

		Map<String, ClientCity> cities = ((IGameBoardPresenter) getConnectedPresenter()).sendGetCities();
		Map<String, ClientRoute> routes = ((IGameBoardPresenter) getConnectedPresenter()).sendGetRoutes();
		String backgroundImage = ((IGameBoardPresenter) getConnectedPresenter()).sendGetBackgroundImage();
		mMapPanel.setCities(cities);
		mMapPanel.setRoutes(routes);
		mMapPanel.setBackgroundImagePath(backgroundImage);
//		addPlayers(((IGameBoardPresenter) getConnectedPresenter()).sendGetPlayers());
		mUsernameLabel.setText(getConnectedPresenter().getUsername());
		mIsLastRound = false;

	}

	//</editor-fold>


	//<editor-fold desc="Update View Methods and actions that can be performed">

	@Override
	public void addPlayers(List<LocalGamePlayer> players)
	{
		if (players != null)
		{
			for (LocalGamePlayer player : players)
			{
				addPlayer(player);
			}
		}
	}

	@Override
	public void addPlayer(IGamePlayer player)
	{
		if (player != null && !mPlayerCardMap.containsKey(player.getUsername()))
		{
			String imagePath = PLAYER_TRAIN_IMAGE_PATH[mPlayerCardMap.size()];
			Color color = PLAYER_COLORS[mPlayerCardMap.size()];
			PlayerCardPanel playerInfo = new PlayerCardPanel(player.getUsername(), imagePath, color);

			mUsernameToIndex.put(player.getUsername(), mPlayerCardMap.size());
			mPlayerCardMap.put(player.getUsername(), playerInfo);
			mPlayerListBox.add(playerInfo);
			mPlayerListBox.repaint();
			mPlayerListBox.revalidate();
		}
	}

	@Override
	public void setRouteAsClaimed(String routeID, String username)
	{
		mMapPanel.setClaimRoute(routeID, PLAYER_COLORS[mUsernameToIndex.get(username)]);
		mMapPanel.repaint();
		mMapPanel.revalidate();
	}


	@Override
	public void setPlayerTurn(String username)
	{
		if (currentTurnPlayer != null)
		{
			mPlayerCardMap.get(currentTurnPlayer).setUnFocusedPlayer();
		}
		currentTurnPlayer = username;
		mPlayerCardMap.get(username).setFocusedPlayer();
	}

	@Override
	public void updateIsLastRound()
	{
		mLastRoundLabel.setVisible(true);
		mIsLastRound = true;
	}

	@Override
	public void clearView()
	{
		//clear data fields
		mUsernameToIndex = new HashMap<>();
		mPlayerCardMap = new HashMap<>();
		currentTurnPlayer = null;
		mSelectedRouteID = null;
		currentTurnPlayer = null;


		mDestinationCardList.clearView();
		mTrainHandList.clearView();
		mPlayerListBox.removeAll();
		mBankTrainList.removeAll();
		mBankTrainList.clearSelection();
		mMapPanel.clearView();
		((GameBankPanel) mBankNumbersPanel).clearView();

		mIsLastRound = false;
		mLastRoundLabel.setVisible(false);

		setGameBoardAction(GAME_BOARD_ACTION.NOT_MY_TURN);
	}

	@Override
	public void updateClientPlayerDestinationCards(List<ClientDestinationCard> handDestinationCards)
	{
		if (handDestinationCards != null)
		{
			mDestinationCardList.setClientDestinationCards(handDestinationCards);
		}
		mDestinationCardList.revalidate();
		mDestinationCardList.repaint();
	}

	@Override
	public void updateClientPlayerTrainCards(List<ClientTrainCard> handTrainCards)
	{
		mTrainHandList.updateTrainCards(handTrainCards);
	}


	@Override
	public void updatePlayerDestCardCount(String player, int count)
	{
		PlayerCardPanel panel = mPlayerCardMap.get(player);
		if (panel != null)
		{
			panel.setNumDestCards(count);
		}
	}

	@Override
	public void updatePlayerTrainCardCount(String player, int count)
	{
		PlayerCardPanel panel = mPlayerCardMap.get(player);
		if (panel != null)
		{
			panel.setNumTrainCards(count);
		}
	}

	@Override
	public void updatePlayerTrainCount(String player, int count)
	{
		PlayerCardPanel panel = mPlayerCardMap.get(player);
		if (panel != null)
		{
			panel.setNumTrains(count);
		}
	}

	@Override
	public void updatePlayerScore(String player, int score)
	{
		PlayerCardPanel panel = mPlayerCardMap.get(player);
		if (panel != null)
		{
			panel.setScore(score);
		}
	}

	public void updatePlayerPrivateScore(String usuername, int privateScore)
	{
		PlayerCardPanel panel = mPlayerCardMap.get(usuername);
		if (panel != null)
		{

			System.out.println("actuall set score for " + usuername + " to " + privateScore);
			panel.setPrivateScore(privateScore);
		}
	}

	@Override
	public void updateTrainCardsInDeck(int deckSize)
	{
		((GameBankPanel) mBankNumbersPanel).updateTrainCardsInDeck(deckSize);
	}

	@Override
	public void updateDestCardsInDeck(int deckSize)
	{
		((GameBankPanel) mBankNumbersPanel).updateDestCardsInDeck(deckSize);
	}

	@Override
	public void updateTrainCardsInDiscard(int discardSize)
	{
		((GameBankPanel) mBankNumbersPanel).updateTrainCardInDiscard(discardSize);
	}

	@Override
	public void updateRevealedTrainCards(List<ClientTrainCard> revealedTrainCards)
	{
		mBankTrainList.removeAll();
		mBankTrainList.clearSelection();
		if (revealedTrainCards != null)
		{
			String[] pictureList = new String[revealedTrainCards.size()];
			for (int i = 0; i < pictureList.length; i++)
			{
				ClientTrainCard card = revealedTrainCards.get(i);
				if (card != null)
				{
					pictureList[i] = (card.getTrainType().getRotatedImage());
				}
			}
			ImageOnlyListRender renderer = new ImageOnlyListRender(61, 95);
			renderer.createImageMap(pictureList);
			mBankTrainList.setListData(pictureList);
			//causes number of rows to auto resize
			mBankTrainList.setVisibleRowCount(5);
			mBankTrainList.setCellRenderer(renderer);

			mBankTrainList.revalidate();
			mBankTrainList.repaint();
		}

	}

	public void updateLongestRoute(Set<String> usernames, int length)
	{
		for (String key : mPlayerCardMap.keySet())
		{
			if (usernames.contains(key))
			{
				mPlayerCardMap.get(key).setIsLongest(true, length);
			} else
			{
				mPlayerCardMap.get(key).setIsLongest(false, 0);
			}
		}
	}

	//</editor-fold>


	//<editor-fold desc="Getters">
	@Override
	public List<ClientTrainCard> getPlayerTrainCardsDisplayed()
	{
		return mTrainHandList.getAllCards();
	}

	@Override
	public JPanel getJPanel()
	{
		return mRootPanel;
	}
	//</editor-fold>


	//<editor-fold desc="Callbacks From User interaction and State Flow">
	public void setGameBoardAction(GAME_BOARD_ACTION action)
	{
		//TODO: Add in mState for drawing the second train card.
		//NOTE: These states are similar to the client model but SHOULD BE DIFFERENT, because we don't want to expose
		//the client models mState. The presenter will simple set this mState initially and these states will let the
		// flow happen
		//it may be worth refactoring to not use a switch, but that was the most simple spot to get started.
		mState = action;
		switch (mState)
		{
			case NOT_MY_TURN:
				mPlayerMessageLabel.setText(" ");
				mPlayerMessageLabel.repaint();
				mTrainHandList.setParametersForSelection(0, null);
				((GameBankPanel) mBankNumbersPanel).setClickable(false);
				mMapPanel.setSelectable(false);
				break;
			case SELECT_ACTION:
				mPlayerMessageLabel.setText("Select which action you'd like to perform");
				mPlayerMessageLabel.repaint();
				mTrainHandList.setParametersForSelection(0, null);
				((GameBankPanel) mBankNumbersPanel).setClickable(false);
				mMapPanel.setSelectable(false);
				break;
			case CLAIM_ROUTE:
				mPlayerMessageLabel.setText("Select a route to claim.");
				mPlayerMessageLabel.repaint();
				mTrainHandList.setParametersForSelection(0, null);
				((GameBankPanel) mBankNumbersPanel).setClickable(false);
				mMapPanel.setSelectable(true);
				break;
			case DRAW_FIRST_TRAIN_CARD:
				mPlayerMessageLabel.setText("Select which train card to draw, or select the deck to draw a random " +
						"card" +
						".");
				mPlayerMessageLabel.repaint();
				mTrainHandList.setParametersForSelection(0, null);
				((GameBankPanel) mBankNumbersPanel).setClickable(true);
				mMapPanel.setSelectable(false);
				repaint();
				break;
			case DRAW_SECOND_TRAIN_CARD:
				mPlayerMessageLabel.setText("Select which train card to draw, or select the deck to draw a random " +
						"card" +
						".");
				mTrainHandList.setParametersForSelection(0, null);
				((GameBankPanel) mBankNumbersPanel).setClickable(true);
				mMapPanel.setSelectable(false);
				break;
		}
	}

	public void onRouteSelected(ClientRoute routeSelected)
	{
		if (routeSelected == null)
		{
			mSelectedRouteID = null;
			mMapPanel.deselect();
			setGameBoardAction(GAME_BOARD_ACTION.CLAIM_ROUTE);
			System.out.println("Cleared Selected Route");
		} else
		{
			mSelectedRouteID = routeSelected.getId();
			mPlayerMessageLabel.setText("Select train cards to discard. (Right click to deselect)");
			mTrainHandList.setParametersForSelection(routeSelected.getNumTrains(), routeSelected.getType());
			System.out.println("Selected route: " + routeSelected.getId());
		}
	}


	private void onTrainCardsSelectedFromHand(List<String> trainCardIDs)
	{
		if (trainCardIDs != null)
		{
			mTrainHandList.clearSelected();
			mMapPanel.deselect();
			((IGameBoardPresenter) getConnectedPresenter()).sendClaimRoute(mSelectedRouteID, trainCardIDs);
			System.out.println("Selected train cards in hand");
//			setGameBoardAction(GAME_BOARD_ACTION.NOT_MY_TURN);
		}
	}

	private void sendDrawTrainCardFromDeck()
	{
		System.out.println("Draw from deck");
		((IGameBoardPresenter) getConnectedPresenter()).sendDrawTrainCardFromDeck();
//		if (mState == GAME_BOARD_ACTION.DRAW_FIRST_TRAIN_CARD)
//		{
//			setGameBoardAction(GAME_BOARD_ACTION.DRAW_SECOND_TRAIN_CARD);
//		}
//		else if (mState == GAME_BOARD_ACTION.DRAW_SECOND_TRAIN_CARD)
//		{
//			setGameBoardAction(GAME_BOARD_ACTION.NOT_MY_TURN);
//		}
	}


	public void sendDrawTrainCardFromZone(boolean allowLocomotive)
	{
		int zone = mBankTrainList.getSelectedIndex() + 1;
		boolean proceedWithDraw = false;
		//ask presenter if it was a locomotive
		// todo consider moving to presenter
		if (!allowLocomotive)
		{
			//test if the card is a locomotive
			if (((IGameBoardPresenter) getConnectedPresenter()).isRevealedTrainCardLocomotive(zone))
			{
				proceedWithDraw = false;
				mPlayerMessageLabel.setText("Select a non-Locomotive Card");
			} else
			{
				proceedWithDraw = true;
			}
		} else
		{
			proceedWithDraw = true;
		}
		if (proceedWithDraw)
		{
			System.out.println("Draw from zone " + zone);
			((IGameBoardPresenter) getConnectedPresenter()).sendDrawTrainCardFromZone(zone);
//			setGameBoardAction(GAME_BOARD_ACTION.NOT_MY_TURN);
		}
	}


	public void sendCancelOperation()
	{
		((IGameBoardPresenter) getConnectedPresenter()).sendCancelOperation();
	}

	//</editor-fold>


}
