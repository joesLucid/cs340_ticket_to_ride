package edu.byu.cs340.view.join_game;

import edu.byu.cs340.models.roomModel.IRoomInfo;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tjacobhi on 07-Feb-17.
 *
 * This is a models for the table that is used on the Join a Game screen.
 */
public class GamesListTableModel extends AbstractTableModel
{
	private static final String[] columnNames = {"Title", "Status", "Player Count"};
	private List<String[]> data = new ArrayList<>();

	@Override
	public int getRowCount()
	{
		return data.size();
	}

	@Override
	public int getColumnCount()
	{
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if (data == null)
		{
			return 0;
		} else if (data.isEmpty())
		{
			return 0;
		} else
		{
			return data.get(rowIndex)[columnIndex + 1];
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return getValueAt(0, columnIndex).getClass();
	}

	/**
	 * This is used to get the ID of a game on a given row
	 * @pre row is in the table
	 * @post results in a UUID for the game in the form of a string.
	 * @param row the row on the table
	 * @return the id of the game at the given row
	 */
	public String getGameID(int row)
	{
		return data.get(row)[0];
	}

	/**
	 * This is used to set the mData in the table with the given list of games
	 * @param games the list of games to display in the table
	 */
	public void setData(List<IRoomInfo> games)
	{
		data = new ArrayList<>(games.size());
		for (IRoomInfo info : games)
		{
			String[] row = new String[getColumnCount() + 1];
			row[0] = info.getRoomID();
			row[1] = info.getGameTitle();
			row[2] = "";
			row[3] = Integer.toString(info.getPlayers().size());
			data.add(row);
		}
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column)
	{
		return columnNames[column];
	}
}
