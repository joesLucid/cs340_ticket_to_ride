package edu.byu.cs340.view;

import edu.byu.cs340.models.roomModel.IRoomInfo;
import edu.byu.cs340.presenter.IJoinGamePresenter;
import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.view.join_game.GamesListTableModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * Created by tjacobhi on 02-Feb-17.
 *
 * The view that allows us to join a game
 */
public class JoinGameView extends MainView implements IJoinGameView
{
	// Constants
	private static final String TITLE_TEXT = "Join a Game";                                 // Title of screen
	private static final String SIGN_OUT_BUTTON_TEXT = "Sign Out";                          // Text for sign out button
	private static final String CREATE_BUTTON_TEXT = "Create Game";                         // Text for create game
	// button
	private static final String JOIN_GAME_BUTTON_TEXT = "Join Game";
	private static final String REFRESH_BUTTON_TEXT = "Refresh";
	private static final String CREATE_GAME_POPUP_TITLE = "Create a Game";                  // Title of popup
	private static final String CREATE_GAME_POPUP_LABEL = "Enter the name of the game:";    // message of popup
	private static final int EDGE_BUFFER = 10;                                              // Space between components

	// Layouts
	private LayoutManager mLayoutManager;

	// Tables
	private JTable mAvailableGamesTable;
	private JScrollPane mTableContainer;
	private GamesListTableModel mTableModel;

	// Buttons
	private JButton mSignOutButton;
	private JButton mCreateGameButton;
	private JButton mJoinGameButton;
	private JButton mRefreshButton;

	// Labels
	private JLabel mUsernameLabel;
	private JLabel mTitleLabel;
	private JLabel mMessage;


	private BufferedImage mBackgroundImage = null;
	private String mBackgroundPath = "/ticketToRideBox.jpg";

// Constructors

	/**
	 * Creates the view and sets up UI elements
	 */
	public JoinGameView()
	{
		setUpLayouts();
		setUpTables();
		setUpButtons();
		setUpLabels();
		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(mBackgroundPath));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	// Public methods
	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
		if (!(getConnectedPresenter() instanceof IJoinGamePresenter))
		{
			throw new InvalidPresenterException();
		}
	}

	@Override
	public void onActivate()
	{
		super.onActivate();
		mMessage.setText("");
		mUsernameLabel.setText(getConnectedPresenter().getUsername());
	}

	@Override
	public void pushCurrentGameList(List<IRoomInfo> gameInfoList)
	{
		mTableModel.setData(gameInfoList);
	}

	@Override
	public void invalidAction(String message)
	{
		mMessage.setText(message);
		mMessage.setForeground(Color.RED);
		System.out.println(message);
	}

	@Override
	public String getViewName()
	{
		return "JoinGame";
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(mBackgroundImage, 0, 0, getWidth(), getHeight(), this);
		g.setColor(ViewUtils.SEMI_TRANSPARENT);
		ViewUtils.drawRoundedRectBehindTextComponent((Graphics2D) g, mTitleLabel, 4, 5);
		ViewUtils.drawRoundedRectBehindTextComponent((Graphics2D) g, mUsernameLabel, 4, 5);

	}

	// Private Methods

	/**
	 * Used to setup any layouts the view may have
	 */
	private void setUpLayouts()
	{
		mLayoutManager = new SpringLayout();
		setLayout(mLayoutManager);
		this.setOpaque(false);
	}

	/**
	 * Sets up any tables the view may have
	 */
	private void setUpTables()
	{
		mTableModel = new GamesListTableModel();
		mAvailableGamesTable = new JTable(mTableModel);
		mAvailableGamesTable.setAutoCreateRowSorter(true);
		mTableContainer = new JScrollPane(mAvailableGamesTable);
		mTableContainer.getViewport().setOpaque(false);
		mTableContainer.setBackground(new Color(180, 180, 180, 150));
		mTableContainer.setBorder(BorderFactory.createEtchedBorder());
		add(mTableContainer);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.HORIZONTAL_CENTER,
				mTableContainer,
				0,
				SpringLayout.HORIZONTAL_CENTER,
				this);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.VERTICAL_CENTER,
				mTableContainer,
				0,
				SpringLayout.VERTICAL_CENTER,
				this);

		mAvailableGamesTable.setUpdateSelectionOnSort(true);
	}

	/**
	 * Sets up any buttons the view may have
	 */
	private void setUpButtons()
	{
		mSignOutButton = new JButton(SIGN_OUT_BUTTON_TEXT);
		add(mSignOutButton);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.EAST,
				mSignOutButton,
				-EDGE_BUFFER,
				SpringLayout.EAST,
				this);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.NORTH,
				mSignOutButton,
				EDGE_BUFFER,
				SpringLayout.NORTH,
				this);

		mSignOutButton.addActionListener((listener) -> getConnectedPresenter().signOut());

		mCreateGameButton = new JButton(CREATE_BUTTON_TEXT);
		add(mCreateGameButton);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.SOUTH,
				mCreateGameButton,
				-EDGE_BUFFER,
				SpringLayout.SOUTH,
				this);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.WEST,
				mCreateGameButton,
				EDGE_BUFFER,
				SpringLayout.WEST,
				this);

		mCreateGameButton.addActionListener((listener) -> onCreateGameButton());

		mJoinGameButton = new JButton(JOIN_GAME_BUTTON_TEXT);
		add(mJoinGameButton);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.SOUTH,
				mJoinGameButton,
				-EDGE_BUFFER,
				SpringLayout.SOUTH,
				this);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.EAST,
				mJoinGameButton,
				-EDGE_BUFFER,
				SpringLayout.EAST,
				this);

		mJoinGameButton.addActionListener((listener) -> onJoinGameButton());

		mRefreshButton = new JButton(REFRESH_BUTTON_TEXT);
		add(mRefreshButton);
		mRefreshButton.addActionListener((listener) -> ((IJoinGamePresenter) getConnectedPresenter()).refresh());

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.WEST,
				mRefreshButton,
				0,
				SpringLayout.WEST,
				mTableContainer);
		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.NORTH,
				mRefreshButton,
				EDGE_BUFFER,
				SpringLayout.SOUTH,
				mTableContainer);
	}

	/**
	 * Sets up any labels that the view may have
	 */
	private void setUpLabels()
	{
		mUsernameLabel = new JLabel();
		add(mUsernameLabel);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.EAST,
				mUsernameLabel,
				-EDGE_BUFFER,
				SpringLayout.WEST,
				mSignOutButton);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.VERTICAL_CENTER,
				mUsernameLabel,
				0,
				SpringLayout.VERTICAL_CENTER,
				mSignOutButton);

		mTitleLabel = new JLabel(TITLE_TEXT);
		Font defaultFont = mTitleLabel.getFont();
		mTitleLabel.setFont(new Font(defaultFont.getName(), Font.PLAIN, 20));
		add(mTitleLabel);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.WEST,
				mTitleLabel,
				EDGE_BUFFER,
				SpringLayout.WEST,
				this);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.NORTH,
				mTitleLabel,
				EDGE_BUFFER,
				SpringLayout.NORTH,
				this);
		mMessage = new JLabel();
		add(mMessage);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.HORIZONTAL_CENTER,
				mMessage,
				0,
				SpringLayout.HORIZONTAL_CENTER,
				this);

		((SpringLayout) mLayoutManager).putConstraint(SpringLayout.NORTH,
				mMessage,
				EDGE_BUFFER,
				SpringLayout.NORTH,
				this);

	}

	/**
	 * This is called when an action fires to join a game
	 */
	private void onJoinGameButton()
	{
		String gameID = null;
		int selectedRow = mAvailableGamesTable.getSelectedRow();
		if (selectedRow != -1)
		{
			int rowSelected = mAvailableGamesTable.convertRowIndexToModel(mAvailableGamesTable.getSelectedRow());
			if (rowSelected >= 0)
				gameID = mTableModel.getGameID(rowSelected);
			if (gameID != null)
				((IJoinGamePresenter) getConnectedPresenter()).joinGame(gameID);
		}
	}

	/**
	 * This is called when the create game button is pressed
	 */
	private void onCreateGameButton()
	{
		String gameTitle = JOptionPane.showInputDialog(this,
				CREATE_GAME_POPUP_LABEL,
				CREATE_GAME_POPUP_TITLE,
				JOptionPane.PLAIN_MESSAGE);
		if (gameTitle != null)
			((IJoinGamePresenter) getConnectedPresenter()).createGame(gameTitle);
	}
}
