package edu.byu.cs340.clientFacade.Event;

/**
 * Created by joews on 3/15/2017.
 */
public interface EventKeys
{
	String USERNAME = "username"; //key to string
	String PASSWORD = "password"; //key to string
	String CONFIRM_PASSWORD = "confirm_password"; //key to string

	String ROUTE_ID = "route_id"; //key to string
	String TRAIN_CARD_LIST = "train_cards";//key to List<ClientTrainCards>
	String TRAIN_CARD_ID_LIST = "train_card_ids"; // key to List<String> which correspond to train card ids

	String DESTINATION_CARD_ID_LIST = "destination_cards";//key to List<String> which correspond to destination card ids

	String TRAIN_CARD_ZONE = "train_card_zone";// int of the zone the train card is in
	String CAN_DRAW_ANOTHER = "can_draw_another";//boolean if the player can draw another card

	String LOBBY_ID = "lobby_id"; //string of the lobby id
	String GAME_ID = "game_id"; //string of the lobby id

	String LOBBY_NAME = "lobby_name"; // string of te lobby name

	String PLAYER_NAME = "player_name"; //string of the players nam
	String IS_READY = "is_ready"; //boolean if is ready
	String MIN_CARD_TO_KEEP = "max_to_return";//in saying the max number of cards to return

	String ERROR_CODE = "error_code";//int of error code
	String ERROR_MESSAGE = "error_message";//string of the error message
}