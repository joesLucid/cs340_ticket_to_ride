package edu.byu.cs340.clientFacade.Event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joews on 3/15/2017.
 */
public class Event
{
	private Map<String, Object> mData;
	private EventType mType;

	public Event(EventType type)
	{
		mType = type;
		mData = new HashMap<>();
	}

	/**
	 * Put an object into the event. Items put in should be defined in the EventKeys class as static strings.
	 * This will work without that, but they should be there anyway.
	 *
	 * @param key  Key to store data under
	 * @param data Object to add.
	 * @return Retruns this object for ease of multipl put statements.
	 */
	public Event put(String key, Object data)
	{
		if (!mData.containsKey(key))
		{
			mData.put(key, data);
			return this;
		} else
		{
			return this;
		}
	}

	public boolean hasKey(String key)
	{
		return mData.containsKey(key);
	}

	/**
	 * Gets the object associated with the key. Keys are defined in the EventKeys class.
	 * @param key String describing the data to get.
	 * @return Object associated with the key.
	 */
	public Object get(String key)
	{
		return mData.get(key);
	}

	/**
	 * Gets the type of the event.
	 * @return The EventType associated with the event.
	 */
	public EventType getEventType()
	{
		return mType;
	}
}
