package edu.byu.cs340.clientFacade.Event;

/**
 * Created by joews on 3/16/2017.
 */ //event types
public enum EventType
{

	UILogin,
	CMDLogin,
	UIRegister,

	UISignOut,
	CMDSignOut,

	UIPullGames,
	UIJoinGame,
	CMDJoinGame,
	CMDGameListUpdated,
	UICreateLobby,
	CMDCreateLobby,

	UIReadyStatus,
	CMDReadyStatus,
	UIStartGame,
	CMDStartGame,
	UILeaveLobby,
	UIRemoveLobbyPlayer,
	CMDRemoveLobbyPlayer,
	UIPollLobby,

	//<editor-fold desc="GAME PLAY">
	//<editor-fold desc="Actions to pick from by the pick action state">
	UIClaimRoutePicked,
	UIDrawDestinationCardsPicked,
	UIDrawTrainCardsPicked,
	//</editor-fold>

	UIClaimRoute,
	CMDClaimRoute,

	CMDDrawDestinationCards,
	UIReturnDestinationCards,
	CMDReturnDestinationCards,

	UIDrawTrainCardFromDeck,
	CMDDrawTrainCard,

	CMDNextPlayerTurn,
	CMDGameOver,
	//</editor-fold>

	UICancel,
	CMDErrorMessage,

	UILeaveGame,
	UIPollGame,
	UIDrawTrainCardFromZone,
	UIDrawDestinationCards,
	CMDLeaveGame
}
