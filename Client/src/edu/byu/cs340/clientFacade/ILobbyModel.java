package edu.byu.cs340.clientFacade;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 * The ILobbyModel is an interface the gameLobbyPresenter uses to interface with the models. This is used to hide
 * functionality of the client facade.
 */
public interface ILobbyModel extends IClientFacade, IUIModel, IChatModel
{
	/**
	 * Adds and sends the message to chat.
	 *
	 * @param message The message to send
	 * @pre message!=null
	 * @pre the player is in a lobby
	 * @post the message will be added to the chat history
	 */
	void sendLobbyChatMessage(String message);


	/**
	 * Sends a signal to mPlayers that the player is ready
	 *
	 * @pre the player is in a lobby
	 * @post the player will be marked as ready
	 */
	void sendReadySignal();

	/**
	 * Sends a signal to mPlayers that the player is not ready
	 *
	 * @pre the player is in a lobby
	 * @post the player will be marked as not ready
	 */
	void sendUnreadySignal();

	/**
	 * Sends a signal to mPlayers that the game is starting. This will create the game from the lobby and change the
	 * game
	 * state appropriate.
	 *
	 * @pre the player is in a lobby
	 * @post if everyone is ready the game will start and the state of the game will be changed, otherwise nothing will
	 * change. A notification will be sent on updating observers if unsuccessful.
	 */
	void sendStartSignal();

	/**
	 * Sends a signal to mPlayers that the player is leaving the lobby. The host will be migrating if there are
	 * mPlayers otherwise close the lobby.
	 *
	 * @pre the player is in a lobby
	 * @post the player will be removed from the lobby. The lobby will migrate host or close the lobby. State changed
	 * back to the games list state.
	 */
	void sendLeaveSignal();

	/**
	 * Sends a signal to mPlayers that the player is kicked and the player then is removed from the game.
	 *
	 * @pre the player is in a lobby
	 * @post if the username is associated with a player in the lobby they will be kicked from the game
	 */
	void sendRemovePlayerSignal(String username);

	/**
	 * Returns the players in the lobby
	 *
	 * @return Players in the lobby based on the local models.
	 */
	List<ILobbyPlayer> getLobbyPlayers();

	/**
	 * Returns whether the the lobby player associated with the client's user is ready.
	 *
	 * @return Whether the clients lobby player is ready.
	 */
	boolean getAmIReady();

	/**
	 * Returns whether the the lobby player associated with the client's user is the host.
	 *
	 * @return Whether the clients lobby player is the host.
	 */
	boolean getAmIHost();

	/**
	 * Gets the name of the current lobby.
	 *
	 * @return The name of the lobby.
	 */
	String getLobbyName();
}
