package edu.byu.cs340.clientFacade;

import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;

import java.util.List;
import java.util.Map;

/**
 * Created by Sean on 2/21/2017.
 *
 * Declares functionality for a client working with the models during game play
 */
public interface IGameModel extends IUIModel, IClientFacade, IChatModel
{
	/**
	 * Creates a command to claim the route specified using the bank specified. Creates an observable message to
	 * return the results.
	 *
	 * @param routeID the unique ID for the route to be claimed
	 * @param cards   a list of card IDs to be used in claiming the route
	 * @pre routeID is a valid route
	 * @pre bank are all valid bank
	 * @pre bank referenced match the route's color or are locomotives
	 * @post created an ObserverMessage with the results of the command's execution
	 */
	void claimRoute(String routeID, List<String> cards);

	/**
	 * Draws from deck
	 */
	void drawTrainCardFromDeck();

	/**
	 * Creates a command to draw a train card from the specified zone. Creates an observable message to return the
	 * results.
	 *
	 * @param zone the index of the place to draw the card from (1-5 = the respective face-up position)
	 * @pre zone is between 1-5 inclusive
	 * @post created an ObserverMessage with the results of the command's execution
	 */
	void drawTrainCardFromZone(int zone);
	
	/**
	 * Creates a command to keep the chosen destination bank. Creates an observable message to return the results.
	 *
	 * @param cardIDs a list of card IDs to be kept
	 * @pre cardIDs contains at least one ID
	 * @post created an ObserverMessage with the results of the command's execution
	 */
	void chooseDestinationCards(List<String> cardIDs, int mMinToKeep);

	/**
	 * Sends the chat for the game.
	 *
	 * @param message The message to send
	 * @pre message!=null
	 * @post the message will be added to the chat history
	 */
	void sendGameChatMessage(String message);
	
	/**
	 * @return a list containing the train cards in the client players hand
	 */
	List<ClientTrainCard> getTrainCards();
	/**
	 * @return a list containing the destination cards that the client player owns
	 */
	List<ClientDestinationCard> getDestinationCards();

	/**
	 * @return a map of IDs to cities
	 */
	Map<String, ClientCity> getCities();

	/**
	 * @return a map of IDs to routes
	 */
	Map<String, ClientRoute> getRoutes();

	/**
	 * @return the full path to the background image for the map
	 */
	String getBackgroundImage();
	/**
	 * @return a list of Players sorted by turn order
	 */
	List<IGamePlayer> getPlayers();
	/**
	 * @return an array containing the face-up cards that can be drawn
	 */
	ClientTrainCard[] getFaceUpCards();
	/**
	 * @return an array containing destination cards to be chosen from
	 */
	ClientDestinationCard[] getTemporaryDestinationCards();

	/**
	 * Selects which action the player wants to do for the turn. THis will cause the state to change.
	 *
	 * @param option Int corresponding to the static variables in the GamePresenterProxy class
	 * @pre option in range [1,3]
	 */
	void selectActionForTurn(int option);

	/**
	 * Called by presetners who want to cancel an action the user selected.
	 *
	 * @post will only do something based on the states ability to cancel.
	 */
	void uiCancel();

	/**
	 * Removes the player from the game.
	 *
	 * @post the state is join game state
	 */
	void leaveGame();

	boolean isRouteClaimable(String routeID);

	boolean canDrawDestCards();

	boolean canDrawTrainCards();
}
