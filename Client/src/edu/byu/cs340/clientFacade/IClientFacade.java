package edu.byu.cs340.clientFacade;

/**
 * Created by joews on 2/4/2017.
 *
 * This is the base interface that all other ClientFacade interfaces must inherit.
 */
public interface IClientFacade
{
	/**
	 * Sets up the client facade
	 */
	void setUp();
}
