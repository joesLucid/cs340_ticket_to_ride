package edu.byu.cs340.clientFacade;

/**
 * Created by tjacobhi on 04-Feb-17.
 *
 * This is the interface that all UI models must implement
 */
public interface IUIModel extends IClientState
{
	/**
	 * Signs the user out of hte server and returns to login.
	 */
	void signOut();

	/**
	 * Gets the username associated with the client.
	 * @return The username of hte client.
	 */
	String getUserName();

}
