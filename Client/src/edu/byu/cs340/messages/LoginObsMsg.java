package edu.byu.cs340.messages;

/**
 * Created by joews on 2/2/2017.
 *
 * Message notifying observers of the status of a login.
 */
public class LoginObsMsg extends StringObsMsg
{
	public LoginObsMsg(String message)
	{
		super(message);
	}
}
