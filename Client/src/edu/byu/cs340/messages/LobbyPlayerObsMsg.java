package edu.byu.cs340.messages;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;

/**
 * Created by joews on 2/10/2017.
 *
 * message notifying that a player has changed.
 */
public abstract class LobbyPlayerObsMsg extends StringObsMsg
{
	/**
	 * Player who has changed.
	 */
	protected ILobbyPlayer player;

	public LobbyPlayerObsMsg(String message, ILobbyPlayer player)
	{
		super(message);
		this.player = player;
	}

	/**
	 * Get value of Player
	 *
	 * @return Returns the value of Player
	 */
	public ILobbyPlayer getPlayer()
	{
		return player;
	}

	/**
	 * Set the value of player variable
	 *
	 * @param player The value to set player member variable to
	 */
	public void setPlayer(LobbyPlayer player)
	{
		this.player = player;
	}
}
