package edu.byu.cs340.messages;

/**
 * Created by joews on 3/6/2017.
 */
public class ChatUpdatedObsMsg extends StringObsMsg
{
	public ChatUpdatedObsMsg(String message)
	{
		super(message);
	}
}
