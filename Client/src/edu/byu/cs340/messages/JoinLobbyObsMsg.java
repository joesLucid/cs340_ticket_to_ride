package edu.byu.cs340.messages;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;

/**
 * Created by Sean on 2/6/2017.
 *
 *  Message notifying observer of the status of joining the lobby
 */
public class JoinLobbyObsMsg extends LobbyPlayerObsMsg
{
	public JoinLobbyObsMsg(String message, ILobbyPlayer player)
	{
		super(message, player);
	}
}
