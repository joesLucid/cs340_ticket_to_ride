package edu.byu.cs340.messages;

/**
 * Created by joews on 2/5/2017.
 *
 * Message notifying observers of status of creating a lobby
 */
public class CreateLobbyObsMsg extends StringObsMsg
{
	public CreateLobbyObsMsg(String message)
	{
		super(message);
	}
}
