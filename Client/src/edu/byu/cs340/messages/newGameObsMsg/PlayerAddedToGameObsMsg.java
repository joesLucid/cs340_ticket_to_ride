package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class PlayerAddedToGameObsMsg implements GameObsMsg
{
	private IGamePlayer mGamePlayer;

	public PlayerAddedToGameObsMsg(IGamePlayer gamePlayer)
	{
		mGamePlayer = gamePlayer;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateAddPlayer(mGamePlayer);
	}
}
