package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;

/**
 * Created by joews on 3/20/2017.
 */
public abstract class IntObsMsg implements IObsMsg
{
	private int mInt;

	public IntObsMsg(int anInt)
	{
		mInt = anInt;
	}

	/**
	 * Get value of Int
	 *
	 * @return Returns the value of Int
	 */
	public int getNumber()
	{
		return mInt;
	}
}
