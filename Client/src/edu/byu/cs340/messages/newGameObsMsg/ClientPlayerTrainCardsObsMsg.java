package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.presenter.IGamePresenter;

import java.util.List;

/**
 * Created by joews on 3/20/2017.
 */
public class ClientPlayerTrainCardsObsMsg implements IObsMsg, GameObsMsg
{
	private List<ClientTrainCard> mCardsInHand;

	public ClientPlayerTrainCardsObsMsg(List<ClientTrainCard> cardsInHand)
	{
		mCardsInHand = cardsInHand;
	}

	/**
	 * Get value of CardsInHand
	 *
	 * @return Returns the value of CardsInHand
	 */
	public List<ClientTrainCard> getCardsInHand()
	{
		return mCardsInHand;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateClientPlayerTrainCards(mCardsInHand);
	}
}
