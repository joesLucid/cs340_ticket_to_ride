package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class RouteClaimedObsMsg implements IObsMsg, GameObsMsg
{
	private String mRouteID;
	private String mClaimingPlayer;

	public RouteClaimedObsMsg(String routeID, String claimingPlayer)
	{
		mRouteID = routeID;
		mClaimingPlayer = claimingPlayer;
	}

	/**
	 * Get value of RouteID
	 *
	 * @return Returns the value of RouteID
	 */
	public String getRouteID()
	{
		return mRouteID;
	}

	/**
	 * Get value of ClaimingPlayer
	 *
	 * @return Returns the value of ClaimingPlayer
	 */
	public String getClaimingPlayer()
	{
		return mClaimingPlayer;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateRouteClaimed(mClaimingPlayer, mRouteID);
	}
}
