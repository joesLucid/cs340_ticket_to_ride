package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class GameChatObsMsg implements GameObsMsg
{
	private ChatMessage mChatMessage;

	public GameChatObsMsg(ChatMessage chatMessage)
	{
		mChatMessage = chatMessage;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateAddChatMessage(mChatMessage);
	}
}
