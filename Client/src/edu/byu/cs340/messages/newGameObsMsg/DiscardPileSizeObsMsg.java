package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class DiscardPileSizeObsMsg extends IntObsMsg implements GameObsMsg
{
	public DiscardPileSizeObsMsg(int anInt)
	{
		super(anInt);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateTrainDiscardSize(getNumber());
	}
}
