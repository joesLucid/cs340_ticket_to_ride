package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public interface GameObsMsg extends IObsMsg
{
	void updateUI(IGamePresenter presenter);
}
