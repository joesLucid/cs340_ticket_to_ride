package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class GameOverObsMsg implements IObsMsg, GameObsMsg
{
	private IGameOverData mGameOverData;

	public GameOverObsMsg(IGameOverData data)
	{
		this.mGameOverData = data;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateGameOver(mGameOverData);
	}
}
