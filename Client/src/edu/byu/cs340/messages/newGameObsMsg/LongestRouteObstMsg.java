package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

import java.util.Set;

/**
 * Created by joews on 3/28/2017.
 */
public class LongestRouteObstMsg implements GameObsMsg
{
	int mLength;
	Set<String> mUserNames;

	public LongestRouteObstMsg(Set<String> strings, int length)
	{
		mUserNames = strings;
		mLength = length;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateLongestRoute(mUserNames, mLength);
	}
}
