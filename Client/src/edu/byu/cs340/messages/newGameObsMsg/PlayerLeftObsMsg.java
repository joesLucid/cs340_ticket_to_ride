package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/27/2017.
 */
public class PlayerLeftObsMsg extends StringObsMsg implements GameObsMsg
{
	public PlayerLeftObsMsg(String string)
	{
		super(string);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updatePlayerLeft(getString());
	}
}
