package edu.byu.cs340.command;


import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.SignOutObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.ClientUser;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;

/**
 * Created by Rebekah on 2/8/2017.
 */
public class ClientSignOutCommand extends SignOutCommand
{
	/**
	 * @param username
	 * @param lobbyID  Nullable
	 * @param gameID   Nullable
	 * @pre username is not null
	 */
	public ClientSignOutCommand(String username, String lobbyID, String gameID)
	{
		super(username, lobbyID, gameID);
	}

	/**
	 * Signs the client out. This is accomplished on the lcient by simply removing the autheroization token.
	 *
	 * @return Returns a SignoutObserverMessage if successful, na ErrorObserverMesage otherwise.
	 */

	@Override
	public IMessage execute()
	{
		IMessage message = null;
		ClientLobbyModel lobby = ClientModelManager.getInstance().getLobby();
		ClientUser user = ClientModelManager.getInstance().getUser();
		if (mUsername != null && user.getAuthenticationToken() != null && mUsername.equals(user.getUserName()))
		{
			//take token from user*/
			user.setAuthenticationToken(null);
			message = new SignOutObsMsg("you are Signed out");
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}
}
