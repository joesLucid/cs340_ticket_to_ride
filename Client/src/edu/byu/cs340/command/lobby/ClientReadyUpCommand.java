package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.ReadyChangeObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;

/**
 * Created by joews on 2/1/2017.
 *
 * Command to ready up or deready up on the client side.
 */
public class ClientReadyUpCommand extends ReadyUpCommand
{
	public ClientReadyUpCommand(String username, int lastCommand, String lobbyID, boolean isReady)
	{
		super(username, lastCommand, lobbyID, isReady);
	}

	/**
	 * Readies the player username.
	 *
	 * @return returns either a ReadychangeObserveRMessage if succesfull with an added argument of player. Otherwise
	 * returns ErrorMessage.
	 */
	@Override
	public IMessage execute()
	{
		ClientLobbyModel clientLobbyModel = ClientModelManager.getInstance().getLobby();
		LobbyPlayer player = clientLobbyModel.getPlayer(mUsername);
		IMessage message = null;
		//verify that the username and password match
		if (player != null)
		{
			player.setReady(mIsReady);
			clientLobbyModel.addCommand(this);
			message = new ReadyChangeObsMsg(null, player);
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDReadyStatus));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}
}
