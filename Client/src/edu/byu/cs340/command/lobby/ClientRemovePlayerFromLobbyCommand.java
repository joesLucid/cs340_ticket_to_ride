package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.LobbyPlayerRemovedObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;

/**
 * Created by joews on 2/1/2017.
 *
 * Command to kick player on the client side.
 */
public class ClientRemovePlayerFromLobbyCommand extends RemovePlayerFromLobbyCommand
{
	public ClientRemovePlayerFromLobbyCommand(String username, int lastCommand, String lobbyID, String
			userNameToRemove)
	{
		super(username, lastCommand, lobbyID, userNameToRemove);
	}

	/**
	 * Removes the user to be kicked from the game.
	 *
	 * @post ClientUser models is updated.
	 * @post observers notified with an LobbyPlayerRemovedObsMsg specifying what has change.
	 */
	@Override
	public IMessage execute()
	{
		IMessage message = null;
		ClientLobbyModel lobby = ClientModelManager.getInstance().getLobby();
		//If the player to remove is me then clear the lobby data
		if (userNameToRemove == null)
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		} else
		{
			LobbyPlayer playerRemove = lobby.getPlayer(userNameToRemove);
			LobbyPlayer playerHost = lobby.getPlayer(newHost);
			message = new LobbyPlayerRemovedObsMsg(null, playerRemove, playerHost);
			lobby.setHost(newHost);
			lobby.removePlayer(userNameToRemove);
			lobby.addCommand(this);
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDRemoveLobbyPlayer).put(EventKeys
					.PLAYER_NAME, userNameToRemove));
		}
		return message;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		return super.strip(userStripping);
	}
}
