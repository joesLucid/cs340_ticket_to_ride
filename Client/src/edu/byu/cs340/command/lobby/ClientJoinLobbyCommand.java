package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.JoinLobbyObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;
import edu.byu.cs340.models.roomModel.IPlayer;

/**
 * Created by joews on 2/8/2017.
 *
 * Command to join a lobby from the game list view.
 */
public class ClientJoinLobbyCommand extends JoinLobbyCommand
{
	public ClientJoinLobbyCommand(String username, String lobbyID, IPlayer joiningPlayer)
	{
		super(username, lobbyID, joiningPlayer);
	}

	/**
	 * Adds the mJoingingPlayer to the lobby associated with mLobbyID.
	 *
	 * @return Returns a JoinLobbyObserveRMessage if success, ErrorMessage otherwise.
	 */
	@Override
	public IMessage execute()
	{
		IMessage message = null;
		//check if the lobbyID or username are null
		if (mJoiningPlayer != null && mLobbyID != null
				&& mUsername != null && mLobbyName != null)
		{
			//create the lobby and set it as the clients lobby
			ClientLobbyModel clientLobby = ClientModelManager.getInstance().getLobby();
//			clientLobby.setPlayerManager(mLobbyState);
			clientLobby.setRoomID(mLobbyID);
			clientLobby.setRoomName(mLobbyName);
			LobbyPlayer lobbyPlayer = (LobbyPlayer) mJoiningPlayer;
			//add  player
			clientLobby.addPlayer(lobbyPlayer.getUsername());
			//set ready status
			if (lobbyPlayer.isReady())
			{
				clientLobby.readyUser(lobbyPlayer.getUsername());
			} else
			{
				clientLobby.unReadyUser(lobbyPlayer.getUsername());
			}
			//set host status
			if (lobbyPlayer.isHost())
			{
				clientLobby.setHost(lobbyPlayer.getUsername());
			}
			clientLobby.addCommand(this);
			message = new JoinLobbyObsMsg(null, lobbyPlayer);

			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDJoinGame).put(EventKeys
					.PLAYER_NAME, mUsername));

		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}
}
