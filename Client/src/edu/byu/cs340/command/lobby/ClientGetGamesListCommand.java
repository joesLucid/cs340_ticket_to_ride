package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.GameListUpdateObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;

/**
 * Created by joews on 2/7/2017.
 *
 * Command to get the list of games and update hte models version.
 */
public class ClientGetGamesListCommand extends GetGamesListCommand
{
	public ClientGetGamesListCommand(String username, IRoomsListInfo roomsListInfo)
	{
		super(username, roomsListInfo);
	}

	/**
	 * SEts the local models game list to the stored IRoomListInfo.
	 *
	 * @return Returns a GameListUpdateObsMsg if successfull, or an ErrorObsMsg if not.
	 */
	@Override
	public IMessage execute()
	{
		if (mRoomsListInfo != null)
		{
			ClientModelManager.getInstance().setGamesListInfo(mRoomsListInfo);
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDGameListUpdated));
			return new GameListUpdateObsMsg(null);
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return null;
	}

}
