package edu.byu.cs340.command.login;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.LoginObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.ClientUser;

/**
 * Created by joews on 2/1/2017.
 *
 * Command to login user on the client side
 */
public class ClientLoginCommand extends LoginCommand
{
	public ClientLoginCommand(String username, String password, String authenticationToken)
	{
		super(username, password, authenticationToken);
	}

	/**
	 * Sets the ClientUser object's parameters to the username, apssword, and authentication token. Notifies observers
	 *
	 * @post ClientUser models is updated.
	 * @post observers notified with an IMessageObserverMessage specifing what has change.
	 */
	@Override
	public IMessage execute()
	{
		IMessage message = null;
		ClientUser user = ClientModelManager.getInstance().getUser();
		if (mUsername != null
				&& mPassword != null
				&& mAuthenticationToken != null)
		{
			//login successfull
			user.setPassword(mPassword);
			user.setAuthenticationToken(mAuthenticationToken);
			user.setUserName(mUsername);
			//Tell the state that login was updated
			ClientModelManager.getInstance().getState().processEvent(new Event(EventType.CMDLogin));
			message = new LoginObsMsg(null);
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}

}
