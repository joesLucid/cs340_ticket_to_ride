package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.map.IRoute;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crown on 3/18/2017.
 */
public class ClientClaimRouteCommand extends ClaimRouteCommand
{
	public ClientClaimRouteCommand(String username, int lastCommand, String gameID,
	                               String routeID, List<ITrainCard> cardsUsed)
	{
		super(username, lastCommand, gameID, routeID, cardsUsed);
	}
	public ClientClaimRouteCommand(ClaimRouteCommand that)
	{
		super(that);
	}
	@Override
	public IMessage execute() throws CommandException
	{
		IClientGameModel gameModel = ClientModelManager.getInstance().getGame();
		gameModel.claimRoute(mRouteID, mUsername);

		List<ClientTrainCard> clientTrainCards = new ArrayList<>();
		for (ITrainCard card : mCardsUsed)
		{
			clientTrainCards.add(new ClientTrainCard(card));
		}
		gameModel.removeTrainCards(mUsername, clientTrainCards);

		ClientTrainCard[] revealed = new ClientTrainCard[mRevealed.size()];
		for (int i = 0; i < mRevealed.size(); i++)
		{
			revealed[i] = mRevealed.get(i) == null ? null : new ClientTrainCard(mRevealed.get(i));
		}
		gameModel.setRevealedCards(revealed);

		for (String name : mNewScores.keySet())
		{
			gameModel.updatePlayerScore(name, mNewScores.get(name));
		}
		gameModel.updateRemainingTrains(mNewNumTrains, mUsername);
		gameModel.updateTrainDiscardPileSize(mNewTrainDiscardSize);
		gameModel.updateTrainDeckSize(mNewTrainDeckSize);
		gameModel.updateLongestRoute(mUsersLongestRoute, mLengthLongestRoute);
		if (gameModel.getClientPlayer().getUsername().equals(mUsername))
		{
			System.out.println("Should update " + mUsername + " score to " + mPrivateScore);
			gameModel.updateLocalPlayerPrivateScore(mPrivateScore);
		}

		gameModel.addCommand(this);
		ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDClaimRoute));
		return null;
	}
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(mUsername).append(" claimed a route");
		try
		{
			IClientGameModel game = ClientModelManager.getInstance().getGame();
			IRoute route = game.getRoute(mRouteID);
			sb.append(" from " + route.getCities().get(0).getName() + " to " + route.getCities().get(1).getName());
			sb.append("and discarded " + mCardsUsed.size() + " train cards.\n");
		} catch (NullPointerException e)
		{
			sb.append(".\n");
		}
		return sb.toString();
	}
}
