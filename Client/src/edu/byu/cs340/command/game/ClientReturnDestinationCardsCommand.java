package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.DummyDestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class ClientReturnDestinationCardsCommand extends ReturnDestinationCardsCommand
{
	public ClientReturnDestinationCardsCommand(ReturnDestinationCardsCommand that)
	{
		super(that);
	}

	public ClientReturnDestinationCardsCommand(String username, int lastCommand, String gameID, int
			maxDestinationCardsToReturn, List<IDestinationCard> cardsToReturn)
	{
		super(username, lastCommand, gameID, maxDestinationCardsToReturn, cardsToReturn);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage message = null;
		//check if the lobbyID or username are null
		if (mGameID != null && mUsername != null)
		{
			IClientGameModel game = ClientModelManager.getInstance().getGame();
			if (mUsername.equals(game.getClientPlayer().getUsername()))
			{
				List<ClientDestinationCard> tempCards = new ArrayList<>();
				//TODO: Change because types are inconsistent...
				for (ClientDestinationCard tempCard : (Arrays.asList(game.getTemporaryDestinationCards())))
				{
					//for each card in the temporary list right now extract it is if it equals the ones in the command
					if (!mCardsToReturn.contains(tempCard))
					{
						tempCards.add(tempCard);
					}
				}
				//if it is the clients command add the card
				game.addDestinationCards(tempCards, mUsername);
				game.getBank().setTemporaryCards(null, 0);
				if (game.getClientPlayer().getUsername().equals(mUsername))
				{
					game.updateLocalPlayerPrivateScore(mPrivateScore);
				}
				game.addCommand(this);
			} else
			{
				//TODO: REDO MODEL SO WE DON'T HAVE TO FAKE IT.....
				List<ClientDestinationCard> fakeCards = new ArrayList<>();
				for (int i = 0; i < mNumCardsKept; i++)
				{
					fakeCards.add(new DummyDestinationCard());
				}
				game.addDestinationCards(fakeCards, mUsername);
				game.addCommand(this);
			}
			game.updatePlayerScore(mUsername, mNewScore);
			game.updateDestCardDeckSize(mNewDestinationDeckSize);
//			message = new DestinationCardReturnedObsMsg("Success", game.getPlayer(mUsername));

			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDReturnDestinationCards).put
					(EventKeys.PLAYER_NAME, mUsername));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(mUsername + " returned " + mNumCardsReturned + " destination cards.\n");
		if (mCardsToReturn != null && mCardsToReturn.size() > 0)
		{
			sb.append("Cards returned are:\n");
			for (IDestinationCard card : mCardsToReturn)
			{
				sb.append("Card from " + card.getCities().get(0).getName() + " to " + card.getCities().get(1).getName
						() + "\n");
			}
		}
		return sb.toString();
	}
}
