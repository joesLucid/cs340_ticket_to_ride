package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.bank.DummyTrainCard;

/**
 * Created by joews on 3/4/2017.
 */
public class ClientDrawTrainCardCommand extends DrawTrainCardCommand
{
	public ClientDrawTrainCardCommand(String username, int lastCommand, String gameID, int drawnZone)
	{
		super(username, lastCommand, gameID, drawnZone);
	}

	public ClientDrawTrainCardCommand(DrawTrainCardCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		//check if the lobbyID or username are null
		if (mGameID != null && mUsername != null)
		{
			IClientGameModel game = ClientModelManager.getInstance().getGame();

			if (mTrainCard == null)
			{
				mTrainCard = new DummyTrainCard();
			}
			//if it is the client's command add the card
			game.addTrainCard(new ClientTrainCard(mTrainCard), mUsername);

			ClientTrainCard[] revealed = new ClientTrainCard[mRevealed.size()];
			for (int i = 0; i < mRevealed.size(); i++)
			{
				revealed[i] = mRevealed.get(i) == null ? null : new ClientTrainCard(mRevealed.get(i));
			}
			game.setRevealedCards(revealed);

			game.updateTrainDeckSize(mNewTrainDeckSize);
			game.updateTrainDiscardPileSize(mNewDiscardSize);

			game.addCommand(this);
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDDrawTrainCard)
					.put(EventKeys.CAN_DRAW_ANOTHER, mCanDrawAnother)
					.put(EventKeys.PLAYER_NAME, mUsername));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return null;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(mUsername).append(" drew a ");
		if (mTrainCard != null)
		{
			sb.append(mTrainCard.getTrainType().name().toLowerCase()).append(" ");
		}
		sb.append("train card from ");
		if (mDrawnZone == 0)
		{
			sb.append("the deck.\n");
		} else
		{
			sb.append("zone ").append(mDrawnZone).append(".\n");
		}
		return sb.toString();
	}
}
