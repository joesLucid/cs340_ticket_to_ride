package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.newGameObsMsg.PlayerLeftObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;

/**
 * Created by crown on 3/24/2017.
 */
public class ClientLeaveGameCommand extends LeaveGameCommand {
	public ClientLeaveGameCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public ClientLeaveGameCommand(BaseGameCommand that) {
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException {
		IClientGameModel game = ClientModelManager.getInstance().getGame();
		if(game.getClientPlayer().getUsername().equals(mUsername)){
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDLeaveGame));
		} else
		{
			ClientModelManager.getInstance().notifyObservers(new PlayerLeftObsMsg(mUsername));
		}
		return null;
	}
}
