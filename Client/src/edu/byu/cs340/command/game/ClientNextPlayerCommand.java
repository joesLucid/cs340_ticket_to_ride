package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;

/**
 * Created by joews on 3/4/2017.
 */
public class ClientNextPlayerCommand extends NextPlayerCommand
{
	public ClientNextPlayerCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}

	public ClientNextPlayerCommand(NextPlayerCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage message = null;
		//check if the lobbyID or username are null
		if (mGameID != null && mUsername != null && mNextPlayer != null)
		{
			IClientGameModel game = ClientModelManager.getInstance().getGame();
			game.updateCurrentPlayerTakingTurn(mNextPlayer);
//			message = new NextPlayerObsMsg("Success", mNextPlayer);
			game.addCommand(this);

			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDNextPlayerTurn).put(EventKeys
					.PLAYER_NAME, mNextPlayer));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}

	@Override
	public String toString()
	{
		return mNextPlayer + " turn has started.\n";
	}
}
