package edu.byu.cs340.command.game;

import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;

/**
 * Created by crown on 3/24/2017.
 */
public class ClientLastRoundCommand extends LastRoundCommand {
	public ClientLastRoundCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public ClientLastRoundCommand(BaseGameCommand that) {
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException {
		IClientGameModel game = ClientModelManager.getInstance().getGame();
		game.setIsLastRound(true);
		game.addCommand(this);
		return null;
	}
}
