package edu.byu.cs340.communication;

/**
 * Created by joews on 2/1/2017.
 *
 * Interface for the poller on the client side.
 */
public interface IPoller {
    /**
     * This method starts the poller polling requests from the server.
     *
     * @pre None
     * @post polls server on an interval
     */
    void startPolling();

	/**
	 * Stops the poller from polling from server
	 *
	 * @pre none
	 * @post stops the poller polling
	 */
	void stopPolling();
}
