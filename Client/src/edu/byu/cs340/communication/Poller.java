package edu.byu.cs340.communication;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IPollerModel;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.state.IState;
import edu.byu.cs340.models.state.JoinGameState;
import edu.byu.cs340.models.state.LobbyState;
import edu.byu.cs340.models.state.game.GameState;

import static java.lang.Thread.sleep;

/**
 * Created by Sean on 2/1/2017.
 *
 * Implementation of the poller to get commands from the server.
 *
 * Poller runs on a new thread, asking the server for updates when the client is in a lobby or a game
 */
public class Poller implements IPoller, Runnable
{

	private boolean mPolling;
	private Thread mThread;
	private static final int mTimeConst = 1;
	private static final int mJoinGamePollTime = 5;
	private int mJoinGameElapsedTime = 0;

	private static Poller mSingleton;


	public static Poller getInstance()
	{
		if (mSingleton == null)
		{
			mSingleton = new Poller();
		}
		return mSingleton;
	}

	public Poller()
	{
		mPolling = false;
	}

	/**
	 * Starts polling the server.
	 *
	 * If poller has not been started before, spawns a new thread to poll.
	 *
	 * If poller is already going, no functional change is made.
	 *
	 * @post polling happens every 1 second and the new commands are executed on the client.
	 */
	@Override
	public void startPolling()
	{
		if (mThread == null)
		{
			mThread = new Thread(this);
			if (mThread != null)
			{
				mPolling = true;
				mThread.start();
			}
		}
	}

	/**
	 * Stops polling the server.
	 *
	 * If poller is not running, no functional change is made
	 *
	 * @post poller will not be running
	 */
	@Override
	public void stopPolling()
	{
		mPolling = false;
	}


	/**
	 *
	 */
	@Override
	public void run()
	{
		try
		{
			//noinspection InfiniteLoopStatement
			while (true)
			{
				if (mPolling)
				{
					IPollerModel client = ClientFacade.getInstance(IPollerModel.class);
					if (client != null)
					{
						IState state = ClientModelManager.getInstance().getState();
						if (state instanceof GameState)
						{
							client.updateGame();
							client.pullChat();
						} else if (state instanceof LobbyState)
						{
							client.updateLobby();
							client.pullChat();
						} else if (state instanceof JoinGameState)
						{
							mJoinGameElapsedTime++;
							if (mJoinGameElapsedTime >= mJoinGamePollTime)
							{
								client.updateGamesListInfo();
								mJoinGameElapsedTime = 0;
							}
						}
					}
				}
				// sleep for one second
				sleep(mTimeConst * 1000);
			}
		} catch (InterruptedException e)
		{
			System.err.println("Interrupted the poller, needs to be restarted");
		}
	}
}
