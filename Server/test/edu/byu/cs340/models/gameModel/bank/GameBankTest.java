package edu.byu.cs340.models.gameModel.bank;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Created by joews on 2/27/2017.
 *
 * Tests for the Game Bank object
 */
class GameBankTest
{
	@BeforeEach
	void setUp()
	{

	}

	@AfterEach
	void tearDown()
	{

	}

	@Test
	void shuffleDestinationCards()
	{

	}

	@Test
	void shuffleTrainCards()
	{

	}

	@Test
	void setDestinationCards()
	{

	}

	@Test
	void setTrainCards()
	{

	}

	@Test
	void drawDestinationCardFromDeck()
	{

	}

	@Test
	void drawTrainCardFromRevealed()
	{

	}

	@Test
	void drawTrainCardFromDeck()
	{

	}

	@Test
	void getRevealedCards()
	{

	}

	@Test
	void putDestinationCardsOnBottom()
	{

	}

	@Test
	void putTrainCardsInDiscard()
	{

	}

	@Test
	void updateRevealedTrainCards()
	{

	}

	@Test
	void isIntialized()
	{

	}

}