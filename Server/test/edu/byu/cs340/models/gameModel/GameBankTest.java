package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.*;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by joews on 2/23/2017.
 */
class GameBankTest
{
	GameBank mGameBank;
	List<IDestinationCard> mDestinationCardList = new ArrayList<>();
	List<Integer> mPoints = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 6, 2, 0, 9, 44, 24}));
	List<ITrainCard> mTrainCardList = new ArrayList<>();
	List<TrainType> mTrainTypes = new ArrayList<>(Arrays.asList(new TrainType[]{
			TrainType.Box, TrainType.Caboose, TrainType.Caboose, TrainType.Freight, TrainType.Hopper, TrainType
			.Locomotive, TrainType.Tanker
	}));

	@BeforeEach
	void setUp()
	{
		mGameBank = new GameBank();
		//create deck of destination bank bank.
		setupDestinationCards();
		setupTrainCards();
	}

	private void setupDestinationCards()
	{
		for (Integer mPoint : mPoints)
		{
			mDestinationCardList.add(new DestinationCard(new ICity[2], mPoint));
		}
		mGameBank.setDestinationCards(mDestinationCardList);
	}

	private void setupTrainCards()
	{
		for (TrainType type : mTrainTypes)
		{
			mTrainCardList.add(new TrainCard(type));
		}
		mGameBank.setTrainCards(mTrainCardList);
	}

	@AfterEach
	void tearDown()
	{
		mGameBank = null;
	}

	@Test
	void shuffleDestinationCards()
	{
		//shuffle the destination bank.
		int maxIters = 5; //iterate 5 times to check for randomness.
		boolean isDifferent = false;
		while (maxIters > 0 && !isDifferent)
		{
			//set up the destination card deck
			this.setupDestinationCards();
			//shuffle it.
			mGameBank.shuffleDestinationCards();
			//test if it is difference
			List<IDestinationCard> drawnCards = new ArrayList<>();
			boolean outOfCards = false;
			int currentCard = 0;
			while (!outOfCards)
			{
				IDestinationCard card = mGameBank.drawDestinationCardFromDeck();
				if (card == null)
				{
					outOfCards = true;
				} else
				{
					drawnCards.add(card);
					if (!card.getId().equals(mDestinationCardList.get(currentCard).getId()))
					{
						isDifferent = true;
					}
					currentCard++;
				}
			}
			maxIters--;
		}
		assertTrue(isDifferent, "Destination Shuffle Changed Deck Order:");
	}

	@Test
	void shuffleTrainCards()
	{
//shuffle the train bank.
		int maxIters = 5; //iterate 5 times to check for randomness.
		boolean isDifferent = false;
		while (maxIters > 0 && !isDifferent)
		{
			//set up the destination card deck
			this.setupTrainCards();
			//shuffle it.
			mGameBank.shuffleTrainCards();
			//test if it is difference
			List<ITrainCard> drawnCards = new ArrayList<>();
			boolean outOfCards = false;
			int currentCard = 0;
			while (!outOfCards)
			{
				ITrainCard card = mGameBank.drawTrainCardFromDeck();
				if (card == null)
				{
					outOfCards = true;
				} else
				{
					drawnCards.add(card);
					if (!card.getId().equals(mTrainCardList.get(currentCard).getId()))
					{
						isDifferent = true;
					}
					currentCard++;
				}
			}
			maxIters--;
		}
		assertTrue(isDifferent, "Train card Shuffle Changed Deck Order:");
	}

	@Test
	void setDestinationCards()
	{

	}

	@Test
	void setTrainCards()
	{

	}

	@Test
	void drawDestinationCardFromDeck()
	{

	}

	@Test
	void drawTrainCardFromRevealed()
	{

	}

	@Test
	void drawTrainCardFromDeck()
	{

	}

	@Test
	void getRevealedCards()
	{

	}

	@Test
	void putDestinationCardsOnBottom()
	{

	}

	@Test
	void putTrainCardsInDiscard()
	{

	}

	@Test
	void updateRevealedTrainCards()
	{

	}

}