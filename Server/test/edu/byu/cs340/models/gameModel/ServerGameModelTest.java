package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.login.LoginCommand;
import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;
import edu.byu.cs340.models.gameModel.map.*;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.lobbyModel.ServerLobbyModel;
import edu.byu.cs340.models.roomModel.IPlayer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by joews on 2/27/2017.
 *
 * Tests for the server game models.
 */
class ServerGameModelTest
{
	private ServerGameModel mServerGameModel;
	private List<ITrainCard> mTrainCards;
	private List<IDestinationCard> mDestinationCards;
	private List<ICity> mCities;
	private List<IRoute> mRoutes;

	private List<String> mPlayerNames;
	private List<TrainType> mTrainCardTypes;
	private String mRoomName;
	private List<String> mCityNames;

	private LobbyModel mLobbyModel;

	//Sets up the whole server gam models
	@BeforeEach
	void setUp()
	{
		if (mServerGameModel == null)
		{
			rebuildGameModel();
		}
	}

	/**
	 * rebuild the game models for tests
	 */
	private void rebuildGameModel()
	{
		mServerGameModel = new ServerGameModel();
		//create a lobby to initializez it.
		mPlayerNames = new ArrayList<>();
		mPlayerNames.add("joeStack");
		mPlayerNames.add("otherPlayer");
		mPlayerNames.add("theOtherPlayer");
		mRoomName = "myRoom";

		mLobbyModel = new ServerLobbyModel(mRoomName, mPlayerNames.get(0));
		mLobbyModel.addPlayer(mPlayerNames.get(1));
		mLobbyModel.readyUser(mPlayerNames.get(1));
		mLobbyModel.addPlayer(mPlayerNames.get(2));
		mLobbyModel.readyUser(mPlayerNames.get(2));


		//create train cards
		mTrainCardTypes = new ArrayList<>();
		mTrainCards = new ArrayList<>();
		//ensure the first two cards are reefers
		mTrainCardTypes.add(TrainType.Reefer);
		mTrainCardTypes.add(TrainType.Reefer);
		mTrainCardTypes.add(TrainType.Locomotive);
		mTrainCards.add(new TrainCard(TrainType.Reefer));
		mTrainCards.add(new TrainCard(TrainType.Reefer));
		mTrainCards.add(new TrainCard(TrainType.Locomotive));

		for (int i = 0; i < 2; i++)
		{
			mTrainCardTypes.add(TrainType.Box);
			mTrainCardTypes.add(TrainType.Passenger);
			mTrainCardTypes.add(TrainType.Locomotive);
			mTrainCardTypes.add(TrainType.Freight);
			mTrainCardTypes.add(TrainType.Reefer);
		}
		for (TrainType mTrainCardType : mTrainCardTypes)
		{
			mTrainCards.add(new TrainCard(mTrainCardType));
		}


		//create cities - pre conditions dictace the name can't be null
		mCities = new ArrayList<>();
		mCityNames = new ArrayList<>();

		mCityNames.add("Provo");
		mCityNames.add("Salt Lake");
		mCityNames.add("Orem");
		mCityNames.add("Springville");
		for (String mCityName : mCityNames)
		{
			mCities.add(new City(mCityName));
		}
		//create routes
		mRoutes = new ArrayList<>();
		//set up first route for easy testing
		ICity[] cit = new ICity[]{mCities.get(0), mCities.get(1)};
		TrainType trainType = TrainType.Reefer;
		int num = 2;
		mRoutes.add(new Route(cit, trainType, num, null));

		//use random seed to pick numbers for cities etc
		Random rand = new Random(1);
		for (int i = 0; i < 5; i++)
		{
			//get two random cities
			int index1 = rand.nextInt(mCities.size());
			int index2 = rand.nextInt(mCities.size());
			cit = new ICity[]{mCities.get(index1), mCities.get(index2)};
			//the last 2 items are locomotive then none, we don't want locomotice, so drop off one number
			index1 = rand.nextInt(TrainType.values().length - 1);
			//if we get length-2 then add one
			if (index1 == TrainType.values().length - 2)
			{
				index1++;
			}
			trainType = TrainType.values()[index1];
			//each route has between 1 aand 6 trains;
			num = rand.nextInt(6) + 1;
			mRoutes.add(new Route(cit, trainType, num, null));
		}
		//create destination cards
		//reseed
		rand = new Random(1);

		mDestinationCards = new ArrayList<>();
		for (int i = 0; i < 10; i++)
		{
			int points = rand.nextInt(15) + 5;
			int index1 = rand.nextInt(mCities.size());
			int index2 = rand.nextInt(mCities.size());
			cit = new ICity[]{mCities.get(index1), mCities.get(index2)};
			mDestinationCards.add(new DestinationCard(cit, points));
		}

		//set into game models
		mServerGameModel.initializeFromLobby(mLobbyModel, mTrainCards, mDestinationCards, mCities, mRoutes);
	}


	@Test
	void getPlayerManager()
	{
		if (mServerGameModel.getPlayerManager() != null)
		{
			boolean success = true;
			for (IPlayer player : mServerGameModel.getPlayerManager().getPlayers())
			{
				if (!mPlayerNames.contains(player.getUsername()))
				{
					success = false;
				}
			}
			assertTrue(success);
		} else
		{
			assertTrue(false, "Player Manager null test ");
		}

		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void drawDestinationCards()
	{
		//try drawing destination cards
		List<IDestinationCard> cards = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());
		assertEquals(IServerGameModel.NUM_DESTINATION_CARDS_TO_DRAW, cards.size());
		assertTrue(mDestinationCards.containsAll(cards));
		assertTrue(cards.size() <= 3);
		mServerGameModel.returnDestinationCardsToBottom(cards, mServerGameModel.getCurrentPlayerName());
		//try again
		List<IDestinationCard> cards2 = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());
		assertEquals(IServerGameModel.NUM_DESTINATION_CARDS_TO_DRAW, cards2.size());
		assertTrue(mDestinationCards.containsAll(cards2));
		assertTrue(cards2.size() <= 3);
		//ensure the cards are unique
		boolean containsNone = true;
		for (IDestinationCard card : cards)
		{
			if (cards2.contains(card))
			{
				containsNone = false;
			}
		}
		assertTrue(containsNone);

		//clearDAO game models so it will be reset on next setup
		mServerGameModel = null;
	}

	@Test
	void drawTrainCardFromDeck()
	{
		//Todo:this function enters an infinite loop
		boolean success = true;
		//draw cads and add to set, if hte card is not in the original list or the card is alread yin set, fail
		Set<ITrainCard> trainCards = new HashSet<>();
		//5 revealed cards so null starts at the 5th to last
		for (int i = 0; i < mTrainCards.size() - 5; i++)
		{
			ITrainCard card = mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName());
			if (trainCards.contains(card) || !mTrainCards.contains(card))
			{
				success = false;
			}
			trainCards.add(card);
		}
		assertTrue(success);
		assertNull(mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName()));

		mServerGameModel = null;
	}

	@Test
	void drawTrainCardFromRevealed()
	{
		Random rand = new Random();
		boolean success = true;
		//draw cads randomly from the revealed.
		Set<ITrainCard> trainCards = new HashSet<>();
		//5 revealed cards so null starts at the 5th to last
		while (mServerGameModel.getNumTrainCardsInDeck() > 0)
		{
			ITrainCard card = mServerGameModel.drawTrainCardFromRevealed(rand.nextInt(5), mServerGameModel
					.getCurrentPlayerName());
			if (trainCards.contains(card) || !mTrainCards.contains(card))
			{
				success = false;
			}
			trainCards.add(card);
		}
		//draw a card from each of hte five revealed zones.
		for (int i = 0; i < 5; i++)
		{
			ITrainCard card = mServerGameModel.drawTrainCardFromRevealed(i, mServerGameModel
					.getCurrentPlayerName());
			//could be null if we cleared hte reveavled zone
			if (card != null)
			{
				if (trainCards.contains(card) || !mTrainCards.contains(card))
				{
					success = false;
				}
			}
			trainCards.add(card);
		}
		assertTrue(success);
		int numNullFound = 0;
		//go through all of the revealed zones and draw a card, count how many are null (should be 5)
		for (int i = 0; i < 5; i++)
		{
			if (mServerGameModel.drawTrainCardFromRevealed(i, mServerGameModel.getCurrentPlayerName()) == null)
			{
				numNullFound++;
			}
		}
		assertEquals(5, numNullFound);
		numNullFound = 0;
		//go through all of the revealed zones and draw a card, count how many are null (should be 5)
		for (int i = 0; i < 5; i++)
		{
			if (mServerGameModel.drawTrainCardFromRevealed(i, mServerGameModel.getCurrentPlayerName()) == null)
			{
				numNullFound++;
			}
		}
		assertEquals(5, numNullFound);

		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void getRevealedCards()
	{
		List<ITrainCard> cards = mServerGameModel.getRevealedCards();
		boolean success = true;
		for (ITrainCard card : cards)
		{
			if (card == null)
			{
				success = false;
			}
		}
		assertTrue(success);
		//draw all of the cards from the deck
		ITrainCard res = mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName());
		while (res != null)
		{
			res = mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName());
		}
		//now draw one from each revealed zone, then get the revealed cards and ensure it is null.
		for (int i = 0; i < 5; i++)
		{
			mServerGameModel.drawTrainCardFromRevealed(i, mServerGameModel.getCurrentPlayerName());
			assertNull(mServerGameModel.getRevealedCards().get(i));
		}
		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void returnDestinationCardsToBottom()
	{
		int i = 2;
		//for hte current player draw destination cards
		List<IDestinationCard> res = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());

		//now draw again
		List<IDestinationCard> res2 = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());

		//check that they match because they should since we haven't returne any
		assertTrue(res.containsAll(res2));

		//now return some of hte destination cards

		boolean result = mServerGameModel.returnDestinationCardsToBottom(res2.subList(0, i), mServerGameModel
				.getCurrentPlayerName());
		assertTrue(result);

		//now get the players destination cards, they should contain res
		List<IDestinationCard> playerCards = mServerGameModel.getCurrentPlayersDestinationCards();

		assertEquals(3 - i, playerCards.size());
		assertTrue(res.containsAll(playerCards));
		assertEquals(0, mServerGameModel.getCurrentPlayersTemporaryDestinationCards().size());

		//try to draw again
		res2 = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());

		boolean success = true;
		for (IDestinationCard card : res2)
		{
			if (res.contains(card))
			{
				success = false;
			}
		}
		assertTrue(success);

		//now,

		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void initializeFromLobby()
	{
		//game models is already initialized

		//check to make sure the player names are teh same as the lobby
		for (IPlayer player : mServerGameModel.getPlayers())
		{
			assertTrue(mPlayerNames.contains(player.getUsername()));
		}
		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void isPlayersTurn()
	{
		String playerTurn = mServerGameModel.getCurrentPlayerName();
		for (String player : mPlayerNames)
		{
			assertFalse(player.equals(playerTurn) ^ mServerGameModel.isPlayersTurn(player));
		}
		//clearDAO the models
		mServerGameModel = null;
	}

	@Test
	void nextPlayer()
	{
		//get first user
		String first = mServerGameModel.getCurrentPlayerName();
		//go to next user
		assertTrue(mServerGameModel.nextPlayer());
		mServerGameModel.nextPlayer();
		String second = mServerGameModel.getCurrentPlayerName();
		assertNotEquals(first, second);
		//draw cards, now you can't switch till you return them
		List<IDestinationCard> drawn = mServerGameModel.drawDestinationCards(mServerGameModel.getCurrentPlayerName());
		assertFalse(mServerGameModel.nextPlayer());
		assertFalse(mServerGameModel.returnDestinationCardsToBottom(drawn, first));
		assertTrue(mServerGameModel.returnDestinationCardsToBottom(drawn, mServerGameModel.getCurrentPlayerName()));
		assertTrue(mServerGameModel.nextPlayer());
		assertTrue(mServerGameModel.nextPlayer());
		assertEquals(mServerGameModel.getCurrentPlayerName(), first);
		//TODO: Implement. Try changing player without doing anything, then draw destination cards without returning
		// then draw and return then next

		mServerGameModel = null;
	}



	@Test
	void isInitialized()
	{
		assertTrue(mServerGameModel.isInitialized());
		ServerGameModel game2 = new ServerGameModel();
		assertFalse(game2.isInitialized());
		game2.initializeFromLobby(mLobbyModel, mTrainCards, mDestinationCards, mCities, mRoutes);
		assertTrue(game2.isInitialized());

		mServerGameModel = null;
	}

	@Test
	void sendChatMessage()
	{
		//TODO: hasn't been implemented yet implement
	}

	@Test
	void getChatMessages()
	{
		//TODO: hasn't been implemented yet implement
	}

	@Test
	void claimRoute()
	{

		//test if route is in models and with no locomitves
		List<ITrainCard> cardsOfInterest = helperSetUpForClaimRoute(0, false);
		String currentPlayerName = mServerGameModel.getCurrentPlayerName();
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		//test number of trains remaining
		assertEquals(45, mServerGameModel.getTrainsRemaining(currentPlayerName));
		boolean worked = mServerGameModel.claimRoute(mRoutes.get(0).getId(), cardsOfInterest);
		assertTrue(worked);
		//test if player has route claimed.
		assertEquals(1, mServerGameModel.getPlayer(currentPlayerName).getRoutes().size());
		assertFalse(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		assertEquals(45 - mRoutes.get(0).getNumTrains(), mServerGameModel.getTrainsRemaining(currentPlayerName));

		//test with locomotives
		cardsOfInterest = helperSetUpForClaimRoute(0, true);
		currentPlayerName = mServerGameModel.getCurrentPlayerName();
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		worked = mServerGameModel.claimRoute(mRoutes.get(0).getId(), cardsOfInterest);
		assertTrue(worked);
		assertEquals(1, mServerGameModel.getPlayer(currentPlayerName).getRoutes().size());
		assertFalse(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));

		//test if route isn't in models
		cardsOfInterest = helperSetUpForClaimRoute(0, false);
		currentPlayerName = mServerGameModel.getCurrentPlayerName();
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		worked = mServerGameModel.claimRoute("BAD ID", cardsOfInterest);
		assertFalse(worked);
		assertEquals(0, mServerGameModel.getPlayer(currentPlayerName).getRoutes().size());
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));

		//test if train cards are empty
		cardsOfInterest = helperSetUpForClaimRoute(0, true);
		cardsOfInterest.clear();
		worked = mServerGameModel.claimRoute(mRoutes.get(0).getId(), cardsOfInterest);
		assertFalse(worked);

		//test if train cards are too few
		cardsOfInterest = helperSetUpForClaimRoute(-1, false);
		currentPlayerName = mServerGameModel.getCurrentPlayerName();
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		worked = mServerGameModel.claimRoute(mRoutes.get(0).getId(), cardsOfInterest);
		assertFalse(worked);
		//test if player has route claimed.
		assertEquals(0, mServerGameModel.getPlayer(currentPlayerName).getRoutes().size());
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));

		//test if train cards are too many
		cardsOfInterest = helperSetUpForClaimRoute(1, false);
		currentPlayerName = mServerGameModel.getCurrentPlayerName();
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));
		worked = mServerGameModel.claimRoute(mRoutes.get(0).getId(), cardsOfInterest);
		assertFalse(worked);
		//test if player has route claimed.
		assertEquals(0, mServerGameModel.getPlayer(currentPlayerName).getRoutes().size());
		assertTrue(mServerGameModel.getPlayer(currentPlayerName).getTrainCards().containsAll(cardsOfInterest));


		//clearDAO to let the game reset
		mServerGameModel = null;
	}

	private List<ITrainCard> helperSetUpForClaimRoute(int additionalCards, boolean getLocomotive)
	{
		rebuildGameModel();
		IRoute routeToClaim = mRoutes.get(0); //requires 2 reefers
		TrainType trainTypeNeeded = routeToClaim.getType();

		//draw all train cards to ensure player has 2 reefers
		ITrainCard drawn = mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName());
		while (drawn != null)
		{
			drawn = mServerGameModel.drawTrainCardFromDeck(mServerGameModel.getCurrentPlayerName());
		}
		for (int i = 0; i < 5; i++)
		{
			mServerGameModel.drawTrainCardFromRevealed(i, mServerGameModel.getCurrentPlayerName());
		}
		//now the player should have all the train cards including but not limited to 2 reffer cards.
		//find those two and return them
		String playerName = mServerGameModel.getCurrentPlayerName();
		List<ITrainCard> cardsInPlayerHand = mServerGameModel.getPlayer(playerName).getTrainCards();
		List<ITrainCard> cardsOfInterest = new ArrayList<>();

		if (getLocomotive)
		{
			additionalCards--;
		}

		for (ITrainCard card : cardsInPlayerHand)
		{
			//if its the right type and the number of cards of interest is less then the neds of hte route plus
			// additional cards, then add it
			if (card.getTrainType() == trainTypeNeeded && cardsOfInterest.size() < routeToClaim.getNumTrains() +
					additionalCards)
			{
				cardsOfInterest.add(card);
			}
			if (card.getTrainType() == TrainType.Locomotive && getLocomotive)
			{
				cardsOfInterest.add(card);
				getLocomotive = false;
				additionalCards++;
			}
		}
		return cardsOfInterest;
	}

	@Test
	void getRoutes()
	{
		assertTrue(mServerGameModel.getRoutes().containsAll(mRoutes));
	}

	@Test
	void getCities()
	{
		assertTrue(mServerGameModel.getCities().containsAll(mCities));
	}

	@Test
	void getLongestRoutePlayer()
	{
		//TODO: implement

	}

	@Test
	void getCurrentPlayersDestinationCards()
	{
		//TODO: implement
	}

	@Test
	void getCurrentPlayersTrainCards()
	{
		//TODO: implement

	}

	@Test
	void getCurrentPlayersTemporaryDestinationCards()
	{
		//TODO: Implement
	}

	@Test
	void getScore()
	{
		//TODO: implement

	}

	@Test
	void addPlayer()
	{
		//test if adding the player worked
		mServerGameModel.addPlayer("newlyAdded");
		boolean success = false;
		for (IPlayer player : mServerGameModel.getPlayers())
		{
			if (player.getUsername().equals("newlyAdded"))
			{
				success = true;
			}
		}
		assertTrue(success);


		mServerGameModel = null;
	}

	/**
	 * Test getHost() and setHost()
	 */
	@Test
	void setHost()
	{
		String host = mServerGameModel.getHost();
		assertEquals(mPlayerNames.get(0), host);
		mServerGameModel.setHost(mPlayerNames.get(1));
		host = mServerGameModel.getHost();
		assertEquals(mPlayerNames.get(1), host);


		//clearDAO the models
		mServerGameModel = null;
	}

	/**
	 * Test removePlayer() and isEmpty()
	 */
	@Test
	void testRemovePlayerEmpty()
	{
		assertFalse(mServerGameModel.isEmpty());
		//remove the players
		for (IPlayer player : mServerGameModel.getPlayers())
		{
			mServerGameModel.removePlayer(player.getUsername());
		}
		assertTrue(mServerGameModel.isEmpty());

		//clearDAO server models
		mServerGameModel = null;
	}

	/**
	 * Test getRoomName() and setRoomName()
	 */
	@Test
	void testRoomName()
	{
		String roomName = mServerGameModel.getRoomName();
		assertEquals(mRoomName, roomName);
		mServerGameModel.setRoomName("newName");
		assertEquals("newName", mServerGameModel.getRoomName());


		//clearDAO the models
		mServerGameModel = null;
	}

	/**
	 * Test getRoomID() and setRoomID()
	 */
	@Test
	void testRoomID()
	{
		String roomId = mServerGameModel.getRoomID();
		assertTrue(roomId != null);
		String again = mServerGameModel.getRoomID();
		assertEquals(roomId, again);
		mServerGameModel.setRoomID("FAKE_ID");
		roomId = mServerGameModel.getRoomID();
		assertEquals("FAKE_ID", roomId);


		//clearDAO the models
		mServerGameModel = null;
	}

	/**
	 * Test getPlayers(), getPlayer(), hasPlayer(), getNumPlayers()
	 */
	@Test
	void testPlayers()
	{
		//test getting all players
		for (IPlayer player : mServerGameModel.getPlayers())
		{
			assertTrue(mPlayerNames.contains(player.getUsername()));
		}

		//test getting a single player
		for (String playerName : mPlayerNames)
		{
			assertEquals(playerName, mServerGameModel.getPlayer(playerName).getUsername());
		}

		//test has player
		for (String playerName : mPlayerNames)
		{
			assertTrue(mServerGameModel.hasPlayer(playerName));
		}

		//test getNumPlayers
		assertEquals(3, mServerGameModel.getNumPlayers());
		mServerGameModel.removePlayer(mServerGameModel.getCurrentPlayerName());
		assertEquals(2, mServerGameModel.getNumPlayers());


		//clearDAO the models
		mServerGameModel = null;
	}


	/**
	 * Test addCommand, getLastCommand, and getRecentCommands
	 */
	@Test
	void testCommands()
	{

		//get the commands of the current player (should be empty)
		assertTrue(mServerGameModel.getRecentCommands(-1, mServerGameModel.getCurrentPlayerName()).isEmpty());
		assertEquals(-1, mServerGameModel.getLastCommand());
		//add a fake command
		mServerGameModel.addCommand(new LoginCommand("fake", "fake", "fake"));
		assertEquals(0, mServerGameModel.getLastCommand());


		//add a fake command assigned to the user.
		IBaseCommand cmd = new LoginCommand(mServerGameModel.getCurrentPlayerName(), "FAKE_PASSWORD", null);
		mServerGameModel.addCommand(cmd);
		List<IBaseCommand> cmds = mServerGameModel.getRecentCommands(-1, mServerGameModel.getCurrentPlayerName());
		assertEquals(cmd.getUsername(), cmds.get(1).getUsername());
		assertEquals(((LoginCommand) cmd).getPassword(), ((LoginCommand) cmds.get(1)).getPassword());

		//clearDAO server models
		mServerGameModel = null;
	}


}