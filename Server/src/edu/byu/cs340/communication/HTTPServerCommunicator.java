package edu.byu.cs340.communication;

import com.sun.net.httpserver.HttpServer;
import edu.byu.cs340.handler.ChatHandler;
import edu.byu.cs340.handler.CommandHandler;
import edu.byu.cs340.handler.LoginCommandHandler;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by Rebekah on 1/30/2017.
 *
 * Communicates with clients over HTTP.
 */
public class HTTPServerCommunicator implements IServerCommunicator {

    private static final int MAX_WAITING_CONNECTIONS = 7;

    private HttpServer server;

    @Override
    public void run(String portNumber) {
        System.out.println("Initializing HTTP Server");
        if(portNumber == null)
        {
            System.out.println("port not initialized. setting port to 25563");
            portNumber = "25563";
        }
        try {
            server = HttpServer.create(
                    new InetSocketAddress(Integer.parseInt(portNumber)),
                    MAX_WAITING_CONNECTIONS);
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }

        server.setExecutor(null); // use the default executor

        System.out.println("Creating contexts");
        server.createContext("/command/login", new LoginCommandHandler());
	    server.createContext("/command", new CommandHandler());
	    server.createContext("/chat", new ChatHandler());
	    System.out.println("Starting server");
        server.start();
    }
}
