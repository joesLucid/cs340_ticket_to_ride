package edu.byu.cs340.communication;

import edu.byu.cs340.models.IAuthenticator;
import edu.byu.cs340.models.RoomManager;

/**
 * Created by joews on 2/4/2017.
 *
 * Interface extending funcitonality of server to work with commnads.
 */
public interface ICommandServerFacade extends IServerFacade
{
	/**
	 * Gets the manager of the rooms.
	 *
	 * @return Returns the RoomManager
	 */
	RoomManager getRoomManager();

	/**
	 * Gets the authenticator
	 * @return Returns the Authenticator.
	 */
	IAuthenticator getAuthenticator();
}
