package edu.byu.cs340.database;

import edu.byu.cs340.*;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.models.ServerUser;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.lobbyModel.ServerLobbyModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 4/11/2017.
 */
public class DatabaseWriter
{
	private IAbstractFactory mAbstractFactory;
	private IGameDAO mGameDAO;
	private IUserDAO mUserDAO;
	private ILobbyDAO mLobbyDAO;
	private static DatabaseWriter mSINGLETON;
	private boolean mIsSetup = false;
	private Map<String, Integer> mRoomIDtoTurnsSinceSave;
	private static int COMMANDS_BETWEEN_SAVES = 7;

	public static DatabaseWriter getInstance()
	{
		if (mSINGLETON == null)
		{
			mSINGLETON = new DatabaseWriter();
		}
		return mSINGLETON;
	}

	private DatabaseWriter()
	{
		mRoomIDtoTurnsSinceSave = new HashMap<>();
	}

	public void setAbstractFactory(IAbstractFactory abstractFactory)
	{
		mAbstractFactory = abstractFactory;
		if (mAbstractFactory != null)
		{
			mGameDAO = mAbstractFactory.createGameDAO();
			mUserDAO = mAbstractFactory.createUserDAO();
			mLobbyDAO = mAbstractFactory.createLobbyDAO();
		}
		if (mAbstractFactory != null &&
				mGameDAO != null &&
				mUserDAO != null &&
				mLobbyDAO != null)
		{
			mIsSetup = true;
		}
	}

	public boolean isSetup()
	{
		return mIsSetup;
	}

	public void setGame(IServerGameModel game)
	{
		if (mGameDAO != null)
		{
			String gameID = game.getRoomID();
			String serializedGame = ServerSerDes.serializeObject(game, ServerSerDes.Source.SERVER);
			mGameDAO.setGame(gameID, serializedGame);
			mRoomIDtoTurnsSinceSave.put(gameID, 0);
		}
	}

	public void setLobby(ServerLobbyModel lobby)
	{
		if (mLobbyDAO != null)
		{
			String lobbyID = lobby.getRoomID();
			String serializedLobby = ServerSerDes.serializeObject(lobby, ServerSerDes.Source.SERVER);
			mLobbyDAO.setLobby(lobbyID, serializedLobby);
			mRoomIDtoTurnsSinceSave.put(lobbyID, 0);
		}
	}

	public void setUser(ServerUser user)
	{
		if (mUserDAO != null)
		{
			String username = user.getUserName();
			int passhash = user.getPassHash();
			String token = user.getAuthenticationToken();
			mUserDAO.setUser(new UserDTO(username, passhash, token));
		}
	}

	public void addCommandToGame(String gameID, IBaseCommand cmd)
	{
		if (mGameDAO != null)
		{
			String serializedCmd = ServerSerDes.serializeObject(cmd, ServerSerDes.Source.SERVER);
			mGameDAO.addCommandToGame(gameID, serializedCmd);
		}
	}

	public List<ServerGameModel> getGames()
	{
		List<ServerGameModel> games = new ArrayList<>();
		if (mGameDAO != null)
		{
			List<String> serializedGames = mGameDAO.getGames();
			if (serializedGames != null)
			{
				//deserialize
				for (String serializedGame : serializedGames)
				{
					ServerGameModel game = null; //deserialize
					try
					{
						game = (ServerGameModel) ServerSerDes.deserializeObject(serializedGame, ServerSerDes.Source
								.SERVER);
						if (game != null)
						{
							games.add(game);
						}
					} catch (ClassNotFoundException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		return games;
	}

	public ServerGameModel getGame(String gameID)
	{
		if (mGameDAO != null)
		{
			String serializedGame = mGameDAO.getGame(gameID);
			//deserialize
			try
			{
				return (ServerGameModel) ServerSerDes.deserializeObject(serializedGame, ServerSerDes.Source.SERVER);
			} catch (ClassNotFoundException e)
			{
			}
		}
		return null;
	}

	public List<IBaseCommand> getGameCommands(String gameID)
	{
		List<IBaseCommand> commands = new ArrayList<>();
		if (mGameDAO != null)
		{
			List<String> serializedCmds = mGameDAO.getGameCommands(gameID);
			if (serializedCmds != null)
			{
				//deserialize
				for (String serializedCmd : serializedCmds)
				{
					IBaseCommand cmd = null; //deserialize
					try
					{
						cmd = (IBaseCommand) ServerSerDes.deserializeObject(serializedCmd, ServerSerDes.Source.SERVER);
						if (cmd != null)
						{
							commands.add(cmd);
						}
					} catch (ClassNotFoundException e)
					{
					}
				}
			}
		}
		return commands;
	}

	public void addCommandToLobby(String gameID, IBaseCommand cmd)
	{
		if (mLobbyDAO != null)
		{
			String serializedCmd = ServerSerDes.serializeObject(cmd, ServerSerDes.Source.SERVER);
			mLobbyDAO.addCommandToLobby(gameID, serializedCmd);
		}
	}

	public List<ServerLobbyModel> getLobbies()
	{
		List<ServerLobbyModel> lobbies = new ArrayList<>();
		if (mLobbyDAO != null)
		{
			List<String> serializedLobbies = mLobbyDAO.getLobbies();
			if (serializedLobbies != null)
			{
				//deserialize
				for (String serializedLobby : serializedLobbies)
				{
					ServerLobbyModel lobby = null; //deserialize
					try
					{
						lobby = (ServerLobbyModel) ServerSerDes.deserializeObject(serializedLobby, ServerSerDes.Source
								.SERVER);
						if (lobby != null)
						{
							lobbies.add(lobby);
						}
					} catch (ClassNotFoundException e)
					{
					}
				}
			}
		}
		return lobbies;
	}

	public ServerLobbyModel getLobby(String lobbyID)
	{
		if (mLobbyDAO != null)

		{
			String serializedLobby = mLobbyDAO.getLobby(lobbyID);
			//deserialize
			try
			{
				return (ServerLobbyModel) ServerSerDes.deserializeObject(serializedLobby, ServerSerDes.Source.SERVER);
			} catch (ClassNotFoundException e)
			{
			}
		}
		return null;
	}

	public List<IBaseCommand> getLobbyCommands(String lobbyID)
	{
		List<IBaseCommand> commands = new ArrayList<>();
		if (mLobbyDAO != null)
		{
			List<String> serializedCmds = mLobbyDAO.getLobbyCommands(lobbyID);
			if (serializedCmds != null)
			{
				//deserialize
				for (String serializedCmd : serializedCmds)
				{
					IBaseCommand cmd = null; //deserialize
					try
					{
						cmd = (IBaseCommand) ServerSerDes.deserializeObject(serializedCmd, ServerSerDes.Source.SERVER);
						if (cmd != null)
						{
							commands.add(cmd);
						}
					} catch (ClassNotFoundException e)
					{
					}
				}
			}
		}
		return commands;
	}

	public List<ServerUser> getUsers()
	{
		List<ServerUser> users = new ArrayList<>();
		if (mUserDAO != null)
		{
			List<UserDTO> serializedUsers = mUserDAO.getUsers();
			if (serializedUsers != null)
			{
				//deserialize
				for (UserDTO dto : serializedUsers)
				{
					ServerUser user = new ServerUser(dto.getUsername(), dto.getPasshash(), dto.getAuthenticationToken
							()); //deserialize
					if (user != null)
					{
						users.add(user);
					}
				}
			}
		}
		return users;
	}


	public Map<String, String> getUsernameToRoomIDMap()
	{
		if (mUserDAO != null)
		{
			Map<String, String> usernameToRoomID = mUserDAO.getUsernameToRoomIDMap();
			return usernameToRoomID;
		} else
		{
			return new HashMap<>();
		}
	}

	public void setUsernameToRoomIDMap(Map<String, String> map)
	{
		if (mUserDAO != null)
		{
			mUserDAO.setUsernameToRoomIDMap(map);
		}
	}

	public boolean shouldSave(String roomID)
	{
		if (mRoomIDtoTurnsSinceSave.containsKey(roomID))
		{
			int value = mRoomIDtoTurnsSinceSave.get(roomID);
			mRoomIDtoTurnsSinceSave.put(roomID, value + 1);
			if (value + 1 >= COMMANDS_BETWEEN_SAVES)
			{
				return true;
			}
		}
		return false;
	}

	public void removeGame(String roomID)
	{
		if (mGameDAO != null)
		{
			mGameDAO.removeGame(roomID);
		}
	}

	public void removeLobby(String roomID)
	{
		if (mLobbyDAO != null)
		{
			mLobbyDAO.removeLobby(roomID);
		}
	}

	public void clearDatabase()
	{
		if (mGameDAO != null)
		{
			mGameDAO.clearDAO();
		}
		if (mLobbyDAO != null)
		{
			mLobbyDAO.clearDAO();
		}
		if (mUserDAO != null)
		{
			mUserDAO.clearDAO();
		}
	}

	public static void setCmdsBetweenSaves(int num)
	{
		COMMANDS_BETWEEN_SAVES = num;
	}
}

