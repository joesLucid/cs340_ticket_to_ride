package edu.byu.cs340;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.lobby.ServerCreateLobbyCommand;
import edu.byu.cs340.command.lobby.ServerJoinLobbyCommand;
import edu.byu.cs340.command.lobby.ServerReadyUpCommand;
import edu.byu.cs340.command.lobby.ServerStartGameCommand;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;
import edu.byu.cs340.models.gameModel.map.*;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;
import edu.byu.cs340.models.lobbyModel.ServerLobbyModel;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by joews on 4/14/2017.
 */
public class MockTest
{

	public static void main(String[] args)
	{
		Random rand = new Random();
		//create train cards
		List<ITrainCard> trainCardList = new ArrayList<>();
		for (int i = 0; i < 1; i++)
		{
			for (int j = 0; j < TrainType.values().length - 1; j++)
			{
				trainCardList.add(new TrainCard(TrainType.values()[j]));
			}
		}
		//create cities
		List<ICity> cities = new ArrayList<>();
		for (int i = 0; i < 2; i++)
		{
			cities.add(new City("" + i));
		}
		//create routes
		List<IRoute> routes = new ArrayList<>();
		for (int i = 0; i < 1; i++)
		{
			//get two random cities
			int index1 = rand.nextInt(cities.size());
			int index2 = rand.nextInt(cities.size());
			ICity[] cit = new ICity[]{cities.get(index1), cities.get(index2)};
			//the last 2 items are locomotive then none, we don't want locomotice, so drop off one number
			index1 = rand.nextInt(TrainType.values().length - 1);
			//if we get length-2 then add one
			if (index1 == TrainType.values().length - 2)
			{
				index1++;
			}
			TrainType trainType = TrainType.values()[index1];
			//each route has between 1 aand 6 trains;
			int num = rand.nextInt(6) + 1;
			routes.add(new Route(cit, trainType, num, null));
		}
		//create destination cards
		List<IDestinationCard> destinationCards = new ArrayList<>();
		for (int i = 0; i < 1; i++)
		{
			int points = rand.nextInt(15) + 5;
			int index1 = rand.nextInt(cities.size());
			int index2 = rand.nextInt(cities.size());
			ICity[] cit = new ICity[]{cities.get(index1), cities.get(index2)};
			destinationCards.add(new DestinationCard(cit, points));
		}

		try
		{
			String path = "C:\\Users\\joews\\OneDrive\\2017 Winter\\CS " +
					"340\\cs340\\out\\artifacts\\FileDatabasePlugin_jar2\\FileDatabasePlugin.jar";
			DatabaseLoader.getInstance().createFactory(path);
			DatabaseWriter.getInstance().setAbstractFactory(DatabaseLoader.getInstance().getFactory());
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}


		//LOBBY TESTING--------------------------------------
		String token1 = ServerFacade.getInstance(ServerFacade.class).getAuthenticator().register("joeStack", "bbbbbb");
		String token2 = ServerFacade.getInstance(ServerFacade.class).getAuthenticator().register("otherPlayer",
				"bbbbbb");

		//crate a lobby with players in it
		IBaseCommand cmd = new ServerCreateLobbyCommand("joeStack", "myID", "myRoom");
		try
		{
			cmd.execute();
		} catch (IBaseCommand.CommandException e)
		{
			System.out.println(e.getMessage());
		}
		String lobbyID = ServerFacade.getInstance(ServerFacade.class).getRoomManager().getLobbyList().getData().get(0)
				.getRoomID();
		cmd = new ServerJoinLobbyCommand("otherPlayer", lobbyID, new LobbyPlayer
				("otherPlayer"));
		try
		{
			cmd.execute();
		} catch (IBaseCommand.CommandException e)
		{
			System.out.println(e.getMessage());
		}
		//create a read up command
		cmd = new ServerReadyUpCommand("otherPlayer", -1, lobbyID, true);
		try
		{
			cmd.execute();
		} catch (IBaseCommand.CommandException e)
		{
			System.out.println(e.getMessage());
		}

		ServerFacade.getInstance(ServerFacade.class).loadFromDatabase();
		ServerLobbyModel slm = ServerFacade.getInstance(ServerFacade.class).getRoomManager().getLobby(lobbyID);

		//END LOBBY TESTING--------------------------------------

		//GAME TESTING--------------------------------------
		cmd = new ServerStartGameCommand("joeStack", -1, lobbyID);
		try
		{
			cmd.execute();
		} catch (IBaseCommand.CommandException e)
		{
			System.out.println(e.getMessage());
		}
		IServerGameModel game1 = ServerFacade.getInstance(ServerFacade.class).getRoomManager().getGame(lobbyID);
//
//		ServerFacade.

		//initialize the game with lobby
//		ServerGameModel game = new ServerGameModel();
//		game.initializeFromLobby(lobby,trainCardList,destinationCards,cities,routes);

//		DatabaseWriter.getInstance().setGame(game);
//
//		//create server
		ServerFacade.getInstance(ServerFacade.class).loadFromDatabase();
//
		IServerGameModel gm = ServerFacade.getInstance(ServerFacade.class).getRoomManager().getGame(game1.getRoomID());
//		System.out.println("Game Name:" + gm.getRoomName());
//		System.out.println("Player 1:" + gm.getPlayerOrder().get(0));
//		System.out.println("Player 2:" + gm.getPlayerOrder().get(1));
//		System.out.println("Player 1 train cards:" + gm.getCurrentPlayersTrainCards().size());
//		System.out.println("Game Train Deck Size:" + gm.getNumTrainCardsInDeck());
		//END GAME TESTING--------------------------------------
	}
}
