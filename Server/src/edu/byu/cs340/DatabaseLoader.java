package edu.byu.cs340;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by tjacobhi on 12-Apr-17.
 */
public class DatabaseLoader
{
	private IAbstractFactory mAbstractFactory;

	private static DatabaseLoader mInstance;

	private DatabaseLoader()
	{
	}

	public IAbstractFactory getFactory()
	{
		return mAbstractFactory;
	}


	public void createFactory(String path) throws MalformedURLException
	{
		try
		{

			JarFile jarFile = new JarFile(path);
			Enumeration<JarEntry> e = jarFile.entries();

			URL[] urls = {new URL("jar:file:" + path + "!/")};
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			while (e.hasMoreElements())
			{
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class"))
				{
					continue;
				}
				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class c = cl.loadClass(className);
				if (IAbstractFactory.class.isAssignableFrom(c))
				{
					mAbstractFactory = (IAbstractFactory) c.newInstance();
					break;
				}
			}
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (InstantiationException e)
		{
			e.printStackTrace();
		}
//		URLClassLoader urlClassLoader = null;
//		try
//		{
//			urlClassLoader = addPath(path);
//			printClassPath(urlClassLoader);
//
//		} catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
////		ServiceLoader<IAbstractFactory> loader = ServiceLoader.load(IAbstractFactory.class)
////		for (IAbstractFactory p : loader) {
////			// do something with the plugin
////		}
////		JarLoader jarLoader = new JarLoader(urls);
////		jarLoader.addFile("C:\\Users\\tjacobhi\\IdeaProjects\\GitCS340\\out\\artifacts\\MockPlugin_jar
// \\MockPlugin" +
////				".jar");
//		try
//
//		{
//			urlClassLoader.loadClass("edu.byu.cs340.MockAbstractFactory");
//		} catch (
//				ClassNotFoundException e)
//
//		{
//			e.printStackTrace();
//		}
//
//		ServiceLoader<IAbstractFactory> loader = ServiceLoader.load(IAbstractFactory.class);
//		for (IAbstractFactory factory : loader)
//		{
//			mAbstractFactory = factory;
//		}

	}

	public static DatabaseLoader getInstance()
	{
		if (mInstance == null)
			mInstance = new DatabaseLoader();

		return mInstance;
	}
}
