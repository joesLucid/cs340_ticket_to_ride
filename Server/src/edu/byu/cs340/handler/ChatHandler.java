package edu.byu.cs340.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import edu.byu.cs340.SerDes;
import edu.byu.cs340.Utils;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.IAuthenticatableServer;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ChatPostMessage;
import edu.byu.cs340.message.ChatPullMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.BaseUser;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.RoomManager;
import sun.net.www.protocol.http.HttpURLConnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static edu.byu.cs340.communication.CommunicatorHelper.readString;
import static edu.byu.cs340.communication.CommunicatorHelper.writeString;

/**
 * Created by Rebekah on 2/13/2017.
 *
 * Handler for handling chat.
 */
public class ChatHandler implements HttpHandler
{

	public static final String ERROR_IN_CHAT_HANDLER = "Error in chat handler";
	public static final String CHAT_MESSAGE_COULD_NOT_BE_DESERIALIZED = "Chat message could not be deserialized";

	@Override
	public void handle(HttpExchange httpExchange) throws IOException
	{
		IMessage result;
		boolean success = false;
		try
		{
			IAuthenticatableServer serverFacade = ServerFacade.getInstance(IAuthenticatableServer.class);
			Headers reqHeaders = httpExchange.getRequestHeaders();
			//get body from http
			InputStream reqBody = httpExchange.getRequestBody();
			String requestBody = readString(reqBody);
			reqBody.close();
			String authToken = reqHeaders.getFirst(Utils.AUTHORIZATION_HEADER);
			//authenticate the user
			BaseUser user = serverFacade.authenticate(authToken);
			if (user == null)
			{
				result = new ErrorMessage(Utils.AUTHORIZATION_OF_REQUEST_FAILED, ErrorMessage.MILD_ERROR);
				success = false;
			} else
			{
				result = processMessage(requestBody);
				success = true;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			//only gets hit if exception on server. In this case we don't know the cause so send generic message
			result = new ErrorMessage(ERROR_IN_CHAT_HANDLER + ".\n" + e.getMessage(), ErrorMessage.MILD_ERROR);
		}

		//take the message and serialize it
		String serializedResult = SerDes.serializeMessage(result, SerDes.Source.SERVER);

		//add serialized message in response and send back over network.
		if (!success)
		{
			//if there is an error this will be the header
			httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, serializedResult.length());
		} else
		{
			httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, serializedResult.length());
		}
		OutputStream outBody;
		outBody = httpExchange.getResponseBody();
		writeString(serializedResult, outBody);


	}

	public IMessage processMessage(String reqMessage) throws ClassNotFoundException
	{
		//make the command from the body
		Object message = SerDes.deserializeChat(reqMessage, SerDes.Source.SERVER);
		try
		{
			if (message instanceof ChatPostMessage)
			{
				return processPostMessage((ChatPostMessage) message);
			} else if (message instanceof ChatPullMessage)
			{
				return processPullMessage((ChatPullMessage) message);
			} else
			{
				return new ErrorMessage(CHAT_MESSAGE_COULD_NOT_BE_DESERIALIZED, ErrorMessage.MILD_ERROR);
			}
		} catch (IBaseCommand.CommandException e)
		{
			return new ErrorMessage(e.getMessage(), e.getErrorCode());
		}


//
//
//	    if (roomID==null){
//		    result = new ErrorMessage("Room on chat is null",ErrorMessage.MILD_ERROR);
//	    }else if (chatMessage==null){
//
//	    }
//
//	    //get the room
//	    messenger.addChatMessage(message);
//	    result = new ChatPostMessage();
//	    success = true;
	}

	private IMessage processPostMessage(ChatPostMessage message) throws IBaseCommand.CommandException
	{
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();
		String roomID = message.getRoomId();
		ChatMessage chatMessage = message.getChatMessage();
		String user = chatMessage.getUserName();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(user);
		ServerCommandUtils.validatePlayerInRoom(user, roomID);

//		OPERATIONS:
		manager.getRoom(roomID).addChatMessage(chatMessage);

		return null;
	}

	private IMessage processPullMessage(ChatPullMessage message) throws IBaseCommand.CommandException
	{

		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();
		String roomID = message.getRoomId();
		String user = message.getUsername();
		long lastMessage = message.getLastMessage();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(user);
		ServerCommandUtils.validatePlayerInRoom(user, roomID);

		//OPERATIONS:
		List<ChatMessage> messages = manager.getRoom(roomID).getChatMessages(lastMessage);
		message.setChatMessages(messages);

		return message;
	}
}
