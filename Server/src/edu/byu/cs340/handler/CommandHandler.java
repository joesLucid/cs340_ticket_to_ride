package edu.byu.cs340.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import edu.byu.cs340.SerDes;
import edu.byu.cs340.Utils;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.communication.IAuthenticatableServer;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.BaseUser;
import sun.net.www.protocol.http.HttpURLConnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static edu.byu.cs340.communication.CommunicatorHelper.readString;
import static edu.byu.cs340.communication.CommunicatorHelper.writeString;

/**
 * Created by joews on 2/1/2017.
 *
 * Handler that authenticates and executes lobby and game commands
 */
public class CommandHandler implements HttpHandler
{


	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
	    IMessage result;
	    boolean success = false;
	    try
	    {
		    IAuthenticatableServer serverFacade = ServerFacade.getInstance(IAuthenticatableServer.class);
		    Headers reqHeaders = httpExchange.getRequestHeaders();
		    //get body from http
		    InputStream reqBody = httpExchange.getRequestBody();
		    String requestBody = readString(reqBody);
		    reqBody.close();
		    String authToken = reqHeaders.getFirst(Utils.AUTHORIZATION_HEADER);
		    //authenticate the user
		    BaseUser user = serverFacade.authenticate(authToken);
		    if (user == null)
		    {
			    result = new ErrorMessage(Utils.AUTHORIZATION_OF_REQUEST_FAILED, ErrorMessage.AUTHORIZATION_ERROR);
			    success = false;
		    } else
		    {
			    //make the command from the body
			    IBaseCommand cmd = SerDes.deserializeCommand(requestBody, SerDes.Source.SERVER);
			    cmd.setUsername(user.getUserName());
			    result = serverFacade.executeCommand(cmd);
			    success = true;
		    }
	    } catch (IBaseCommand.CommandException e)
	    {
		    //only gets hit if exception on server. In this case we don't know the cause so send generic message
		    result = new ErrorMessage(e.getMessage(), e.getErrorCode());
	    } catch (ClassNotFoundException e)
	    {
		    result = new ErrorMessage(Utils.REQUEST_COULD_NOT_BE_DESERIALIZED_ON_SERVER, ErrorMessage.FATAL_ERROR);
	    } catch (Exception e)
	    {
//		    e.printStackTrace();
		    result = new ErrorMessage("Unknown error on server", ErrorMessage.FATAL_ERROR);
	    }

	    //take the message and serialize it
	    String serializedResult = SerDes.serializeMessage(result, SerDes.Source.SERVER);
	    //add serialized message in response and send back over network.
	    if (!success)
	    {
		    //if there is an error this will be the header
		    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, serializedResult.length());
	    } else
	    {
		    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, serializedResult.length());
	    }
	    OutputStream outBody;
	    outBody = httpExchange.getResponseBody();
	    writeString(serializedResult, outBody);

    }
}
