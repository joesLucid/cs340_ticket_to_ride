package edu.byu.cs340.models;

import edu.byu.cs340.database.DatabaseWriter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 2/1/2017.
 * THe authenticator object holds users and their authorization tokens. This is used on the server to validate that the
 * message comes form a logged in user.
 *
 * @invariant mUsers!= null
 * @invariant mAuthenticationKeyToUser != null
 */
public class Authenticator implements IAuthenticator
{
	private Map<String, ServerUser> mUsers;//a edu.byu.cs340.models.gameModel of username to user objects
	private Map<String, ServerUser> mAuthenticationKeyToUser;//a edu.byu.cs340.models.gameModel of authentication
	// token to user

	/**
	 * Constructs an authenticator object with no users.
	 *
	 * @pre None
	 * @post mUsers!=null
	 * @post mAuthenticationKeyToUser!=null
	 */
	public Authenticator()
	{
		mUsers = new HashMap<>();
		mAuthenticationKeyToUser = new HashMap<>();

		//temporary.... prone to hacking...
		ServerUser user = new ServerUser("administrator", "iychtkittoae");
		mUsers.put(user.getUserName(), user);
		String auth = user.login(user.getUserName(), "iychtkittoae");
		mAuthenticationKeyToUser.put(auth, user);
	}

	/**
	 * Logs in the user by creating a UUID for the authentication token and sending it as the response.
	 *
	 * @param username username to login
	 * @param password password associated with username
	 * @return Returns the authentication token if login valid, otherwise null.
	 *
	 * @pre hasUserByName(username)==true
	 * @pre username != null
	 * @pre password != null
	 * @pre Utils.isPasswordValid(password)==true
	 * @pre Utils.isUsernameValid(username)==true
	 * @post hasUserByName(username)==true
	 * @post mUsers.get(username).getAuthenticationToken()!=null
	 */
	@Override
	public String login(String username, String password)
	{
		ServerUser user = mUsers.get(username);
		String oldAuthenication = user.getAuthenticationToken();
		String newAuthentication = user.login(username, password);
		if (newAuthentication != null)
		{
			mAuthenticationKeyToUser.remove(oldAuthenication);
			//adds user to user/authentication collection
			mAuthenticationKeyToUser.put(newAuthentication, user);
			//update database
			DatabaseWriter.getInstance().setUser(user);
		}
		System.out.println(user.getAuthenticationToken() + " (" + user.getUserName() + ") logged in");
		return newAuthentication;
	}

	/**
	 * Registers the user and adds them to the users in the authenticator. It also creates and sends back the
	 * Authentication token assocaite with the uer.
	 *
	 * @param username Username to register
	 * @param password Password to associate with the username
	 * @return Returns the authorization token associated with the newly registered user or null otehrwise.
	 *
	 * @pre hasUserByName(username)==false
	 * @pre username != null
	 * @pre password != null
	 * @pre Utils.isPasswordValid(password)==true
	 * @pre Utils.isUsernameValid(username)==true
	 * @post hasUserByName(username)==true
	 * @post mUsers.get(username).getAuthenticationToken()!=null
	 */
	@Override
	public String register(String username, String password)
	{
		String authentication;
		ServerUser user = new ServerUser(username, password);
		mUsers.put(username, user);
		System.out.println(user.getUserName() + " registered");
		authentication = user.login(username, password);

		mAuthenticationKeyToUser.put(authentication, user);
		if (authentication == null || authentication.isEmpty())
		{
			//if authentication is null or empty then remove the user because it errored
			if (mUsers.containsKey(user))
			{
				mUsers.remove(user);
			}
		}
		//update database
		DatabaseWriter.getInstance().setUser(user);
		return authentication;
	}

	/**
	 * Returns whether the authenticator has a given user by username
	 *
	 * @param username Username to check is in the authenticator (can be null).
	 * @return Returns true is the user is in the authenticator, false otherwise.
	 *
	 * @pre None
	 * @post No change.
	 */
	public boolean hasUserByName(String username)
	{
		if (username == null)
		{
			return false;
		} else
		{
			return mUsers.containsKey(username);
		}
	}

	/**
	 * Returns whether the authenticator has the authenticationToken active.
	 *
	 * @param authenticationToken The authentication token to check.
	 * @return True is the authentication token is active in the authenticator, false otherwise.
	 *
	 * @pre None.
	 * @post No change.
	 */
	public boolean hasUserByAuthentication(String authenticationToken)
	{
		if (authenticationToken == null)
		{
			return false;
		} else
		{
			return mAuthenticationKeyToUser.containsKey(authenticationToken);
		}
	}

	/**
	 * Signs the user out of the authenticator and clears there authenticationToken associated with the user.
	 *
	 * @param username Username of the user to signout.
	 * @return Returns true if the user is signed out successfully, false otherwise.
	 *
	 * @pre username!=null
	 * @post mUsers.get(username).getAuthenticationToken == null.
	 */
	@Override
	public boolean signOut(String username)
	{
		if (mUsers.containsKey(username))
		{
			ServerUser user = mUsers.get(username);
			if (user.getAuthenticationToken() == null)
			{
				return false;
			}
			if (mAuthenticationKeyToUser.containsKey(user.getAuthenticationToken()))
			{
				mAuthenticationKeyToUser.remove(user.getAuthenticationToken());
				user.removeAuthenticationToken();
				System.out.println(user.getUserName() + " signed out. token = " + user.getAuthenticationToken());
				return true;
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}

	}

	/**
	 * Verifies the authenticationToken is stored in the authenticator and returns the user associated with it.
	 *
	 * @param authenticationToken AuthenticationToken to verify is in the authenticator.
	 * @return Returns the BaseUser associated with the authentication token or null otherwise.
	 *
	 * @pre authenticationToken=!=null
	 * @post No change.
	 */
	@Override
	public BaseUser authenticate(String authenticationToken)
	{
		return mAuthenticationKeyToUser.get(authenticationToken);
	}

	@Override
	public void loadFromDatabase()
	{
		//get the users and set them
		List<ServerUser> users = DatabaseWriter.getInstance().getUsers();
		mUsers = new HashMap<>();
		mAuthenticationKeyToUser = new HashMap<>();
		for (ServerUser user : users)
		{
			mUsers.put(user.getUserName(), user);
			mAuthenticationKeyToUser.put(user.getAuthenticationToken(), user);
		}
	}
}
