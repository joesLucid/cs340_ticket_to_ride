package edu.byu.cs340.models.gameModel.players;

import edu.byu.cs340.models.lobbyModel.LobbyPlayerManager;
import edu.byu.cs340.models.roomModel.IPlayer;
import edu.byu.cs340.models.roomModel.PlayerManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by joews on 2/23/2017.
 *
 * Object that holds the game players.
 */
public class GamePlayerManager extends PlayerManager
{
	private String currentPlayer;
	private List<String> playerOrder;

	/**
	 * Constructor that creates the game player manager with no players inside.
	 */
	public GamePlayerManager()
	{
		super();
		playerOrder = new ArrayList<>();
	}

	/**
	 * Constructor that creates the manager with hostName as a player.
	 *
	 * @param hostName The name of the host player.
	 */
	public GamePlayerManager(String hostName)
	{
		super(hostName);
		currentPlayer = hostName;
		playerOrder = new ArrayList<>();
		initializePlayerOrder();
	}

	/**
	 * Overridesthe addPlayer method to set the player order as well.
	 *
	 * @param playerName The playerName being added.
	 * @return Returns true if the player is added, false otherwise.
	 */
	@Override
	public boolean addPlayer(String playerName)
	{
		boolean success = super.addPlayer(playerName);
		initializePlayerOrder();
		return success;
	}

	@Override
	protected IPlayer createPlayer(String playerName)
	{
		if (playerName != null)
		{
			return new GamePlayer(playerName);
		} else
		{
			return null;
		}
	}

	@Override
	public GamePlayer getPlayer(String username)
	{
		IPlayer player = super.getPlayer(username);
		if (player != null)
		{
			return (GamePlayer) player;
		} else
		{
			return null;
		}
	}

	/**
	 * @param lobbyState
	 */
	public void initializeFromLobby(LobbyPlayerManager lobbyState)
	{
		this.mHostName = lobbyState.getHost();
		List<IPlayer> lobbyPlayers = lobbyState.getPlayers();
		for (IPlayer player : lobbyPlayers)
		{
			IPlayer gamePlayer = createPlayer(player.getUsername());
			gamePlayer.setIsHost(player.isHost());
			this.mPlayers.put(player.getUsername(), gamePlayer);
		}
		initializePlayerOrder();
	}

	/**
	 *
	 */
	protected void initializePlayerOrder()
	{
		playerOrder.clear();
		for (IPlayer player : getPlayers())
		{
			playerOrder.add(player.getUsername());
		}
		Collections.shuffle(playerOrder);
		currentPlayer = null;
	}

	/**
	 * Changes to the next player.
	 */
	public void nextPlayer()
	{
		currentPlayer = getNextPlayerName();
	}

	/**
	 * @param username
	 * @return
	 */
	public boolean isCurrentPlayer(String username)
	{
		return getCurrentPlayer().getUsername().equals(username);
	}

	/**
	 * @return
	 */
	public GamePlayer getCurrentPlayer()
	{
		if (currentPlayer == null)
		{
			return getPlayer(playerOrder.get(0));
		}
		return getPlayer(currentPlayer);
	}

	/**
	 * Get value of PlayerOrder
	 *
	 * @return Returns a copy of the PlayerOrder
	 */
	public List<String> getPlayerOrder()
	{
		return Collections.unmodifiableList(playerOrder);
	}

	public String getNextPlayerName()
	{
		if (currentPlayer == null)
		{
			return playerOrder.get(0);
		}
		int index = playerOrder.indexOf(currentPlayer);
		index++;
		if (index >= playerOrder.size())
		{
			index = 0;
		}
		return playerOrder.get(index);
	}

	public void removePlayer(GamePlayer player)
	{
		if(getCurrentPlayer().equals(player))
		{
			nextPlayer();
		}
		playerOrder.remove(player.getUsername());
		super.removePlayer(player.getUsername());
	}

	public String getPlayerBefore(String username)
	{
		int index = playerOrder.indexOf(username);
		index--;
		if (index < 0)
		{
			index = playerOrder.size() - 1;
		}
		return playerOrder.get(index);
	}

	public void setNextPlayer(String nextPlayer)
	{
		if (playerOrder.contains(nextPlayer))
		{
			currentPlayer = nextPlayer;
		}
	}
}


