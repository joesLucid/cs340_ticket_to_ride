package edu.byu.cs340.models.gameModel.players;

import edu.byu.cs340.models.gameModel.GameParameters;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.gameOver.PlayerScoreStruct;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IGameMap;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.roomModel.Player;

import java.util.*;

/**
 * Created by joews on 2/2/2017.
 *
 * A player in the game held by the game models. This extends the functionality of IPlayer. Specifically, it will hold
 * information related to the game state for this particular player.
 *
 * @invariant getDestinationCards()!=null
 * @invariant getTrainCards()!=null
 * @invariant getScore()>=0
 * @invariant 0&lte;getTrainsRemaining()&lte;STARTING_NUM_TRAINS
 */
public class GamePlayer extends Player
{
	private List<IDestinationCard> mDestinationCards;
	private List<IDestinationCard> mTemporaryDestinationCards;
	private List<ITrainCard> mTrainCards;
	private int mTrainsRemaining;
	private int mPointsFromCompletedDestCards = 0;
	private int mPointsLostFromNotUnsatisfiedDestCards = 0;
	private int mPointsFromLongestRoute = 0;
	private int mPointsFromClaimedRoutes = 0;
	private List<IRoute> mRoutes;
	private Map<String, List<IRoute>> mCityRouteMap;
	private int mLongestRouteSize;

	/**
	 * Constructor for game player.
	 *
	 * @param user Name of user
	 * @pre user!=null & !user.isEmpty()
	 */
	public GamePlayer(String user)
	{
		super(user);
		mDestinationCards = new ArrayList<>();
		mTemporaryDestinationCards = new ArrayList<>();
		mTrainCards = new ArrayList<>();
		mRoutes = new ArrayList<>();
		mTrainsRemaining = GameParameters.STARTING_NUM_TRAINS;
		mLongestRouteSize = 0;
	}


	/**
	 * Get value of DestinationCards
	 *
	 * @return Returns the value of DestinationCards
	 *
	 * @post result != null
	 */
	public List<IDestinationCard> getDestinationCards()
	{
		return mDestinationCards;
	}

	/**
	 * Set the value of destinationCards variable and overrides existing cards
	 *
	 * @param destinationCards The value to set destinationCards member variable to
	 * @pre destinationCards != null
	 */
	public void setDestinationCards(List<IDestinationCard> destinationCards)
	{
		mDestinationCards = destinationCards;
	}

	/**
	 * Get value of TrainCards
	 *
	 * @return Returns the value of TrainCards
	 *
	 * @post result != null
	 */
	public List<ITrainCard> getTrainCards()
	{
		return mTrainCards;
	}

	/**
	 * Set the value of trainCards variable
	 *
	 * @param trainCards The value to set trainCards member variable to
	 * @pre trainCards != null
	 */
	public void setTrainCards(List<ITrainCard> trainCards)
	{
		mTrainCards = trainCards;
	}

	/**
	 * Adds the card to the cards the player is holding.
	 *
	 * @param card Train card to add to the player.
	 * @pre card != null
	 */
	public void addTrainCard(ITrainCard card)
	{
		if (card != null && !mTrainCards.contains(card))
		{
			mTrainCards.add(card);
		}
	}

	/**
	 * Adds thee card to the destination cards the player is holding. Should be called after setting the temporary
	 * destination cards. Only adds if the card is in temporary destination cards.
	 *
	 * @param card Card to add. Should be one of the temporary destination cards.
	 * @pre getTemporaryDestinationCards().contains(card);
	 */
	public void addDestinationCard(IDestinationCard card)
	{
		if (card != null && !mDestinationCards.contains(card))
		{
			if (mTemporaryDestinationCards.contains(card))
			{
				mTemporaryDestinationCards.remove(card);
				mDestinationCards.add(card);
			}
		}
	}

	/**
	 * Adds a list of destination cards to the cards the player is holidng.
	 *
	 * @param cards The cards to add.
	 * @pre addTemporaryDestinationCards(cards2) has been called
	 * @pre hasTemporaryCards()==true
	 * @pre cards2 contains all of cards.
	 */
	public void addDestinationCards(List<IDestinationCard> cards)
	{
		if (cards != null)
		{
			for (IDestinationCard card : cards)
			{
				addDestinationCard(card);
			}
		}
	}

	/**
	 * Adds the temporary destination cards the player is to chose from.
	 *
	 * @param cards Cards the player is to chose form.
	 */
	public void addTemporaryDestinationCards(List<IDestinationCard> cards)
	{
		if (cards != null)
		{
			for (IDestinationCard card : cards)
			{
				if (card != null && !mTemporaryDestinationCards.contains(card))
				{
					mTemporaryDestinationCards.add(card);
				}
			}
		}
	}

	/**
	 * Returns the temporary destination cards the player is to choose from.
	 *
	 * @return the list of temporary destination cards the player is holding.
	 */
	public List<IDestinationCard> getTemporaryDestinationCards()
	{
		return Collections.unmodifiableList(mTemporaryDestinationCards);
	}

	/**
	 * Removes all of hte temperaory destination cards.
	 *
	 * @pre getTemporaryDestinationCards() has been called and hte output has been stored elsewhere.
	 */
	public void removeTemporaryDestinationCards()
	{
		mTemporaryDestinationCards.clear();
	}

	/**
	 * Returns whether the player has any temporary cards
	 *
	 * @return True if htey have some, false otehrwise
	 */
	public boolean hasTemporaryCards()
	{
		return mTemporaryDestinationCards.size() != 0;
	}

	/**
	 * Get value of TrainsRemaining
	 *
	 * @return Returns the value of TrainsRemaining
	 *
	 * @post 0&lte;result&lte;STARTING_NUM_TRAINS
	 */
	public int getTrainsRemaining()
	{
		return mTrainsRemaining;
	}

	/**
	 * Set the value of trainsRemaining variable
	 *
	 * @param trainsRemaining The value to set trainsRemaining member variable to
	 * @post 0&lte;trainsRemaining&lte;STARTING_NUM_TRAINS
	 */
	public void setTrainsRemaining(int trainsRemaining)
	{
		mTrainsRemaining = trainsRemaining;
	}

	/**
	 * sets the longest route
	 * @param size size of the longest route
	 */
	public void setLongestRouteSize(int size) { this.mLongestRouteSize = size; }

	/**
	 * Returns the size of this player's longest route
	 * @return the size of the this player's longest route
	 */
	public int getLongestRouteSize()
	{
		return this.mLongestRouteSize;
	}

	/**
	 * Get value of Score
	 *
	 * @return Returns the value of Score
	 *
	 * @post result&gte;0
	 */
	public int getScore()
	{
		return mPointsFromClaimedRoutes + mPointsFromCompletedDestCards + mPointsFromLongestRoute +
				mPointsLostFromNotUnsatisfiedDestCards;
	}

	public int getHiddenScore()
	{
		return mPointsFromCompletedDestCards + mPointsLostFromNotUnsatisfiedDestCards;
	}

	/**
	 * Gets the value for score that's visable to all players
	 * @return returns public score
	 */
	public int getPublicScore()
	{
		return mPointsFromClaimedRoutes + mPointsFromLongestRoute;
	}

	/**
	 * Updates teh score of hte player
	 *
	 * @param isLongestRoute if true, adds 10 point bonous to player
	 */
	public void updateScore(boolean isLongestRoute)
	{
		mPointsLostFromNotUnsatisfiedDestCards = 0;
		mPointsFromCompletedDestCards = 0;
		mPointsFromLongestRoute = 0;
		mPointsFromClaimedRoutes = 0;
		prepCityMapRoute();
		for (IDestinationCard card : mDestinationCards)
		{
			if (isDestCardSatisfied(card))
			{
				mPointsFromCompletedDestCards += card.getPoints();
			} else
			{
				mPointsLostFromNotUnsatisfiedDestCards -= card.getPoints();
			}
		}
		for (IRoute route : mRoutes)
		{
			mPointsFromClaimedRoutes += route.getPoints();
		}
		if (isLongestRoute)
		{
			mPointsFromLongestRoute = IGameMap.LONGEST_ROUTE_POINTS;
		} else
		{
			mPointsFromLongestRoute = 0;
		}
	}

	private void prepCityMapRoute()
	{
		mCityRouteMap = new HashMap<>();
		for (IRoute route : mRoutes)
		{
			if (mCityRouteMap.containsKey(route.getCities().get(0).getId()))
			{
				mCityRouteMap.get(route.getCities().get(0).getId()).add(route);
			} else
			{
				List<IRoute> routes = new ArrayList<>();
				routes.add(route);
				mCityRouteMap.put(route.getCities().get(0).getId(), routes);
			}
			if (mCityRouteMap.containsKey(route.getCities().get(1).getId()))
			{
				mCityRouteMap.get(route.getCities().get(1).getId()).add(route);
			} else
			{
				List<IRoute> routes = new ArrayList<>();
				routes.add(route);
				mCityRouteMap.put(route.getCities().get(1).getId(), routes);
			}
		}
	}

	private boolean isDestCardSatisfied(IDestinationCard card)
	{
		//get start city and end city
		ICity start = card.getCities().get(0);
		ICity end = card.getCities().get(1);
		Set<ICity> visitedCities = new HashSet<>();
		visitedCities.add(start);
		return checkConnected(start, end, visitedCities);
	}

	private boolean checkConnected(ICity start, ICity end, Set<ICity> visitedCities)
	{
		List<IRoute> routes = getRoutesConnectedTo(start);
		Set<ICity> citySet = new HashSet<>();
		if (routes != null)
		{
			for (IRoute route : routes)
			{
				ICity testCity = null;
				if (route.getCities().get(0).equals(start))
				{
					testCity = route.getCities().get(1);
				} else
				{
					testCity = route.getCities().get(0);
				}
				if (testCity.equals(end))
				{
					return true;
				} else if (!visitedCities.contains(testCity))
				{
					citySet.add(testCity);
					visitedCities.add(testCity);
				}
			}
		}
		for (ICity city : citySet)
		{
			if (checkConnected(city, end, visitedCities))
			{
				return true;
			}
		}
		return false;
	}

	private List<IRoute> getRoutesConnectedTo(ICity start)
	{
		return mCityRouteMap.get(start.getId());
	}

	/**
	 * Get value of Routes
	 *
	 * @return Returns the value of Routes
	 *
	 * @post result!=null
	 */
	public List<IRoute> getRoutes()
	{
		return mRoutes;
	}

	/**
	 * Set the value of routes variable
	 *
	 * @param routes The value to set routes member variable to
	 * @pre routes!=null
	 */
	public void setRoutes(List<IRoute> routes)
	{
		mRoutes = routes;
	}

	/**
	 * Adds the route to the player's routes. Updates the player's score.
	 *
	 * @param route The route to add to the player.
	 * @pre Route+null
	 * @pre route.getNumTrains()>=getTrainsRemaining()
	 */
	public void claimRoute(IRoute route)
	{
		if (route != null && !mRoutes.contains(route))
		{
			mTrainsRemaining -= route.getNumTrains();
			mRoutes.add(route);
		}
	}


	/**
	 * Removes the train cards tfrom the list of train cards if they exist
	 *
	 * @param trainCards Train cards to discard.
	 * @pre trainCards!=null and each train card in trainCards != null
	 */
	public void discardTrainCards(List<ITrainCard> trainCards)
	{
		mTrainCards.removeAll(trainCards);
	}

	public PlayerScoreStruct getScoreStruct()
	{
		PlayerScoreStruct scoreStruct = new PlayerScoreStruct();
		scoreStruct.mUsername = getUsername();
		scoreStruct.mPointsFromLongestRoute = mPointsFromLongestRoute;
		scoreStruct.mPointsFromReachedDestinations = mPointsFromCompletedDestCards;
		scoreStruct.mPointsLostFromUnreachedDestinations = mPointsLostFromNotUnsatisfiedDestCards;
		scoreStruct.mPointsFromClaimedRoutes = mPointsFromClaimedRoutes;
		scoreStruct.mTotalPoints = getScore();
		scoreStruct.mLongestPath = getLongestRouteSize();
		scoreStruct.mDestCompleted = getNumDestCompleted();
		return scoreStruct;
	}

	private int getNumDestCompleted()
	{
		int completed = 0;
		for (IDestinationCard card : mDestinationCards)
		{
			if (isDestCardSatisfied(card))
			{
				completed++;
			}
		}
		return completed;
	}

}
