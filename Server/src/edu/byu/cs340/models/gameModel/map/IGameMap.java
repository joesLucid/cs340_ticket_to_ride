package edu.byu.cs340.models.gameModel.map;

import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.List;
import java.util.Set;

/**
 * Created by joews on 2/24/2017.
 *
 * Interface for the game map. Stores the routes, cities and longest route player name.
 *
 * @invariant None
 */
public interface IGameMap
{
	/**
	 * The points associated with the longest route.
	 */
	int LONGEST_ROUTE_POINTS = 10;
	/**
	 * Gets the route assocaited with the ID
	 *
	 * @param routeID The ID of the route to get
	 * @return Route associated ID or null.
	 *
	 * @pre routeID!=null && !routeID.isEmpty()
	 * @pre isInitialized() == true
	 * @post if hasRoute(routeID) == true then result.getId().equals(routeID)
	 * @post if hasRoute(routeID) == false then result == null
	 */
	IRoute getRoute(String routeID);

	/**
	 * Returns whther the game map has teh route associated with the id.
	 *
	 * @param routeID The id of the route.
	 * @return True if the route exists, false otherwise
	 *
	 * @pre routeID != null
	 * @pre isInitialized() == true
	 * @post return true is route is in the map, false otherwise.
	 */
	boolean hasRoute(String routeID);

	/**
	 * Claims the route by id associated with the user.
	 *
	 * @param routeID  The ID of the route to claim
	 * @param username the username who calims the route
	 * @return true if the route was claimed, false otherwise
	 *
	 * @pre routeID != null && !routeID.isEmpty()
	 * @pre username!= null && !username.isEmpty()
	 * @pre hasRoute(routeID) == true
	 * @pre isInitialized() == true
	 * @post if route has not been claimed getRoute(routeID).getClaimedUser().equals(username)
	 */
	boolean claimRoute(String routeID, String username);


	/**
	 * Gets the routes on the edu.byu.cs340.models.gameModel.
	 *
	 * @return A copy of the routes in the edu.byu.cs340.models.gameModel.
	 *
	 * @post if isInitialized() == true, returns a copy of the routes in the map
	 * @post if isInitialized() == false, returns null
	 */
	List<IRoute> getRoutes();

	/**
	 * Gets all of the cities on the edu.byu.cs340.models.gameModel.
	 *
	 * @return A copy of the cities on the edu.byu.cs340.models.gameModel.
	 *
	 * @post if isInitialized() == true, returns a copy of the cities in the map
	 * @post if isInitialized() == false, returns null
	 */
	List<ICity> getCities();

	/**
	 * Gets the username of the player with the longest route.
	 *
	 * @return The username of the player with hte longest route, null if there is no player with the longest route.
	 */
	Set<String> getLongestRoutePlayer();

	void setLongestRoutePlayer(Set<String> usernames);

	/**
	 * Sets the routes.
	 *
	 * @param routes Sets the routes.
	 * @pre routes != null
	 * @post the items in routes match the items in getRoutes()
	 */
	void setRoutes(List<IRoute> routes);

	/**
	 * Sets the cities.
	 *
	 * @param cities Sets the routes.
	 * @pre cities != null
	 * @post the items in cities match the items in getCities()
	 */
	void setCities(List<ICity> cities);

	/**
	 * Tells if the routes and cities have been set.
	 *
	 * @return True if the routes and cities have been set, false otherwise.
	 * @pre None
	 * @post true if the routes and cities are non-null and non-empty
	 */
	boolean isInitialized();

	/**
	 * Finds the length of the longest route for a player
	 * @param player the player to find the longest route for
	 * @return the size of the longest route
	 */
	int calculateLongestRoute(GamePlayer player);
}
