package edu.byu.cs340.models.gameModel.bank;

import java.util.List;

/**
 * Created by joews on 2/22/2017.
 * Interface for the game bank which holds the bank in the bank.
 */
public interface IGameBank
{
	/**
	 * Shuffles the destination bank.
	 *
	 * @pre setDestinationCards(List&lt;DestinationCard&gt; has been called
	 * @post destination bank are in a random order
	 */
	void shuffleDestinationCards();

	/**
	 * Shuffles the train bank.
	 *
	 * @pre setTrainCards(List&lt;TrainCard&gt; has been called
	 * @post train bank are in a random order
	 */
	void shuffleTrainCards();

	/**
	 * Clears the current destination bank. Sets the bank in the list
	 * as the new destination bank. Then puts
	 *
	 * @param destinationCards The bank to set as the deck of destination bank.
	 * @pre destinationCards != null
	 */
	void setDestinationCards(List<IDestinationCard> destinationCards);

	/**
	 * Ensures that if there are 5 or more train bank in the deck/discard/revealed pile, then there are 5 bank
	 * revealed. If a revealed zone currently has no card it will draw the top card of the deck and place it in the
	 * empty spot. If there are 3 or more locomotives revealed, it will discard them and show 5 more. If there are no
	 * bank left in the deck and it needs to draw the discard gets shuffled into the deck.
	 */
	void updateRevealedTrainCards();

	/**
	 * Clears the current train bank, discarded train bank and the revealed zone bank. Sets the bank in the list
	 * as the new train bank.
	 *
	 * @param trainCards The bank to set as the deck of train bank.
	 */
	void setTrainCards(List<ITrainCard> trainCards);

	/**
	 * Draws the top cad from the destination card deck and returns it. It is removed from the deck. If the deck is
	 * empty it will return null.
	 *
	 * @return The card drawn or null if no bank are available.
	 *
	 * @post returns the destination card drawn or null if non are available
	 */
	IDestinationCard drawDestinationCardFromDeck();

	/**
	 * Gets the train card from the designated revealedZone (index). Replaces the revealed card
	 * with the top card of the train card deck.
	 *
	 * @param revealedZone The revealed zone 0-4.
	 * @return The train card selected.
	 *
	 * @pre 4&lte;revealedZone&gte;0
	 */
	ITrainCard drawTrainCardFromRevealed(int revealedZone);

	/**
	 * Draws the top cad from the train card deck and returns it. It is removed from the deck. If the deck is
	 * empty it will shuffle the discard pile and use that as the deck. If the discard pile is also empty it will
	 * return null.
	 *
	 * @return The card drawn or null if no bank are available.
	 *
	 * @post returns the train card drawn or null if non are available
	 * @post if the deck is empty when drawing, it will set the deck as the discard pile and shuffle it.
	 */
	ITrainCard drawTrainCardFromDeck();

	/**
	 * Returns an unmodifiable list of Train Cards that are the revealed bank.
	 *
	 * @return An unmodifiable list of hte revealed train bank.
	 */
	List<ITrainCard> getRevealedCards();

	/**
	 * Puts the returned destination bank on the bottom of the destination card deck in the order listed.
	 *
	 * @param destinationCardList Cards to put on bottom of destination card deck.
	 */
	void putDestinationCardsOnBottom(List<IDestinationCard> destinationCardList);

	/**
	 * puts the train bank in the discard pile.
	 *
	 * @param trainCards The train bank to discard.
	 */
	void putTrainCardsInDiscard(List<ITrainCard> trainCards);

	/**
	 * says is the bank has had destination and train cards set.
	 *
	 * @return True if the traing cards and destination cards have been set.
	 */
	boolean isIntialized();

	/**
	 * The number of train card current in the deck
	 *
	 * @return The numbder of train cards in the deck
	 */
	int getNumTrainCardsInDeck();

	/**
	 * The number of destination cards current in the deck
	 *
	 * @return The number of destination cards in the deck
	 */
	int getNumDestinationCardsInDeck();

//	/**
//	 * Method to return destination cards only to be used with demo. DELETE after phase 2.
//	 *
//	 * @return A list of the destination cards.
//	 */
//	List<IDestinationCard> getDestinationCardDeck();
//
//	/**
//	 * Method to return train cards only to be used with demo. DELETE after phase 2.
//	 * @return A list of the train cards.
//	 */
//	List<ITrainCard> getDemoTrainCardDeck();

	/**
	 *
	 * @return count of train cards in discard
	 */
	int trainCardDiscardCount();
}
