package edu.byu.cs340.models.gameModel.server_state;

import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.map.TrainType;
import edu.byu.cs340.models.gameModel.players.GamePlayer;

/**
 * Created by tjacobhi on 23-Mar-17.
 */
public class DrawTrainCardState extends ServerState
{
	private int MAX_CARDS = 2;
	private int numberOfCardsDrawn = 0;
	boolean canDrawRevealved;

	public DrawTrainCardState(int numberOfCardsDrawn)
	{
		this.numberOfCardsDrawn = numberOfCardsDrawn;
		this.canDrawRevealved = true;
	}

	public DrawTrainCardState(int numberOfCardsDrawn, IServerStatable args)
	{
		this.numberOfCardsDrawn = numberOfCardsDrawn;
		canDrawRevealved = true;
		//TODO: WHY ???? Why are we drawing a card on the constructor? This is really throwing it off..
//		takeAction(StateActions.DrawTrainCardDeck, args);
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args)
	{
		if (action.equals(StateActions.DrawTrainCardDeck))
		{
			ITrainCard card = ((ServerGameModel) args).drawTrainCardFromDeck();
			if (card != null)
			{
				((GamePlayer) args.getPlayer(args.getCurrentPlayerName())).addTrainCard(card);
				numberOfCardsDrawn++;
			}
			boolean hasNonLocomtive = false;
			boolean hasNonNull = false;
			for (ITrainCard card2 : ((ServerGameModel) args).getRevealedCards())
			{
				if (card2 != null)
				{
					hasNonNull = true;
					if (card2.getTrainType() != TrainType.Locomotive)
					{
						hasNonLocomtive = true;
						break;
					}
				}
			}
			if (numberOfCardsDrawn >= MAX_CARDS)
			{
				args.setState(new PlayerTurnState(args));
			} else if (numberOfCardsDrawn == 1 && !(hasNonLocomtive ||
					((ServerGameModel) args).getNumTrainCardsInDeck()
							> 0 || ((ServerGameModel) args).getNumTrainCardsInDiscard() > 0))
			{
				args.setState(new PlayerTurnState(args));
			} else if (numberOfCardsDrawn == 0)
			{
				args.setState(new PlayerTurnState(args));
			}
			return card;
		}
		return null;
	}


	@Override
	public Object takeAction(StateActions action, IServerStatable args, Object values)
	{
		if (action.equals(StateActions.DrawTrainCardDeck))
		{
			takeAction(action, args);
		} else if (action.equals(StateActions.DrawTrainCardRevealed))
		{
			GamePlayer player = ((ServerGameModel) args).getPlayer(args.getCurrentPlayerName());
			if (values instanceof Integer)
			{
				ITrainCard card = ((ServerGameModel) args).getGameBank().drawTrainCardFromRevealed((Integer) values);
				if (card != null)
				{
					player.addTrainCard(card);
					if (card.getTrainType().equals(TrainType.Locomotive))
					{
						args.setState(new PlayerTurnState(args));
					}
					numberOfCardsDrawn++;
					if (numberOfCardsDrawn >= MAX_CARDS)
					{
						args.setState(new PlayerTurnState(args));
					}
				}
				boolean hasNonLocomtive = false;
				boolean hasNonNull = false;
				for (ITrainCard card2 : ((ServerGameModel) args).getRevealedCards())
				{
					if (card2 != null)
					{
						hasNonNull = true;
						if (card2.getTrainType() != TrainType.Locomotive)
						{
							hasNonLocomtive = true;
							break;
						}
					}
				}
				if (numberOfCardsDrawn >= MAX_CARDS)
				{
					args.setState(new PlayerTurnState(args));
				} else if (numberOfCardsDrawn == 1 && !(hasNonLocomtive ||
						((ServerGameModel) args).getNumTrainCardsInDeck()
								> 0 || ((ServerGameModel) args).getNumTrainCardsInDiscard() > 0))
				{
					args.setState(new PlayerTurnState(args));
				} else if (numberOfCardsDrawn == 0)
				{
					args.setState(new PlayerTurnState(args));
				}

				return card;
			}
		}
		return null;
	}

	@Override
	public void verifyState(StateActions action, IServerStatable args)
	{

	}
}
