package edu.byu.cs340.models.gameModel.server_state;

/**
 * Created by tjacobhi on 23-Mar-17.
 */
public class PlayerTurnState extends ServerState
{
	public PlayerTurnState(IServerStatable args)
	{
		if (args.isFinalRound() && !args.isLastTurn() && args.getCurrentPlayerName().equals(args
				.getFinalRoundLastPlayer()))
		{
			args.markLastTurn();
		} else if (args.isLastTurn() && args.getCurrentPlayerName().equals(args.getFinalRoundLastPlayer()))
		{
			args.setState(new GameOverState(args));
		}
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args)
	{

		return null;
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args, Object values)
	{


		return null;
	}

	@Override
	public void verifyState(StateActions action, IServerStatable args)
	{
		if (action.equals(StateActions.DrawTrainCardDeck))
		{
			args.setState(new DrawTrainCardState(0, args));
		} else if (action.equals(StateActions.DrawDestinationCards))
		{
			args.setState(new DrawDestinationCardsState(args));
		} else if (action.equals(StateActions.DrawTrainCardRevealed))
		{
			args.setState(new DrawTrainCardState(0, args));
		}
	}
}
