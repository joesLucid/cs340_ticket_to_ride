package edu.byu.cs340.models.gameModel;

/**
 * Created by joews on 3/27/2017.
 */
public class GameParameters
{
	/**
	 * The number of trains per player by default.
	 */
	public static int STARTING_NUM_TRAINS = 45;
	public static int MAX_LOCOMOTIVE_ALLOWED = 3;
	public static int NUM_REVEALED_CARDS = 5;
	public static int FINAL_ROUND_TRAINS_REMAINING = 2;
	//the maximum number of players that can be in a room
	public static int MAX_PLAYERS = 5;
	//the maximum number of players that can be in a room
	public static int MIN_PLAYERS = 2;
}
