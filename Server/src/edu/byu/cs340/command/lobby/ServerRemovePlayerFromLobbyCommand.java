package edu.byu.cs340.command.lobby;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.roomModel.IPlayerRoom;

/**
 * Created by joews on 2/1/2017.
 *
 * Removes the player from a lobby on the server.
 */
public class ServerRemovePlayerFromLobbyCommand extends RemovePlayerFromLobbyCommand
{

	public static final String CAN_T_REMOVE_SELF_NOT_FOUND_IN_ROOM = "Can't remove self. Not found in room";
	public static final String YOU_DON_T_HAVE_PERMISISON_TO_REMOVE_THIS_PLAYER = "You don't have permisison to remove " +
			"this player";

	public static final String CAN_T_REMOVE_PLAYER_FROM_NON_EXISTANT_ROOM = "Can't remove player from non-existant " +
			"room";


	public ServerRemovePlayerFromLobbyCommand(String username, int lastCommand, String lobbyID, String
			userNameToRemove)
	{
		super(username, lastCommand, lobbyID, userNameToRemove);
	}

	/**
	 * Removes the player from the speciefied lobby.
	 *
	 * @return An IMessage that holds the list of comands that the user needs to run.
	 *
	 * @throws CommandException Exception saying what type of error occurred and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		boolean proceed = false;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		ServerCommandUtils.validatePlayerInLobby(mUsername, mLobbyID);
		if (mLobbyID == null)
		{
			throw new CommandException(ServerCommandUtils.LOBBY_ID_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		}

		//if the data is valid, now check if the user requesting the command matches the user to remove
		boolean removeSelf = mUsername.equals(userNameToRemove);
		boolean roomExists = manager.hasRoom(mLobbyID);
		if (removeSelf)
		{

			//if it matches then check if the room exists.
			if (roomExists)
			{
				//if the room exists remove the player.
				proceed = true;
			} else
			{
				throw new CommandException(CAN_T_REMOVE_SELF_NOT_FOUND_IN_ROOM, ErrorMessage.FATAL_ERROR);
			}
		} else
		{
			//if you are trying to remove someone else first make sure the player sending command is host
			if (roomExists && manager.getRoom(mLobbyID).getHost().equals(mUsername))
			{
				proceed = true;
			}//if the room exists but the user requesting remove isn't host, return an error
			else if (roomExists)
			{
				throw new CommandException(YOU_DON_T_HAVE_PERMISISON_TO_REMOVE_THIS_PLAYER, ErrorMessage.MILD_ERROR);
			}//if room deosn't exist return an error
			else
			{
				throw new CommandException(CAN_T_REMOVE_PLAYER_FROM_NON_EXISTANT_ROOM, ErrorMessage.MILD_ERROR);
			}
		}

		if (proceed)
		{
			IPlayerRoom room = manager.getRoom(mLobbyID);
			if (room instanceof LobbyModel)
			{
				LobbyModel lobby = (LobbyModel) room;
				lobby.addCommand(this);
				manager.removePlayer(userNameToRemove, mLobbyID);
				newHost = lobby.getHost();
				if (lobby.isEmpty())
				{
					String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
					result = new CommandMessage(cmd);
				} else
				{
					result = ServerCommandUtils.getCommandsFromLobby(mLobbyID, mLastCommand,mUsername);
				}
			} else
			{
				result = ServerCommandUtils.getCommandsFromGame(mLobbyID, -1,mUsername);
			}
		}
		return result;
	}
}
