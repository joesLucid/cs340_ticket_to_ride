package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.IAuthenticator;
import edu.byu.cs340.models.RoomManager;

/**
 * Created by joews on 2/11/2017.
 */
public class ServerUpdateLobbyCommand extends UpdateLobbyCommand
{
	public ServerUpdateLobbyCommand(String username, int lastCommand, String lobbyID)
	{
		super(username, lastCommand, lobbyID);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		String message = null;
		boolean success;
		IMessage result;

		//get data  manager to work with
		ICommandServerFacade serverFacade = ServerFacade.getInstance(ICommandServerFacade.class);
		IAuthenticator authenticator = serverFacade.getAuthenticator();
		RoomManager roomManager = serverFacade.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		//validate room because if the game has launched that is fine, we want ot just see if the room exists. The
		// operation will auto pull form the game if it started
		ServerCommandUtils.validatePlayerInRoom(mUsername, mLobbyID);


		//OPERATIONS:
		//simply pull the recent commnads
		result = ServerCommandUtils.getCommandsFromLobby(mLobbyID, mLastCommand, mUsername);

		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		return null;
	}
}
