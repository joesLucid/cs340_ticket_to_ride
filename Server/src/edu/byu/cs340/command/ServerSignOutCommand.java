package edu.byu.cs340.command;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.IAuthenticator;

/**
 * Created by Rebekah on 2/8/2017.
 *
 * Comand to sign the player out of the server.
 */
public class ServerSignOutCommand extends SignOutCommand
{
	/**
	 * @param username the name of the user
	 * @param lobbyID  Nullable the lobbyID if they're in one
	 * @param gameID   Nullable the GameID if they're in one
	 * @pre username is not null
	 */
	public ServerSignOutCommand(String username, String lobbyID, String gameID)
	{
		super(username, lobbyID, gameID);
	}

	/**
	 * Signs the player out of the authenticator.
	 *
	 * @return Returns this comand to execute
	 *
	 * @throws CommandException thrown if error and it holds the error message and error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result;
		ICommandServerFacade serverFacade = ServerFacade.getInstance(ICommandServerFacade.class);
		IAuthenticator authenticator = serverFacade.getAuthenticator();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);

		//OPERATION:
		boolean success = authenticator.signOut(mUsername);
		if (success)
		{
			String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
			result = new CommandMessage(cmd);
		} else
		{
			throw new CommandException("user was not signed out successfully:", ErrorMessage.MILD_ERROR);
		}

		return result;
	}
}
