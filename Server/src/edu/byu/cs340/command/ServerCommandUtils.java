package edu.byu.cs340.command;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.Utils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.message.CommandListMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.models.IAuthenticator;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.lobbyModel.ServerLobbyModel;
import edu.byu.cs340.models.roomModel.IPlayerRoom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 2/11/2017.
 *
 * Utils class that the server commands can use.
 */
public abstract class ServerCommandUtils
{
	public static final String ROOM_IS_NULL = "Room is null";
	public static final String UNABLE_TO_GET_COMMANDS_FROM_ROOM = "Unable to get commands from room.";
	public static final String USERNAME_CAN_NOT_BE_NULL = "Username can not be null.";
	public static final String ERROR_INVALID_USERNAME_LENGTH = "Username must be between " + Utils
			.MIN_USERNAME_LENGTH + " and " + Utils.MAX_USERNAME_LENGTH + " characters long, non null and " +
			"alpha-numeric.";
	public static final String ERROR_INVALID_PASSWORD_LENGTH = "Password must be between " + Utils.MIN_PASSWORD_LENGTH
			+ " and " + Utils.MAX_PASSWORD_LENGTH + " characters long, non null and alpha-numeric.";
	public static final String PASSWORD_CAN_NOT_BE_NULL = "Password can not be null.";
	public static final String LOBBY_NOT_FOUND_ON_SERVER = "Lobby not found on server.";
	public static final String LOBBY_ID_CAN_NOT_BE_NULL = "Lobby ID can not be null.";
	public static final String LOBBY_ID_CAN_NOT_BE_EMPTY = "Lobby ID can not be empty.";
	public static final String LOBBY_NAME_CAN_NOT_BE_NULL = "Lobby name can not be null.";
	public static final String LOBBY_NAME_CAN_NOT_BE_EMPTY = "Lobby name can not be empty.";
	public static final String PLAYER_NOT_FOUND_IN_LOBBY = "Player not found in lobby.";
	public static final String PLAYER_NOT_FOUND_IN_GAME = "Player not found in game.";
	public static final String GAME_NOT_FOUND_ON_SERVER = "Game not found on server.";
	public static final String GAME_ID_CAN_NOT_BE_NULL = "Game ID can not be null.";
	public static final String GAME_ID_CAN_NOT_BE_EMPTY = "Game ID can not be empty.";
	public static final String ROOM_ID_CAN_NOT_BE_NULL = "Room ID can not be null.";
	public static final String ROOM_NOT_FOUND_ON_SERVER = "Room not found on server.";
	public static final String PLAYER_NOT_FOUND_IN_ROOM = "Player not found in room.";
	public static final String USER_NOT_FOUND_ON_SERVER = "User not found on server.";
	public static final String GAME_NOT_INITIALIZED = "Game not initialized";
	public static final String NOT_YOUR_TURN = "Not your turn";
	public static final String SET_UP_NOT_COMPLETE = "Set up not complete";
	public static final String ROOM_IS_FULL = "Room is full";
	public static final String ROOM_ID_CAN_NOT_BE_EMPTY = "Room ID can not be empty.";
	public static final String CLAIM_ROUTE_ERROR = "Could not claim route.";

	/**
	 * Gets all of the commands from the lobby the player has missed since last update. If the lobby specified
	 * has launched into a game, it will instead return the commands related to that game from its inception.
	 *
	 * @param roomID      The lobby to get commands from.
	 * @param lastCommand The last command recieved from the lobby.
	 * @return Returns a CommandListMessage holding the commands to execute.
	 */
	public static CommandListMessage getCommandsFromLobby(String roomID, int lastCommand, String userName) throws
			IBaseCommand
					.CommandException
	{
		ICommandServerFacade facade = ServerFacade.getInstance(ICommandServerFacade.class);
		IPlayerRoom room = facade.getRoomManager().getRoom(roomID);
		if (room == null)
		{
			throw new IBaseCommand.CommandException(ROOM_IS_NULL, ErrorMessage.NULL_ROOM);
		}
		List<IBaseCommand> commands = null;
		//test if the room is a lobby
		if (room instanceof LobbyModel)
		{
			commands = room.getRecentCommands(lastCommand, userName);
		} else if (room instanceof ServerGameModel)
		{
			commands = room.getRecentCommands(-1, userName);
		} else
		{
			throw new IBaseCommand.CommandException(UNABLE_TO_GET_COMMANDS_FROM_ROOM, ErrorMessage.MILD_ERROR);
		}
		List<String> serializedCommands = new ArrayList<>();
		if (commands != null)
		{
			for (IBaseCommand cmd : commands)
			{
				String serializeCmd = SerDes.serializeCommand(cmd, SerDes.Source.SERVER);
				serializedCommands.add(serializeCmd);
			}
		}

		return new CommandListMessage(serializedCommands);
	}

	/**
	 * Gets all of the commands from the game the player has missed since last update.
	 *
	 * @param roomID      The game to get commands from.
	 * @param lastCommand The last command received from the game.
	 * @return Returns a CommandListMessage holding the commands to execute.
	 */

	public static CommandListMessage getCommandsFromGame(String roomID, int lastCommand, String userName) throws
			IBaseCommand
			.CommandException
	{
		ICommandServerFacade facade = ServerFacade.getInstance(ICommandServerFacade.class);
		IPlayerRoom room = facade.getRoomManager().getRoom(roomID);
		if (room == null)
		{
			throw new IBaseCommand.CommandException(ROOM_IS_NULL, ErrorMessage.NULL_ROOM);
		}
		List<IBaseCommand> commands = null;
		//test if the room is a lobby
		if (room instanceof ServerGameModel)
		{
			commands = room.getRecentCommands(lastCommand, userName);
		} else
		{
			throw new IBaseCommand.CommandException(UNABLE_TO_GET_COMMANDS_FROM_ROOM, ErrorMessage.MILD_ERROR);
		}
		List<String> serializedCommands = new ArrayList<>();
		if (commands != null)
		{
			for (IBaseCommand cmd : commands)
			{
				String serializeCmd = SerDes.serializeCommand(cmd, SerDes.Source.SERVER);
				serializedCommands.add(serializeCmd);
			}
		}

		return new CommandListMessage(serializedCommands);
	}

	//<editor-fold desc="User validation">
	public static void validateUsernameInAuthenticator(String username) throws IBaseCommand.CommandException
	{
		IAuthenticator authenticator = ServerFacade.getInstance(ICommandServerFacade.class).getAuthenticator();
		validateUsername(username);
		if (!authenticator.hasUserByName(username))
		{
			throw new IBaseCommand.CommandException(USER_NOT_FOUND_ON_SERVER, ErrorMessage.MILD_ERROR);
		}
	}
	public static void validateUsername(String username) throws IBaseCommand.CommandException
	{
		if (username == null)
		{
			throw new IBaseCommand.CommandException(USERNAME_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (!Utils.isUsernameValid(username))
		{
			throw new IBaseCommand.CommandException(ERROR_INVALID_USERNAME_LENGTH, ErrorMessage.MILD_ERROR);
		}
	}

	public static void validatePassword(String password) throws IBaseCommand.CommandException
	{
		if (password == null)
		{
			throw new IBaseCommand.CommandException(PASSWORD_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (!Utils.isPasswordValid(password))
		{
			throw new IBaseCommand.CommandException(ERROR_INVALID_PASSWORD_LENGTH, ErrorMessage.MILD_ERROR);
		}
	}
	//</editor-fold>

	//<editor-fold desc="Lobby Validation">

	/**
	 * validates that the lobbyID is not null, the lobby exists, and the user is in that lobby
	 *
	 * @param username
	 * @param lobbyID
	 * @throws IBaseCommand.CommandException
	 */
	public static void validatePlayerInLobby(String username, String lobbyID) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateLobby(lobbyID);
		if (!roomManager.getLobby(lobbyID).hasPlayer(username))
		{
			throw new IBaseCommand.CommandException(PLAYER_NOT_FOUND_IN_LOBBY, ErrorMessage.PLAYER_NOT_IN_ROOM);
		}
	}

	/**
	 * Validates if the lobbyID is not null, not empty, and the lobby exists
	 *
	 * @param lobbyId
	 * @throws IBaseCommand.CommandException
	 */
	public static void validateLobby(String lobbyId) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateLobbyID(lobbyId);
		if (!roomManager.hasLobby(lobbyId))
		{
			throw new IBaseCommand.CommandException(LOBBY_NOT_FOUND_ON_SERVER, ErrorMessage.MILD_ERROR);
		}
	}

	/**
	 * Validates that the lobby ID format is correct (not null and not empty)
	 *
	 * @param lobbyID
	 */
	public static void validateLobbyID(String lobbyID) throws IBaseCommand.CommandException
	{
		if (lobbyID == null)
		{
			throw new IBaseCommand.CommandException(LOBBY_ID_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (lobbyID.isEmpty())
		{
			throw new IBaseCommand.CommandException(LOBBY_ID_CAN_NOT_BE_EMPTY, ErrorMessage.MILD_ERROR);
		}
	}

	public static void validateLobbyName(String lobbyName) throws IBaseCommand.CommandException
	{
		if (lobbyName == null)
		{
			throw new IBaseCommand.CommandException(LOBBY_NAME_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (lobbyName.trim().isEmpty())
		{
			throw new IBaseCommand.CommandException(LOBBY_NAME_CAN_NOT_BE_EMPTY, ErrorMessage.MILD_ERROR);
		}
	}
	//</editor-fold>


	//<editor-fold desc="Game Validation">
	public static void validatePlayerInGame(String username, String gameID) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateGame(gameID);
		if (!roomManager.getGame(gameID).hasPlayer(username))
		{
			throw new IBaseCommand.CommandException(PLAYER_NOT_FOUND_IN_GAME, ErrorMessage.PLAYER_NOT_IN_ROOM);
		}
	}

	/**
	 * Validates if the gameID is not null, not empty, and the game exists
	 *
	 * @param gameID
	 * @throws IBaseCommand.CommandException
	 */
	public static void validateGame(String gameID) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateGameID(gameID);
		if (!roomManager.hasGame(gameID))
		{
			throw new IBaseCommand.CommandException(GAME_NOT_FOUND_ON_SERVER, ErrorMessage.MILD_ERROR);
		}
	}

	/**
	 * Validates that hte game ID format is correct (not null and not empty
	 *
	 * @param gameID
	 */
	public static void validateGameID(String gameID) throws IBaseCommand.CommandException
	{
		if (gameID == null)
		{
			throw new IBaseCommand.CommandException(GAME_ID_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (gameID.isEmpty())
		{
			throw new IBaseCommand.CommandException(GAME_ID_CAN_NOT_BE_EMPTY, ErrorMessage.MILD_ERROR);
		}
	}
	//</editor-fold>


	//<editor-fold desc="Room Validation">
	public static void validatePlayerInRoom(String username, String roomID) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateRoom(roomID);
		if (!roomManager.getRoom(roomID).hasPlayer(username))
		{
			throw new IBaseCommand.CommandException(PLAYER_NOT_FOUND_IN_ROOM, ErrorMessage.PLAYER_NOT_IN_ROOM);
		}
	}

	public static void validateRoom(String roomID) throws IBaseCommand.CommandException
	{
		RoomManager roomManager = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager();
		validateRoomID(roomID);
		if (!roomManager.hasRoom(roomID))
		{
			throw new IBaseCommand.CommandException(ROOM_NOT_FOUND_ON_SERVER, ErrorMessage.MILD_ERROR);
		}
	}

	public static void validateRoomID(String roomID) throws IBaseCommand.CommandException
	{
		if (roomID == null)
		{
			throw new IBaseCommand.CommandException(ROOM_ID_CAN_NOT_BE_NULL, ErrorMessage.MILD_ERROR);
		} else if (roomID.isEmpty())
		{
			throw new IBaseCommand.CommandException(ROOM_ID_CAN_NOT_BE_EMPTY, ErrorMessage.MILD_ERROR);
		}
	}
	//</editor-fold>

	public static void testSaveGame(IServerGameModel model)
	{
		//write to database before setting next player because hte next player command will not have been added
		// yet.
		if (DatabaseWriter.getInstance().shouldSave(model.getRoomID()))
		{
			DatabaseWriter.getInstance().setGame(model);
		}
	}

	public static void testSaveLobby(ServerLobbyModel model)
	{
		//write to database before setting next player because hte next player command will not have been added
		// yet.
		if (DatabaseWriter.getInstance().shouldSave(model.getRoomID()))
		{
			DatabaseWriter.getInstance().setLobby(model);
		}
	}
}
