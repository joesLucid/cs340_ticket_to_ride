package edu.byu.cs340.command.login;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.IAuthenticator;

/**
 * Created by joews on 2/1/2017.
 */
public class ServerLoginCommand extends LoginCommand
{
	public static final String LOGIN_OPERATION_ON_SERVER_FAILED = "Login operation on server failed.";
	String message = null;
	boolean success;

	public ServerLoginCommand(String username, String password, String authenticationToken)
	{
		super(username, password, authenticationToken);
	}


	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result;
		ICommandServerFacade serverFacade = ServerFacade.getInstance(ICommandServerFacade.class);
		IAuthenticator authenticator = serverFacade.getAuthenticator();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		ServerCommandUtils.validatePassword(mPassword);
		//OPERATIONS:
		mAuthenticationToken = authenticator.login(mUsername, mPassword);
		if (mAuthenticationToken == null)
		{
			throw new CommandException(LOGIN_OPERATION_ON_SERVER_FAILED, ErrorMessage.MILD_ERROR);
		} else
		{
			String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
			result = new CommandMessage(cmd);
		}

		return result;
	}
}
