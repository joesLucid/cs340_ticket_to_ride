package edu.byu.cs340.command.login;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.IAuthenticator;

/**
 * Created by joews on 2/1/2017.
 */
public class ServerRegisterCommand extends RegisterCommand
{

	public static final String ERROR_IN_REGISTER_OPERATION_ON_SERVER = "Error in register operation on server.";
	public static final String PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH = "Password and confirmation password " +
			"do not match.";
	public static final String USER_NAME_IS_TAKEN_BY_ANOTHER_USER = "User name is taken by another user.";


	public ServerRegisterCommand(String username, String password, String authenticationToken, String confirmPassword)
	{
		super(username, password, authenticationToken, confirmPassword);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result;
		ICommandServerFacade serverFacade = ServerFacade.getInstance(ICommandServerFacade.class);
		IAuthenticator authenticator = serverFacade.getAuthenticator();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePassword(mPassword);
		if (authenticator.hasUserByName(mUsername))
		{
			throw new CommandException(USER_NAME_IS_TAKEN_BY_ANOTHER_USER, ErrorMessage.MILD_ERROR);
		} else if (!mPassword.equals(mConfirmPassword))
		{
			throw new CommandException(PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH, ErrorMessage.MILD_ERROR);
		}

		//OPERATIONS:
		mAuthenticationToken = authenticator.register(mUsername, mPassword);
		if (mAuthenticationToken == null)
		{
			throw new CommandException(ERROR_IN_REGISTER_OPERATION_ON_SERVER, ErrorMessage.MILD_ERROR);
		} else
		{
			String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
			result = new CommandMessage(cmd);
		}
		return result;

	}
}
