package edu.byu.cs340.command.game;

import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.gameModel.gameOver.GameOverData;

/**
 * Created by crown on 3/25/2017.
 */
public class ServerGameOverCommand extends GameOverCommand {
	public ServerGameOverCommand(String username, int lastCommand, String gameID, GameOverData data) {
		super(username, lastCommand, gameID);
		mData = data;
	}

	@Override
	public IMessage execute() throws CommandException
	{

		return super.execute();
	}

	public ServerGameOverCommand(ServerGameOverCommand that)
	{
		super(that);
	}
}
