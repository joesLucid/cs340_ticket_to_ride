package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class ServerReturnDestinationCardsCommand extends ReturnDestinationCardsCommand
{

	public static final String CARDS_RETURNED_ARE_NOT_THE_RIGHT_FORMAT = "Cards returned are not the right format";

	public ServerReturnDestinationCardsCommand(String username, int lastCommand, String gameID, int
			maxDestinationCardsToReturn, List<IDestinationCard> cardsToReturn)
	{
		super(username, lastCommand, gameID, maxDestinationCardsToReturn, cardsToReturn);
	}

	public ServerReturnDestinationCardsCommand(ServerReturnDestinationCardsCommand that)
	{
		super(that);
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		if (mUsername == null || userStripping == null || !mUsername.equals(userStripping))
		{
			//strip!!
			ServerReturnDestinationCardsCommand cmd = new ServerReturnDestinationCardsCommand(this);
			cmd.mCardsToReturn.clear();
			return cmd;
		} else
		{
			return new ServerReturnDestinationCardsCommand(this);
		}
	}

	@Override
	public IMessage execute() throws IBaseCommand.CommandException
	{
		mSuccess = false;
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		if (!manager.getGame(mGameID).isInitialized())
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.GAME_NOT_INITIALIZED, ErrorMessage.MILD_ERROR);
		}//else if it is not the setup and its not he palyers turn
		else if (!manager.getGame(mGameID).isInitialState() && !manager.getGame(mGameID).isPlayersTurn(mUsername))
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.NOT_YOUR_TURN, ErrorMessage.MILD_ERROR);
		} else if (mCardsToReturn == null || mCardsToReturn.size() > mMaxCardsToReturn)
		{
			throw new IBaseCommand.CommandException(CARDS_RETURNED_ARE_NOT_THE_RIGHT_FORMAT, ErrorMessage
					.MILD_ERROR);
		}

		//OPERATIONS:
		//if the game is still setting up then draw the destination cards for the player and add command
		IServerGameModel gameModel = manager.getGame(mGameID);
		mNumCardsKept = ((GamePlayer) gameModel.getPlayer(mUsername)).getTemporaryDestinationCards().size() -
				mCardsToReturn.size();
		mSuccess = gameModel.returnDestinationCardsToBottom(mCardsToReturn, mUsername);
		mNewDestinationDeckSize = gameModel.getNumDestinationCardsInDeck();
		//mNewScore = edu.byu.cs340.models.gameModel.getScore(mUsername);
		mNewScore = gameModel.getPublicScore(mUsername);
		mPrivateScore = gameModel.getHiddenScore(mUsername);
		if (mSuccess)
		{
			gameModel.addCommand(this);
		}
		//now test if we should go to next players turn
		if (!gameModel.isInitialState())
		{
			//get the next player as player name
			String currentPlayer = gameModel.getCurrentPlayerName();
			IBaseCommand cmd = new ServerNextPlayerCommand(currentPlayer, mLastCommand, mGameID, null);
			cmd.execute();
		}
		result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);
		//draw destination cards and require 2 back.

		return result;
	}
}
