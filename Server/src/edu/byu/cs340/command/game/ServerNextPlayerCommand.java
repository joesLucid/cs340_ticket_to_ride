package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.IServerGameModel;

/**
 * Created by joews on 3/4/2017.
 */
public class ServerNextPlayerCommand extends NextPlayerCommand
{

	public ServerNextPlayerCommand(String username, int lastCommand, String gameID, String nextPlayer)
	{
		super(username, lastCommand, gameID);
		mNextPlayer = nextPlayer;
	}

	public ServerNextPlayerCommand(NextPlayerCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		if (!manager.getGame(mGameID).isInitialized())
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.GAME_NOT_INITIALIZED, ErrorMessage.MILD_ERROR);
		}//else if it is not the setup and its not the players turn
		else if (manager.getGame(mGameID).isInitialState())
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.SET_UP_NOT_COMPLETE, ErrorMessage.MILD_ERROR);
		}

		//OPERATIONS:
		IServerGameModel model = manager.getGame(mGameID);
		boolean success;
		if (mNextPlayer != null)
		{
			success = model.setNextPlayer(mNextPlayer);
		} else
		{
			success = model.nextPlayer();
		}
		if (success)
		{
			mNextPlayer = model.getCurrentPlayerName();
			model.addCommand(this);
		}
		result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);

		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		//no stripping needed
		return new ServerNextPlayerCommand(this);
	}
}
