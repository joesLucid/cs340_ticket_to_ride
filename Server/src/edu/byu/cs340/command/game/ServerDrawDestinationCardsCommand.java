package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.IServerGameModel;

import java.util.ArrayList;

/**
 * Created by joews on 3/4/2017.
 */
public class ServerDrawDestinationCardsCommand extends DrawDestinationCardsCommand
{

	public ServerDrawDestinationCardsCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
		mTemporaryCards = new ArrayList<>();
	}

	public ServerDrawDestinationCardsCommand(ServerDrawDestinationCardsCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		if (!manager.getGame(mGameID).isInitialized())
		{
			throw new CommandException(ServerCommandUtils.GAME_NOT_INITIALIZED, ErrorMessage.MILD_ERROR);
		} else if (!manager.getGame(mGameID).hasDestinationCards())
		{
			throw new CommandException("no cards in dest card deck", ErrorMessage.NO_CARDS_IN_DEST_DECK);
		}

		//OPERATIONS:
		//if the game is not fully set up yet
		else if (manager.getGame(mGameID).isInitialState())
		{
			//if the game is still setting up then draw the destination cards for the player and add command
			IServerGameModel gameModel = manager.getGame(mGameID);
			mTemporaryCards = gameModel.drawDestinationCards(mUsername);
			mMaxCardsToReturn = mTemporaryCards.size() - 2;
			gameModel.addCommand(this);
			result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);
			//draw destination cards and require 2 back.
		} else if (manager.getGame(mGameID).isPlayersTurn(mUsername))
		{
			//if it is the current player's turn then draw their destination cards and add the command to the game
			IServerGameModel gameModel = manager.getGame(mGameID);
			mTemporaryCards = gameModel.drawDestinationCards(mUsername);
			mMaxCardsToReturn = mTemporaryCards.size() - 1;
			gameModel.addCommand(this);
			result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);
		} else
		{
			throw new CommandException(ServerCommandUtils.NOT_YOUR_TURN, ErrorMessage.MILD_ERROR);
		}
		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		if (mUsername == null || userStripping == null || !mUsername.equals(userStripping))
		{
			//strip!!
			return null;
		} else
		{
			return new ServerDrawDestinationCardsCommand(this);
		}
	}
}
