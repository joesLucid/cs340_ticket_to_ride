package edu.byu.cs340;

/**
 * Created by joews on 4/11/2017.
 *
 *
 */
public interface IAbstractFactory
{
	IGameDAO createGameDAO();

	IUserDAO createUserDAO();

	ILobbyDAO createLobbyDAO();
}
