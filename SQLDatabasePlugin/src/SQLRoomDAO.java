import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jenessa on 4/10/2017.
 * 
 * A helper DAO to remove duplicate code from the game and lobby DAOs, because they act identically.
 */
public class SQLRoomDAO {
	private String mDatabaseName;
	
	private Connection mDatabaseConnection;
	
	public SQLRoomDAO(String databaseName){
		mDatabaseName = databaseName;
		mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
		try{
			Statement create = mDatabaseConnection.createStatement();
			create.executeUpdate(CREATE_ROOM_TABLE);
			create.executeUpdate(CREATE_COMMANDS_TABLE);
			create.close();
		} catch (SQLException e){
			System.err.println("Error creating room database.");
		} finally {
			try{
				mDatabaseConnection.close();
			} catch(SQLException e){
				System.err.println("Couldn't close the connection.");
			}
		}
	}
	
	public void setRoom(String roomID, String serializedRoom) {
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String deleteRoom = "DELETE FROM rooms WHERE id = '" + roomID + "';";
			String deleteCommands = "DELETE FROM commands WHERE room_id = '" + roomID + "';";
			String insert = "INSERT INTO rooms VALUES ('" + roomID + "','" + serializedRoom + "');";
			statement.executeUpdate(deleteRoom);
			statement.executeUpdate(deleteCommands);
			statement.executeUpdate(insert);
			statement.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error setting room in database.");
			e.printStackTrace();
		}
	}

	public void addCommandToRoom(String roomID, String serializedCmd) {
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String insert = "INSERT INTO commands VALUES ('" + roomID + "','" + serializedCmd + "');";
			statement.executeUpdate(insert);
			statement.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error adding command to room in database.");
		}
	}

	public List<String> getRooms() {
		List<String> rooms = new ArrayList<>();
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String selectRooms = "SELECT room FROM rooms;";
			ResultSet resultSet = statement.executeQuery(selectRooms);
			while(resultSet.next()){
				String oneRoom = resultSet.getString(1);
				rooms.add(oneRoom);
			}
			statement.close();
			mDatabaseConnection.close();
		} catch(SQLException e){
			System.err.println("Error getting rooms from database.");
		}
		return rooms;
	}

	public String getRoom(String roomID) {
		String room = "";
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String selectRooms = "SELECT room FROM rooms WHERE id = '" + roomID + "';";
			ResultSet resultSet = statement.executeQuery(selectRooms);
			resultSet.next();
			room = resultSet.getString(1);
			statement.close();
			mDatabaseConnection.close();
		} catch(SQLException e){
			e.printStackTrace();
			System.err.println("Error getting room from database.");
		}
		return room;
	}

	public List<String> getRoomCommands(String roomID) {
		List<String> commands = new ArrayList<>();
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String selectCommands = "SELECT command FROM commands WHERE room_id = '" + roomID + "';";
			ResultSet resultSet = statement.executeQuery(selectCommands);
			while(resultSet.next()){
				String oneCommand = resultSet.getString(1);
				commands.add(oneCommand);
			}
			statement.close();
			mDatabaseConnection.close();
		} catch(SQLException e){
			System.err.println("Error getting commands from database.");
		}
		return commands;
	}

	public void removeRoom(String roomID) {
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String deleteRoom = "DELETE FROM rooms WHERE id = '" + roomID + "';";
			String deleteCommands = "DELETE FROM commands WHERE room_id = '" + roomID + "';";
			statement.executeUpdate(deleteRoom);
			statement.executeUpdate(deleteCommands);
			statement.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error removing room in database.");
		}
	}

	public void clear() {
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(mDatabaseName);
			Statement statement = mDatabaseConnection.createStatement();
			String dropRoomsTable = "DROP TABLE IF EXISTS rooms;";
			String dropCommandsTable = "DROP TABLE IF EXISTS commands;";
			statement.executeUpdate(dropRoomsTable);
			statement.executeUpdate(dropCommandsTable);
			statement.executeUpdate(CREATE_ROOM_TABLE);
			statement.executeUpdate(CREATE_COMMANDS_TABLE);
		} catch(SQLException e){
			e.printStackTrace();
			System.err.println("Error clearing database");
		}
	}

	private final String CREATE_ROOM_TABLE = "CREATE TABLE IF NOT EXISTS rooms (" +
												"id TEXT PRIMARY KEY NOT NULL," +
												"room TEXT NOT NULL );";
	private final String CREATE_COMMANDS_TABLE = "CREATE TABLE IF NOT EXISTS commands (" +
													"room_id TEXT NOT NULL," +
													"command TEXT NOT NULL );";
}
