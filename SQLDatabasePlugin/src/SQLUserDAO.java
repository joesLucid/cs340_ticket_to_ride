import edu.byu.cs340.IUserDAO;
import edu.byu.cs340.UserDTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by crown on 4/10/2017.
 * user information data access object
 */
public class SQLUserDAO implements IUserDAO {
	private final String DATABASE_NAME = "users.db";
	
	private Connection mDatabaseConnection;
	
	public SQLUserDAO(){
			mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		try{
			Statement create = mDatabaseConnection.createStatement();
			create.executeUpdate(CREATE_USER_TABLE);
			create.executeUpdate(CREATE_USER_TO_ROOM_TABLE);
			create.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error creating game database.");
		}
	}

	/* does same thing as clearDAO
	public void dropTables()
	{
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		try{
			Statement drop = mDatabaseConnection.createStatement();
			drop.executeUpdate("DROP TABLE IF EXISTS rooms;");
			drop.executeUpdate("DROP TABLE IF EXISTS users;");
			drop.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error dropping game database.");
		}
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		try{
			Statement create = mDatabaseConnection.createStatement();
			create.executeUpdate(CREATE_USER_TABLE);
			create.executeUpdate(CREATE_USER_TO_ROOM_TABLE);
			create.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error creating game database.");
		}
	}*/


	@Override
	public void setUser(UserDTO userData) {
		//if user is not in database add the user
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		try {
			Statement insert = mDatabaseConnection.createStatement();
			insert.executeUpdate("DELETE FROM users WHERE username = '"+ userData.getUsername() + "';");
			insert.executeUpdate("INSERT INTO users VALUES ('" + userData.getUsername() + "',"
					+ userData.getPasshash() +",'" + userData.getAuthenticationToken() +"');");
			insert.close();
			mDatabaseConnection.close();
		} catch (SQLException e) {
			System.err.println("Error setting user in database.");
		}
	}

	@Override
	public List<UserDTO> getUsers() {
		//get the users from the database, if they do not exist return null
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		List<UserDTO> allUsers = null;
		try {
			Statement selectAll = mDatabaseConnection.createStatement();
			ResultSet resultSet = selectAll.executeQuery("SELECT username, password, token FROM users;");
			allUsers = new ArrayList<>();
			while (resultSet.next()) {
				String username = resultSet.getString("username");
				int passhash = resultSet.getInt("password");
				String token = resultSet.getString("token");
				//get the stuff
				UserDTO userInfo = new UserDTO(username, passhash, token);
				allUsers.add(userInfo);
			}
			selectAll.close();
			mDatabaseConnection.close();
		} catch (SQLException e) {
			System.err.println("Error getting users from database.");
		}
		return allUsers;
	}

	@Override
	public Map<String, String> getUsernameToRoomIDMap() {
	    //get the map or return null
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		Map<String, String> usersToRooms = null;
		try {
			Statement selectAll = mDatabaseConnection.createStatement();
			ResultSet resultSet = selectAll.executeQuery("SELECT username, roomid FROM rooms;");
			usersToRooms = new HashMap<>();
			while (resultSet.next()){
				String username = resultSet.getString("username");
				String roomid = resultSet.getString("roomid");
				//get the stuff
				usersToRooms.put(username, roomid);
			}
			selectAll.close();
			mDatabaseConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error getting user to room map from database.");
		}
		return usersToRooms;
	}

	@Override
	public void setUsernameToRoomIDMap(Map<String, String> map) {
	    //set the map
		mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
		for(String key : map.keySet())
		{
			try {
				Statement insert = mDatabaseConnection.createStatement();
				insert.executeUpdate("DELETE FROM rooms WHERE username = '"+ key + "';");
				insert.executeUpdate("INSERT INTO rooms VALUES ('" + key + "','"
						+ map.get(key) +"');");
				insert.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println("Error setting user to room map in database.");
			}
		}
		try {
			mDatabaseConnection.close();
		} catch (SQLException e) {
			System.err.println("Couldn't close connection.");
		}
	}

	@Override
	public void clearDAO()
	{
		try{
			mDatabaseConnection = SQLDatabaseOpener.getConnection(DATABASE_NAME);
			Statement statement = mDatabaseConnection.createStatement();
			statement.executeUpdate("DROP TABLE IF EXISTS users;");
			statement.executeUpdate("DROP TABLE IF EXISTS rooms;");
			statement.executeUpdate(CREATE_USER_TABLE);
			statement.executeUpdate(CREATE_USER_TO_ROOM_TABLE);
			statement.close();
			mDatabaseConnection.close();
		} catch (SQLException e){
			System.err.println("Error clearing the database.");
		}
	}

	public static String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS users (" +
											"username TEXT PRIMARY KEY NOT NULL," +
											"password INTEGER NOT NULL, " +
											"token TEXT NOT NULL );";

	public static String CREATE_USER_TO_ROOM_TABLE = "CREATE TABLE IF NOT EXISTS rooms (" +
													"username TEXT PRIMARY KEY NOT NULL," +
													"roomid TEXT NOT NULL );";

}
