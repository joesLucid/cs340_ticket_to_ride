import edu.byu.cs340.ILobbyDAO;

import java.util.List;

/**
 * Created by crown on 4/12/2017.
 *
 * The Data Access Object for storing and retrieving data in the SQLite lobby database.
 */
public class SQLLobbyDAO implements ILobbyDAO {
	private final String DATABASE_NAME = "lobbies.db";
	
	private SQLRoomDAO dao;
	
	public SQLLobbyDAO(){
		dao = new SQLRoomDAO(DATABASE_NAME);
	}


	@Override
	public void setLobby(String lobbyID, String serializedLobby) {
		dao.setRoom(lobbyID, serializedLobby);
	}

	@Override
	public void addCommandToLobby(String lobbyID, String serializedCmd) {
		dao.addCommandToRoom(lobbyID, serializedCmd);
	}

	@Override
	public List<String> getLobbies() {
		return dao.getRooms();
	}

	@Override
	public String getLobby(String lobbyID) {
		return dao.getRoom(lobbyID);
	}

	@Override
	public List<String> getLobbyCommands(String lobbyID) {
		return dao.getRoomCommands(lobbyID);
	}

	@Override
	public void removeLobby(String lobbyID) {
		dao.removeRoom(lobbyID);
	}

	@Override
	public void clearDAO()
	{
		dao.clear();
	}
}
