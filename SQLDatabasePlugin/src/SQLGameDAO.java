import edu.byu.cs340.IGameDAO;

import java.util.List;

/**
 * Created by Jenessa on 4/15/2017.
 *
 * The Data Access Object for storing and retrieving data in the SQLite game database.
 */
public class SQLGameDAO implements IGameDAO {
	private final String DATABASE_NAME = "games.db";
	private SQLRoomDAO dao;
	
	public SQLGameDAO(){
		dao = new SQLRoomDAO(DATABASE_NAME);
	}

	@Override
	public void setGame(String gameID, String serializedGame) {
		dao.setRoom(gameID, serializedGame);
	}

	@Override
	public void addCommandToGame(String gameID, String serializedCmd) {
		dao.addCommandToRoom(gameID, serializedCmd);
	}

	@Override
	public List<String> getGames() {
		return dao.getRooms();
	}

	@Override
	public String getGame(String gameID) {
		return dao.getRoom(gameID);
	}

	@Override
	public List<String> getGameCommands(String gameID) {
		return dao.getRoomCommands(gameID);
	}

	@Override
	public void removeGame(String gameID) {
		dao.removeRoom(gameID);
	}

	@Override
	public void clearDAO()
	{
		dao.clear();
	}
}
