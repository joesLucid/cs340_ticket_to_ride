import edu.byu.cs340.IAbstractFactory;
import edu.byu.cs340.IGameDAO;
import edu.byu.cs340.ILobbyDAO;
import edu.byu.cs340.IUserDAO;

/**
 * Created by Jenessa on 4/10/2017.
 * 
 * Factory for creating DAOs specific to the SQLite database.
 */
public class SQLDatabaseFactory implements IAbstractFactory
{
	@Override
	public IGameDAO createGameDAO() {
		return new SQLGameDAO();
	}

	@Override
	public IUserDAO createUserDAO() {
		return new SQLUserDAO();
	}
	
	@Override
	public ILobbyDAO createLobbyDAO() {
		return new SQLLobbyDAO();
	}
}
